var formRegister = $('#form-register'),
        formLogin = $('#form-login'),
        formOfficerRegister = $('#form-officer-register'),
        formForgotPassword = $("#form-forgot-password"),
        formCreatePassword = $("#form-create-password");

$(function () {
        initAlert();
        initValidator();

        initAutoGenerateGender();
        initAddTeamMember();
        initDeleteTeamMember();
        initCollapseApplicationType();
        initGeocoder();
        
        initInputTypeFile();
});
function initAlert() {
        if (alert != '') {
                $('.alert-info').removeClass('hide').hide().fadeIn(500, function () {
                        $(this).delay(3000).fadeOut(500);
                })
        }
}

function initInputTypeFile(){
        $(":file").filestyle({
                        iconName: "glyphicon glyphicon-inbox",
                        buttonText: "Pilih fail",
                        placeholder: "Tiada fail dipilih"
                });
}
function initValidator() {
        $('.number-only').keypress(function (e) {
                var mInput = $(this).val() + "";
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                        return false;
                }
        });
        var msgError1 = "Input tidak sah";
        
        //Registration Form
        formRegister.bootstrapValidator({
                feedbackIcons: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                        username: {
                                validators: {
                                        notEmpty: {message: msgError1},
                                        regexp: {
                                                regexp: /^[a-zA-Z0-9_\.]+$/,
                                                message: 'Kata nama hanya boleh terdiri daripada abjad, nombor, titik dan garis bawah'
                                        },
                                        remote: {
                                                message: 'Kata nama sudah wujud',
                                                url: baseUrl + 'accounts/check_username_exist/',
                                                data: function (validator) {
                                                        return {
                                                                username: validator.getFieldElements('username').val()
                                                        };
                                                },
                                                type: 'POST'
                                        },
                                }
                        },
                        password: {
                                validators: {
                                        notEmpty: {message: msgError1}
                                }
                        },
                        retype_password: {
                                validators: {
                                        notEmpty: {message: msgError1},
                                        identical: {
                                                field: 'password',
                                                message: 'Kata laluan tidak sepadan pengesahan'
                                        }
                                }
                        },
                        name: {
                                validators: {
                                        notEmpty: {message: msgError1}
                                }
                        },
                        ic_num_1: {
                                validators: {
                                        notEmpty: {message: msgError1},
                                        numeric: {message: msgError1},
                                        stringLength: {
                                                max: 6,
                                                min: 6,
                                                message: msgError1
                                        },
                                        remote: {
                                                message: 'Nombor kad pengenalan sudah wujud',
                                                url: baseUrl + 'accounts/check_ic_number_exist/',
                                                threshold: 6,
                                                data: function (validator) {
                                                        var ic_num_1 = validator.getFieldElements('ic_num_1').val();
                                                        var ic_num_2 = validator.getFieldElements('ic_num_2').val();
                                                        var ic_num_3 = validator.getFieldElements('ic_num_3').val();
                                                        var ic_num = ic_num_1 + '-' + ic_num_2 + '-' + ic_num_3;
                                                        
                                                        return {
                                                                ic_num_1: ic_num_1,
                                                                ic_num_2: ic_num_2,
                                                                ic_num_3: ic_num_3,
                                                        };
                                                },
                                                type: 'POST'
                                        },
                                }
                        },
                        ic_num_2: {
                                validators: {
                                        notEmpty: {message: msgError1},
                                        numeric: {message: msgError1},
                                        stringLength: {
                                                max: 2,
                                                min: 2,
                                                message: msgError1
                                        },
                                        remote: {
                                                message: 'Nombor kad pengenalan sudah wujud',
                                                url: baseUrl + 'accounts/check_ic_number_exist/',
                                                threshold: 2,
                                                data: function (validator) {
                                                        var ic_num_1 = validator.getFieldElements('ic_num_1').val();
                                                        var ic_num_2 = validator.getFieldElements('ic_num_2').val();
                                                        var ic_num_3 = validator.getFieldElements('ic_num_3').val();
                                                        var ic_num = ic_num_1 + '-' + ic_num_2 + '-' + ic_num_3;
                                                        
                                                        return {
                                                                ic_num_1: ic_num_1,
                                                                ic_num_2: ic_num_2,
                                                                ic_num_3: ic_num_3,
                                                        };
                                                },
                                                type: 'POST'
                                        },
                                }
                        },
                        ic_num_3: {
                                validators: {
                                        notEmpty: {message: msgError1},
                                        numeric: {message: msgError1},
                                        stringLength: {
                                                max: 4,
                                                min: 4,
                                                message: msgError1
                                        },
                                        remote: {
                                                message: 'Nombor kad pengenalan sudah wujud',
                                                url: baseUrl + 'accounts/check_ic_number_exist/',
                                                threshold: 4,
                                                data: function (validator) {
                                                        var ic_num_1 = validator.getFieldElements('ic_num_1').val();
                                                        var ic_num_2 = validator.getFieldElements('ic_num_2').val();
                                                        var ic_num_3 = validator.getFieldElements('ic_num_3').val();
                                                        var ic_num = ic_num_1 + '-' + ic_num_2 + '-' + ic_num_3;
                                                        
                                                        return {
                                                                ic_num_1: ic_num_1,
                                                                ic_num_2: ic_num_2,
                                                                ic_num_3: ic_num_3,
                                                        };
                                                },
                                                type: 'POST'
                                        },
                                }
                        },
                        address: {
                                validators: {
                                        notEmpty: {message: msgError1}
                                }
                        },
                        postcode: {
                                validators: {
//                                        notEmpty: {message: msgError1},
//                                        numeric: {message: msgError1},
                                        callback: {
                                                message: msgError1,
                                                callback: function (value, validator, $field) {
                                                        var result = false;

                                                        if (value >= 79000 && value <= 89999) {
                                                                result = true;
                                                        }/*else if(value == "89999"){	
                                                         result = true;
                                                         }else if(value == "79503"){	
                                                         result = true;
                                                         }*/
                                                        return result;
                                                }
                                        }
                                }
                        },
                        telp_home_no: {
                                validators: {
                                        notEmpty: {message: msgError1},
                                        numeric: {message: msgError1},
                                }
                        },
                        telp_no: {
                                validators: {
                                        numeric: {message: msgError1},
                                }
                        },
                        email: {
                            validators: {
                                emailAddress: {message: msgError1},
                            }
                        },
                        innovator_photo: {
                                validators: {
                                        notEmpty: {message: msgError1},
                                        file: {
                                                extension: 'png,jpeg,jpg,gif,bmp',
                                                type: 'image/png,image/jpeg,image/gif,image/x-ms-bmp',
                                                message: 'Please choose image file'
                                        }
                                }
                        },
                        district_id: {
                                validators: {
                                        notEmpty: {message: msgError1}
                                }
                        },
                        team_member_0: {
                                validators: {
                                        notEmpty: {message: msgError1}
                                }
                        },
                }
        });

        formRegister.bootstrapValidator('enableFieldValidators', 'team_member_0', false);
        formRegister.find("input[name=application_type]").on('click', function () {
                if (formRegister.find('input[name="application_type"]:checked').val() == 0) {
                        formRegister.bootstrapValidator('enableFieldValidators', 'team_member_0', false);
                } else {
                        formRegister.bootstrapValidator('enableFieldValidators', 'team_member_0', true);
                }
        });

        //Form Login
        formLogin.bootstrapValidator({
                feedbackIcons: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                        username: {
                                validators: {
                                        notEmpty: {message: 'The username is required'},
                                }
                        },
                        password: {
                                validators: {
                                        notEmpty: {message: 'The password is required'}
                                }
                        },
                }
        });
}

function initAddTeamMember() {
        var item_num = formRegister.find('.team-member-id').length;

        formRegister.on('click', '.add-team-member', function () {
                str = '<div class="col-md-12 team-member-item no-padding">';
                str += '<input type="hidden" class="team-member-id" name="team_member_id[]" value="0">';
                str += '<div class="col-md-11 no-padding">';
                str += '<div class="col-md-6 no-padding-left">';
                str += '<input type="text"class="form-control" name="team_member_' + item_num + '" placeholder="Nama">';
                str += '</div>';
                str += '<div class="col-md-6 no-padding-left">';
                str += '<input type="text"class="form-control" name="team_member_ic_num_' + item_num + '" placeholder="Nombor Kad Pengenalan">';
                str += '</div>';
                str += '</div>';
                str += '<div class="col-md-1 no-padding">';
                str += '<button type="button" class="btn btn-danger delete-team-member"><span class="fa fa-times"></span></button>';
                str += '</div>';
                str += '<div class="col-md-12 no-padding-left padding-top-10">';
                str += '<div class="col-md-3">';
                str += '<label>Gambar Ahli</label>';
                str += '</div>';
                str += '<div class="col-md-9 no-padding">';
                str += '<input type="file" class="form-control" name="team_member_pic_' + item_num + '">';
                str += '</div>';
                str += '</div>';
                str += '</div>';

                $(".team-member-wrap").append(str);
                item_num++;
        });
}

function initDeleteTeamMember() {
        formRegister.on('click', '.delete-team-member', function () {
                $(this).parent().parent().remove();
        });
}

function initAutoGenerateGender() {
        formRegister.find('input[name=ic_num_3]').on('keyup', function () {
                var field_val = $(this).val(),
                        ic_no_length = field_val.length;
                if (ic_no_length == 4) {
                        var last_two = parseInt(field_val.substr(ic_no_length - 2));
                        if (last_two % 2 == 1) {
                                //odd
                                $('input[name="gender"][value="0"]').prop('checked', true);
                        } else {
                                //even
                                $('input[name="gender"][value="1"]').prop('checked', true);
                        }
                }
        });
}

function initCollapseApplicationType() {
        formRegister.on('change', 'input[name=application_type]', function (e) {
                var elem = $(this);
                var collapse = formRegister.find('.collapse');
                collapse.collapse('toggle');
        });
}


function initGeocoder() {
        var geocoder = new google.maps.Geocoder();
        $('textarea[name=address]').on('blur', function () {
                var address = $(this).val();

                geocoder.geocode({'address': address}, function (results, status) {

                        if (status == google.maps.GeocoderStatus.OK) {
                                var latitude = results[0].geometry.location.lat();
                                var longitude = results[0].geometry.location.lng();

                                $('input[name=latitude]').val(latitude);
                                $('input[name=longitude]').val(longitude);
                        }
                });
        });
}