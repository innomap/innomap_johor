var tableApplication = $('#table-application'),
        formApplication = $('#form-application');

$(function () {
        var table = tableApplication.dataTable({
                "language": {
                    "url": baseUrl+"assets/js/plugins/datatables/i18n/malay.json"
                },
                "bStateSave": true,
        });

        initResetDtState(table);
        initImgFancybox();

        initAlert();
        initValidator();
        initAddPicture();
        initDeletePicture();
        initPopover();
        initDatepicker();

        initEdit();
        initView();
        initDelete();
        initDownloadPdf();
        initBtnBack();
        initInputTypeFile();
        initViewComment();
});

function initAlert() {
        if (alert != '') {
                $('.alert-info').removeClass('hide').hide().fadeIn(500, function () {
                        $(this).delay(3000).fadeOut(500);
                })
        }

        if (alert_dialog != '') {
                bootbox.alert({
                        message: lang_application_submitted_msg,
                        callback: function () {
                                window.location = baseUrl + "applications/generate_pdf/" + alert_dialog;
                        }
                });
        }
}

function initValidator() {
        $('.number-only').keypress(function (e) {
                var mInput = $(this).val() + "";
                console.log(mInput);
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                        return false;
                }
        });

        var errorMsg1 = "Input tidak sah";

        formApplication.bootstrapValidator({
                feedbackIcons: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                },
                live: 'disabled',
                fields: {
                        title: {
                                validators: {
                                        notEmpty: {message: errorMsg1},
                                }
                        },
                        idea: {
                                validators: {
                                        notEmpty: {message: errorMsg1},
                                }
                        },
                        description: {
                                validators: {
                                        notEmpty: {message: errorMsg1}
                                }
                        },
                        estimated_market_size: {
                                validators: {
                                        notEmpty: {message: errorMsg1},
                                }
                        },
                        how_to_use: {
                                validators: {
                                        notEmpty: {message: errorMsg1}
                                }
                        },
                        material: {
                                validators: {
                                        notEmpty: {message: errorMsg1},
                                }
                        },
                        special_achievement: {
                                validators: {
                                        notEmpty: {message: errorMsg1},
                                }
                        },
                        created_date: {
                                validators: {
                                        notEmpty: {message: errorMsg1},
                                }
                        },
                        discovered_date: {
                                validators: {
                                        notEmpty: {message: errorMsg1},
                                }
                        },
                        manufacturing_cost: {
                                validators: {
                                        notEmpty: {message: errorMsg1},
                                }
                        },
                        selling_price: {
                                validators: {
                                        notEmpty: {message: errorMsg1},
                                }
                        },
                        income: {
                                validators: {
                                        notEmpty: {message: errorMsg1},
                                }
                        },
                        innovation_category: {
                                validators: {
                                        notEmpty: {message: errorMsg1},
                                }
                        },
                        myipo_protection: {
                                validators: {
                                        notEmpty: {message: errorMsg1}
                                }
                        },
                        'target[]': {
                                validators: {
                                        choice: {
                                                min: 1,
                                                message: 'Sila pilih sekurang-kurangnya satu pilihan'
                                        }
                                }
                        },
                        'troubleshooting[]': {
                                validators: {
                                        choice: {
                                                min: 1,
                                                message: 'Sila pilih sekurang-kurangnya satu pilihan'
                                        }
                                }
                        },
                        picture_0: {
                                validators: {
                                        callback: {
                                                message: errorMsg1,
                                                callback: function (value, validator, $field) {
                                                        var result = false;
                                                        var imgLength = formApplication.find('.img-app').size();

                                                        if (imgLength > 0) {
                                                                result = true;
                                                        } else {
                                                                if (value != "") {
                                                                        result = true;
                                                                }
                                                        }
                                                        return result;
                                                }
                                        }
                                },
                        },
                }
        }).on('success.form.bv', function (e) {
                // Prevent form submission
                e.preventDefault();

                var $form = $(e.target),
                        validator = $form.data('bootstrapValidator'),
                        submitButton = validator.getSubmitButton();

                if (submitButton.context.name == "btn_send_for_approval") {
                        showSubmissionAlert();
                } else {
                        //formApplication.bootstrapValidator('defaultSubmit');
                        $('#pleaseWaitDialog').modal();
                        $form.ajaxSubmit({
                                dataType: 'json',
                                success: function (responseText, statusText, xhr, $form) {
                                        $('#pleaseWaitDialog').modal('hide');
                                        window.location.href = responseText.redirect_url;
                                }
                        });
                }

        });

        $('#btn-save').on('click', function () {
                formApplication.bootstrapValidator('enableFieldValidators', 'title', false);
                formApplication.bootstrapValidator('enableFieldValidators', 'idea', false);
                formApplication.bootstrapValidator('enableFieldValidators', 'description', false);
                formApplication.bootstrapValidator('enableFieldValidators', 'estimated_market_size', false);
                formApplication.bootstrapValidator('enableFieldValidators', 'how_to_use', false);
                formApplication.bootstrapValidator('enableFieldValidators', 'material', false);
                formApplication.bootstrapValidator('enableFieldValidators', 'special_achievement', false);
                formApplication.bootstrapValidator('enableFieldValidators', 'created_date', false);
                formApplication.bootstrapValidator('enableFieldValidators', 'discovered_date', false);
                formApplication.bootstrapValidator('enableFieldValidators', 'manufacturing_cost', false);
                formApplication.bootstrapValidator('enableFieldValidators', 'selling_price', false);
                formApplication.bootstrapValidator('enableFieldValidators', 'income', false);
                formApplication.bootstrapValidator('enableFieldValidators', 'innovation_category', false);
                formApplication.bootstrapValidator('enableFieldValidators', 'myipo_protection', false);
                formApplication.bootstrapValidator('enableFieldValidators', 'target[]', false);
                formApplication.bootstrapValidator('enableFieldValidators', 'troubleshooting[]', false);
                formApplication.bootstrapValidator('enableFieldValidators', 'picture_0', false);
        });
}

function showSubmissionAlert() {
        bootbox.hideAll();
        bootbox.dialog({
                message: lang_send_for_approval_confirm_message + " ?",
                title: lang_send_innovation_for_approval,
                onEscape: function () {},
                size: "large",
                buttons: {
                        close: {
                                label: lang_cancel,
                                className: "btn-default flat",
                                callback: function () {
                                        $(this).modal('hide');
                                        formApplication.find('input[type=submit]').prop('disabled', false);
                                }
                        },
                        danger: {
                                label: lang_send_for_approval,
                                className: "btn-success flat",
                                callback: function () {
                                        //formApplication.bootstrapValidator('defaultSubmit');				}
                                        $(this).modal('hide');
                                        $('#pleaseWaitDialog').modal();
                                        formApplication.ajaxSubmit({
                                                url: formApplication.attr('action'),
                                                dataType: 'json',
                                                data: {btn_send_for_approval: 1},
                                                success: function (responseText, statusText, xhr, $form) {
                                                        $('#pleaseWaitDialog').modal('hide');
                                                        window.location.href = responseText.redirect_url;
                                                }
                                        });
                                }
                        }
                }
        });
}

function initEdit() {
        tableApplication.on('click', '.btn-edit-application', function (e) {
                currentId = $(this).data('id');
                window.location.href = baseUrl + 'applications/edit/' + currentId;
        });
}

function initDelete() {
        tableApplication.on('click', '.btn-delete-application', function (e) {
                currentId = $(this).data('id');
                var name = $(this).data('name');
                bootbox.dialog({
                        message: lang_delete_confirm_message + " <strong>" + name + "</strong> ?",
                        title: lang_delete_application,
                        onEscape: function () {},
                        size: "small",
                        buttons: {
                                close: {
                                        label: lang_cancel,
                                        className: "btn-default flat",
                                        callback: function () {
                                                $(this).modal('hide');
                                        }
                                },
                                danger: {
                                        label: lang_delete,
                                        className: "btn-danger flat",
                                        callback: function () {
                                                window.location.href = baseUrl + 'applications/delete/' + currentId;
                                        }
                                }
                        }
                });
        });
}

function initImgFancybox() {
        $('.fancyboxs').fancybox({
                padding: 0,
                openEffect: 'elastic',
                helpers: {
                        overlay: {
                                css: {
                                        'background': 'rgba(50, 50, 50, 0.85)'
                                }
                        }
                }
        });
}

function initView() {
        tableApplication.on('click', '.btn-view-application', function (e) {
                currentId = $(this).data('id');
                window.location.href = baseUrl + 'applications/view/' + currentId;
        });

        if (typeof view_mode != 'undefined') {
                if (view_mode) {
                        disabledForm();
                }
        }
}

function disabledForm() {
        formApplication.find("input[type=text]").prop('readonly', true);
        formApplication.find("textarea, select, input[type=checkbox], input[type=radio]").prop('disabled', true);
        formApplication.find("button, input[type=file], input[type=submit]").hide();
        $('.btn-download-wrap').hide();
        formApplication.find('.popover-item').hide();
        formApplication.find("button.btn-back").show();
}

function initDownloadPdf() {
        tableApplication.on('click', '.btn-download-pdf', function (e) {
                var id = $(this).data('id') + "";
                
                if(id.length === 0){
                        return;
                }
                
                window.location.href = baseUrl + 'applications/generate_pdf/' + id;
        });
}

function initPopover() {
        $('.popover-item').popover({
                trigger: 'hover',
                container: 'body',
                placement: 'bottom'
        });
}

function initAddPicture() {
        $('.add-picture').on('click', function () {
                var num = $(".picture-item").length;
                if (num < 4) {
                        str = '<div class="col-md-11">'
//			str += 		'<input type="file" class="form-control picture-item" name="picture_'+num+'">';
                        str += '<input type="file" name="picture_'+num+'" accept="image/x-png, image/x-PNG, image/jpeg, image/jpg, image/JPEG, image/JPG" class="form-control picture-item">';
                        str += '</div>';
                        $(".picture-wrapper div.item").append(str);
                }
                $(":file").filestyle({
                        iconName: "glyphicon glyphicon-inbox",
                        buttonText: "Pilih file",
                        placeholder: "Tiada file dipilih"
                });
        });
}

function initDeletePicture() {
        $('.delete-picture').on('click', function () {
                var currentId = $(this).data('id'),
                        currentName = $(this).data('name');

                formApplication.append('<input type="hidden" name="deleted_picture[]" value="' + currentId + '">');
                formApplication.append('<input type="hidden" name="deleted_picture_name[]" value="' + currentName + '">');
                $(this).parent().remove();
                formApplication.bootstrapValidator('revalidateField', 'picture_0');
        });
}

function initDatepicker() {
        $('.datepicker').datepicker({
                format: 'dd-mm-yyyy',
                autoClose: true,
                endDate: new Date()
        }).on('changeDate', function (e) {
                formApplication.bootstrapValidator('revalidateField', e.currentTarget.name);
        });
}

function initBtnBack() {
        formApplication.on('click', '.btn-back', function (e) {
                window.location.href = baseUrl + 'applications';
        });
}

function initInputTypeFile() {
        $(":file").filestyle({
                iconName: "glyphicon glyphicon-inbox",
                buttonText: "Pilih fail",
                placeholder: "Tiada fail dipilih"
        });
}

function initViewComment(){
        $('.btn-comment').on('click', function (e) {
                var comment = $(this).data('comment') + "";
                comment = comment.length > 0 ? comment : "-";
                bootbox.dialog({
                        message: comment,
                        title: "Ulasan",
                        onEscape: function () {},
                        size: "small",
                        buttons: {
                                close: {
                                        label: "OK",
                                        className: "btn-success flat",
                                        callback: function () {
                                                $(this).modal('hide');
                                        }
                                },
                        }
                });
        });
}