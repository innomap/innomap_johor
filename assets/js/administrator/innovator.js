var tableInnovator = $('#table-innovator'),
	formResetPassword = $('#form-reset-password');

$(function(){
	var table = tableInnovator.dataTable({
		"language": {
		    "url": baseUrl+"assets/js/plugins/datatables/i18n/malay.json"
		},
		"bStateSave": true,
	});

	initResetDtState(table);
	initView();
	initImgFancybox();
	initResetPassword();
	initValidator();
	initDelete();
});

function initView(){
	tableInnovator.on('click','.btn-view-innovator', function(e){
		var currentId = $(this).data('id');
		window.location.href = adminUrl+'innovators/view/'+currentId;
	});

	$('.btn-back').on('click',function(e){
		window.location.href = adminUrl+'innovators';
	});
}

function initImgFancybox(){
	$('.fancyboxs').fancybox({
        padding: 0,
        openEffect: 'elastic',
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(50, 50, 50, 0.85)'
                }
            }
        }
    });
}

function initResetPassword(){
	tableInnovator.on('click','.btn-reset-password', function(e){
		var currentId = $(this).data('id');
		window.location.href = adminUrl+'innovators/reset_password/'+currentId;
	});

	$('.btn-cancel').on('click',function(e){
		window.location.href = adminUrl+'innovators';
	});
}

function initValidator(){
	formResetPassword.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			password: {
				validators: {
					notEmpty: {message: 'The Password is required'}
				}
			},
			retype_password: {
				validators: {
					notEmpty: {message: 'The Re-type Password is required'},
					identical: {
	                    field: 'password',
	                    message: 'The Password and its re-type password are not the same'
	                }
				}
			},
		}
	});
}

function initDelete(){
	tableInnovator.on('click','.btn-delete-innovator', function(e){
		currentId = $(this).data('id');
		var name = $(this).data('name');
		bootbox.dialog({
			message: lang_delete_confirm_message+" <strong>"+name+"</strong> ?",
			title: lang_delete_user,
			onEscape: function(){},
			size: "small",
			buttons: {
				close: {
					label: lang_cancel,
					className: "btn-default flat",
					callback: function() {
						$(this).modal('hide');
					}
				},
				danger: {
					label: lang_delete,
					className: "btn-danger flat",
					callback: function() {
						window.location.href = adminUrl+'innovators/delete/'+currentId;
					}
				}
			}
		});
	});
}