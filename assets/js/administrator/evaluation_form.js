var tableEvaluationForm = $('#table-evaluation-form'),
	formEvaluationForm = $('#form-evaluation-form'),
	modalEvaluationForm = $("#modal-evaluation-form");

$(function(){
	var table = tableEvaluationForm.dataTable({
		"language": {
		    "url": baseUrl+"assets/js/plugins/datatables/i18n/malay.json"
		},
		"bStateSave": true,
	});

	initResetDtState(table);
	initAlert();
	initValidator();
	initEdit();
	initDelete();
});
function initAlert(){
	if(alert != ''){
		$('.alert-info').removeClass('hide').hide().fadeIn(500, function(){
			$(this).delay(3000).fadeOut(500);	
		})
	}
}

function initValidator(){
	formEvaluationForm.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			name:{
				validators: {
					notEmpty: {message: 'The field is required'},
				}
			}
		}
	});
}

function initEdit(){
	$('.btn-edit-form').on('click', function(e){
		var currentId = $(this).data('id');
		window.location.href = adminUrl+'evaluation_form/edit/'+currentId;
	});


	modalEvaluationForm.on('hidden.bs.modal', function(){
		var str = criteriaFieldTemplate("","","",0);
		$('.criteria-wrap').html(str);
		formEvaluationForm.attr('action', adminUrl+'evaluation_form/store')
					.find('input[name=id]').val('');
		formEvaluationForm.bootstrapValidator('resetForm', true);
		formEvaluationForm.get(0).reset();
	});
}

function initDelete(){
	tableEvaluationForm.on('click','.btn-delete-form', function(e){
		currentId = $(this).data('id');
		var name = $(this).data('name');
		bootbox.dialog({
			message: lang_delete_confirm_message+" <strong>"+name+"</strong> ?",
			title: lang_delete_evaluation_focus,
			onEscape: function(){},
			size: "small",
			buttons: {
				close: {
					label: lang_cancel,
					className: "btn-default flat",
					callback: function() {
						$(this).modal('hide');
					}
				},
				danger: {
					label: lang_delete,
					className: "btn-danger flat",
					callback: function() {
						window.location.href = adminUrl+'evaluation_form/delete/'+currentId;
					}
				}
			}
		});
	});
}

function toogleCategory(category){
	if(category == 0){
		$('#subcategory-service').show();
		$('#subcategory-produk').hide();
	}else{
		$('#subcategory-service').hide();
		$('#subcategory-produk').show();
	}
}