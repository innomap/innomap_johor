var tableApplication = $('#table-application'),
        formApplication = $('#form-application'),
        modalAssign = $('#modal-assign'),
        formAssign = $('#form-assign'),
        formEvaluationList = $('#form-evaluation-list'),
        tableEvaluationList = $('#table-evaluation-list'),
        formEvaluation = $('#form-evaluation');

$(function () {
        var table = tableApplication.dataTable({
                "order": [[ 1, "asc" ]],
                "language": {
                        "url": baseUrl + "assets/js/plugins/datatables/i18n/malay.json"
                },
                "bStateSave": true,
        });

        initResetDtState(table);
        initAlert();
        initView();
        initImgFancybox();
        initBtnBack();
        initAssignApplication();
        initViewEvaluationList();
        initViewEvaluation();
        initApproval();
        initViewComment();
        initPrintPage();
        initDownloadPdf();
        initPrintPdf();
});

function initAlert() {
        if (alert != '') {
                $('.alert-info').removeClass('hide').hide().fadeIn(500, function () {
                        $(this).delay(3000).fadeOut(500);
                })
        }
}

function initView() {
        tableApplication.on('click', '.btn-view-application', function (e) {
                currentId = $(this).data('id');
                window.open(adminUrl + 'applications/view/' + currentId, '_self');
        });

        if (typeof view_mode != 'undefined') {
                if (view_mode) {
                        formApplication.find("input[type=text]").prop('readonly', true);
                        formApplication.find("textarea, select, input[type=checkbox], input[type=radio]").prop('disabled', true);
                        formApplication.find("button, input[type=file], input[type=submit]").hide();
                        formApplication.find("button.btn-back").show();
                }
        }
}

function initImgFancybox() {
        $('.fancyboxs').fancybox({
                padding: 0,
                openEffect: 'elastic',
                helpers: {
                        overlay: {
                                css: {
                                        'background': 'rgba(50, 50, 50, 0.85)'
                                }
                        }
                }
        });
}

function initBtnBack() {
        formApplication.on('click', '.btn-back', function (e) {
                window.location.href = adminUrl + 'applications';
        });
}

function initAssignApplication() {
        tableApplication.on('click', '.btn-assign-application', function () {
                var currentId = $(this).data('id');
                modalAssign.find('input[name=application_id]').val(currentId);
                modalAssign.modal();

                modalAssign.on('hidden.bs.modal', function () {
                        formAssign.bootstrapValidator('resetForm', true);
                        formAssign.get(0).reset();
                });
        });
}

function initViewEvaluationList() {
        tableApplication.on('click', '.btn-view-evaluation-list', function (e) {
                currentId = $(this).data('id');
                window.open(adminUrl + 'applications/view_evaluation_list/' + currentId, '_blank');
        });

        formEvaluationList.on('click', '.btn-cancel', function () {
                window.location.href = adminUrl + 'applications';
        });
}

function initViewEvaluation() {
        tableEvaluationList.on('click', '.btn-view-evaluation', function (e) {
                currentId = $(this).data('id');
                window.location.href = adminUrl + 'applications/view_evaluation/' + currentId;
        });

        if (typeof view_mode != 'undefined') {
                if (view_mode == 1) {
                        calculateTotal();
                        formEvaluation.find("input[type=text]").prop('readonly', true);
                        formEvaluation.find("textarea").prop('disabled', true);
                        formEvaluation.find("button,input[type=submit]").hide();
                        formEvaluation.find("button.btn-cancel").show();
                        formEvaluation.find(".popover-item").hide();
                        formEvaluation.find(".point-input").prop('disabled', true);

                        formEvaluation.on('click', '.btn-cancel', function (e) {
                                window.history.back();
                        });
                }
        }
}

function check_value(input) {
        if (isNaN(parseInt(input))) {
                return 0;
        } else {
                return parseInt(input);
        }
}

function calculateTotal() {
        var sumTotal = 0;
        $('.focus-item').each(function (i) {
                var percentage = $(this).data('percentage'),
                        id = $(this).data('id'),
                        total = 0, maxPoint = 0;

                $('.input-' + id + ':checked').each(function (j) {
                        maxPoint += check_value($(this).data('maxpoint'));
                });

                $('.input-' + id + ':checked').each(function (j) {
                        var point = check_value($(this).val());
                        total += point;
                });

                var calTotal = total / maxPoint * percentage;
                $('#total-' + id).val(calTotal.toFixed(1));
                sumTotal += calTotal;
        });

        $('#total').val(sumTotal.toFixed(1));
        $('input[name=sum_total]').val(sumTotal.toFixed(1));
}

function getApprovalMessage(message, name) {
        var message = '<div class="row"><div class="col-md-12">'
                + message
                + '<strong>' + name + '</strong> ?</div></div>'
                + '<div class="row margin-top-10"><div class="col-md-12">'
                + '<textarea class="form-control font-16px" name="comment" placeholder="Ulasan..." /> </div></div>';

        return message;
}

function initViewComment() {
        tableApplication.on('click', '.btn-comment', function (e) {
                var comment = $(this).data('comment') + "";
                comment = comment.length > 0 ? comment : "-";
                bootbox.dialog({
                        message: comment,
                        title: "Ulasan",
                        onEscape: function () {},
                        size: "small",
                        buttons: {
                                close: {
                                        label: "OK",
                                        className: "btn-success flat",
                                        callback: function () {
                                                $(this).modal('hide');
                                        }
                                },
                        }
                });
        });
}

function initApproval() {
        tableApplication.on('click', '.btn-approve', function (e) {
                currentId = $(this).data('id');
                name = $(this).data('name');
                bootbox.dialog({
                        message: getApprovalMessage(lang_approve_confirm_message, name),
                        title: lang_approve,
                        onEscape: function () {},
                        size: "small",
                        buttons: {
                                close: {
                                        label: lang_cancel,
                                        className: "btn-default flat",
                                        callback: function () {
                                                $(this).modal('hide');
                                        }
                                },
                                danger: {
                                        label: lang_approve,
                                        className: "btn-success flat",
                                        callback: function () {
                                                var url = adminUrl + 'applications/approve';
                                                var comment = $('textarea[name=comment]').val();
                                                console.log(comment);
                                                $.post(url, {id: currentId, is_approved: 1, comment: comment}, function (data) {
                                                        console.log("data > ".data);
                                                        var status = data.status;
                                                        var msg = data.msg;
                                                        console.log("status > ".status);
                                                        console.log("msg > ".msg);

                                                        window.location.href = adminUrl + 'applications/approved';
                                                }, "json");
                                        }
                                }
                        }
                });
        });

        tableApplication.on('click', '.btn-reject', function (e) {
                currentId = $(this).data('id');
                name = $(this).data('name');
                bootbox.dialog({
                        message: getApprovalMessage(lang_reject_confirm_message, name),
                        title: lang_reject,
                        onEscape: function () {},
                        size: "small",
                        buttons: {
                                close: {
                                        label: lang_cancel,
                                        className: "btn-default flat",
                                        callback: function () {
                                                $(this).modal('hide');
                                        }
                                },
                                danger: {
                                        label: lang_reject,
                                        className: "btn-danger flat",
                                        callback: function () {
                                                var url = adminUrl + 'applications/approve';
                                                var comment = $('textarea[name=comment]').val();
                                                console.log(comment);
                                                $.post(url, {id: currentId, is_approved: 0, comment: comment}, function (data) {
                                                        console.log("data > ".data);
                                                        var status = data.status;
                                                        var msg = data.msg;
                                                        console.log("status > ".status);
                                                        console.log("msg > ".msg);

                                                        window.location.href = adminUrl + 'applications/rejected';
                                                }, "json");
                                        }
                                }
                        }
                });
        });
}

function initPrintPage(){
    $('.print-page').click(function(e){
        e.preventDefault();
        window.print();
    });
}

function initDownloadPdf() {
    tableApplication.on('click', '.btn-download-pdf', function (e) {
        var id = $(this).data('id') + "";

        if(id.length === 0){
            return;
        }
        
        window.location.href = adminUrl + 'applications/generate_pdf/' + id;
    });
}

function initPrintPdf(){
    $('.btn-print-pdf').on('click', function(){
        var id = $(this).data('id') + "";
        $.post(adminUrl+'applications/get_print_pdf_url/',{id: id}, function(response){
            bootbox.dialog({closeButton: false,message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Loading...</div>' });
            print(response.url);
        },'json');
    });
}

function print(url){
    bootbox.hideAll();
    var _this = this,
      iframeId = 'iframeprint',
      $iframe = $('iframe#iframeprint');
    $iframe.attr('src', url);

    $iframe.load(function() {
      callPrint(iframeId);
    });
}

function callPrint(iframeId) {
    var PDF = document.getElementById(iframeId);
    PDF.focus();
    PDF.contentWindow.print();
}