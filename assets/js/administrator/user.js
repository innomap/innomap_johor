var tableUser = $('#table-user'),
	formUser = $('#form-user');

$(function(){
	var table = tableUser.dataTable({
		"language": {
		    "url": baseUrl+"assets/js/plugins/datatables/i18n/malay.json"
		},
		"bStateSave": true,
	});

	initResetDtState(table);

	initAlert();
	initValidator();
	initAdd();
	initEdit();
	initDelete();
	initAutoGenerateGender();
});
function initAlert(){
	if(alert != ''){
		$('.alert-info').removeClass('hide').hide().fadeIn(500, function(){
			$(this).delay(3000).fadeOut(500);	
		})
	}
}

function initAdd(){
	$('.btn-add-user').on('click', function(e){
		window.location.href = adminUrl+'users/add';
	});

	formUser.on('click','.btn-cancel', function(){
		window.location.href = adminUrl+'users';
	});
}

function initValidator(){
	formUser.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			username: {
				validators: {
					notEmpty: {message: 'The username is required'},
					regexp: {
	                    regexp: /^[a-zA-Z0-9_\.]+$/,
	                    message: 'The username can only consist of alphabetical, number, dot and underscore'
	                },
					remote: {
                        message: 'The username is not available',
                        url: baseUrl+'accounts/check_username_exist/',
                        data: function(validator) {
                            return {
                                username: validator.getFieldElements('username').val(),
                                id: validator.getFieldElements('id').val()
                            };
                        },
                        type: 'POST'
                    },
				}
			},
			password: {
				validators: {
					notEmpty: {message: 'The Password is required'}
				}
			},
			retype_password: {
				validators: {
					notEmpty: {message: 'The Re-type Password is required'},
					identical: {
	                    field: 'password',
	                    message: 'The Password and its re-type password are not the same'
	                }
				}
			},
		/*	name: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			ic_num_1: {
				validators: {
					notEmpty: {message: 'The field is required'},
					numeric: {message: 'The field must be a number'},
					stringLength: {
                        max: 6,
                        min:6,
                        message: 'The field must be 6 characters'
                    }
				}
			},
			ic_num_2: {
				validators: {
					notEmpty: {message: 'The field is required'},
					numeric: {message: 'The field must be a number'},
					stringLength: {
                        max: 2,
                        min:2,
                        message: 'The field must be 2 characters'
                    }
				}
			},
			ic_num_3: {
				validators: {
					notEmpty: {message: 'The field is required'},
					numeric: {message: 'The field must be a number'},
					stringLength: {
                        max: 4,
                        min:4,
                        message: 'The field must be 4 characters'
                    }
				}
			},
			position:{
				validators: {
					notEmpty: {message: 'The field is required'},
				}
			},
			department:{
				validators: {
					notEmpty: {message: 'The field is required'},
				}
			},
			address:{
				validators: {
					notEmpty: {message: 'The field is required'},
				}
			},
			postcode:{
				validators: {
					notEmpty: {message: 'The field is required'},
				}
			},*/
			district_id:{
				validators: {
					notEmpty: {message: 'The field is required'},
				}
			},
		/*	telp_no:{
				validators: {
					notEmpty: {message: 'The field is required'},
				}
			},*/
		}
	});
}

function initEdit(){
	if(edit_mode == 1){
		formUser.find('.btn-change-pass').removeClass('hide');
		formUser.find('.collapse-group').addClass('collapse');
	}

	formUser.on('click','.btn-change-pass',function(e){
		e.preventDefault();
	    var elem = $(this);
	    var collapse = elem.parent().parent().parent().find('.collapse');
	    collapse.collapse('toggle');
	});

	$('.btn-edit-user').on('click', function(e){
		var currentId = $(this).data('id');
		window.location.href = adminUrl+'users/edit/'+currentId;
	});
}

function initDelete(){
	tableUser.on('click','.btn-delete-user', function(e){
		currentId = $(this).data('id');
		var name = $(this).data('name');
		bootbox.dialog({
			message: lang_delete_confirm_message+" <strong>"+name+"</strong> ?",
			title: lang_delete_user,
			onEscape: function(){},
			size: "small",
			buttons: {
				close: {
					label: lang_cancel,
					className: "btn-default flat",
					callback: function() {
						$(this).modal('hide');
					}
				},
				danger: {
					label: lang_delete,
					className: "btn-danger flat",
					callback: function() {
						window.location.href = adminUrl+'users/delete/'+currentId;
					}
				}
			}
		});
	});
}

function initAutoGenerateGender(){
	formUser.find('input[name=ic_num_3]').on('keyup', function(){
		var field_val = $(this).val(),
			ic_no_length = field_val.length;
		if(ic_no_length == 4){
			var last_two = parseInt(field_val.substr(ic_no_length-2));
			if(last_two%2 == 1){
				//odd
				$('input[name="gender"][value="0"]').prop('checked', true);
			}else{
				//even
				$('input[name="gender"][value="1"]').prop('checked', true);
			}
		}
	});
	
}