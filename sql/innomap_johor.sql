-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 31, 2016 at 10:58 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `innomap_johor`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('aa0486f83f16c1c5f652f0c32564dcfee8825c3c', '::1', 1464595135, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436343539353133353b),
('a027a690cdf96290afb9a626e7f6b80fe6621e77', '::1', 1464596304, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436343539363133323b),
('fdd6fbce32687f0484d3ba7d105d6f5ae7fe56fa', '::1', 1464597442, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436343539373335393b),
('7ac3d53633f1003eb8e263b8bbcb9af3d83bd98c', '::1', 1464598849, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436343539383538313b),
('a51654dd926e37ae3e53509ea05162f81a8a5c07', '::1', 1464599287, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436343539393135383b),
('f57f9111fde048d716ff12668f2ab5ac98abdb8d', '::1', 1464600633, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436343630303530323b),
('83fddbade3eba35747aab9a8bdcde2d0b535d99e', '::1', 1464600970, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436343630303937303b),
('15c0321a2b824912e34f51ee7eae4c076810e73d', '::1', 1464602339, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436343630323238333b),
('4a747dd76b2f1fe784a7372fa756e1051ff88f6b', '::1', 1464602778, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436343630323737373b),
('2a6b880668b91f3164f1e45cb00f3233b237647e', '::1', 1464666794, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436343636363530393b),
('47aa7e32fad5fb7098a77edd2ed51ba65c71f168', '::1', 1464667129, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436343636363932363b),
('321aa7a3c1e5df491833154e710338019d684eda', '::1', 1464667525, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436343636373239323b),
('669c383a4dc410dab6acbee89a62b4b3a8468b6b', '::1', 1464668449, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436343636383136363b),
('a6081d434fc3c2675bd8e9412f4d26129e4384b4', '::1', 1464668598, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436343636383437333b),
('f7e7c286002dd625cb0612d149993c5c5adb7943', '::1', 1464669305, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436343636393231393b),
('5c868adbb4ef9c716dcb92225fe22b1880278e5f', '::1', 1464670322, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436343637303037393b),
('de8b77770b6b9af015a1d37a724698f99accada8', '::1', 1464670684, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436343637303636383b),
('cd402ac7e0b0bb7ca5bd0dd740b16a8cfe89dd3c', '::1', 1464671293, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436343637313239333b),
('0d07b2ff74e233ef6fc2f4bfe2081403f0d890c5', '::1', 1464672548, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436343637323332353b),
('3773921251fd275bc9e39b10011fd2c2e493eddf', '::1', 1464673302, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436343637333230333b),
('75a249a6d40101554e31192a802c18cecc2c899b', '::1', 1464674118, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436343637333933353b616c6572747c733a32343a22526567697374726174696f6e207375636365737366756c2e223b5f5f63695f766172737c613a313a7b733a353a22616c657274223b733a333a226f6c64223b7d),
('0495bfb2661b8c5cc7b60ea9d11da8c6d1aa5923', '::1', 1464674538, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436343637343533383b),
('b3b2197719ad6e39e797bc4593f8b1b45a5c99a8', '::1', 1464675195, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436343637353032383b696e6e6f6d61705f6a6f686f725f757365727c613a333a7b733a323a226964223b733a313a2232223b733a353a22656d61696c223b733a32383a226b696b69406d6f62696c75732d696e7465726163746976652e636f6d223b733a373a22726f6c655f6964223b733a313a2230223b7d),
('60917284d3864ccc261480c00bb98dbb58ffc037', '::1', 1464679295, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436343637393237373b696e6e6f6d61705f6a6f686f725f757365727c613a333a7b733a323a226964223b733a313a2232223b733a353a22656d61696c223b733a32383a226b696b69406d6f62696c75732d696e7465726163746976652e636f6d223b733a373a22726f6c655f6964223b733a313a2230223b7d616c6572747c733a34373a2254686520656d61696c206f722070617373776f726420796f7520656e746572656420697320696e636f72726563742e223b5f5f63695f766172737c613a313a7b733a353a22616c657274223b733a333a226f6c64223b7d),
('4756e599e5e930e9f808590d1557f5a8ca63df6e', '::1', 1464679833, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436343637393537383b696e6e6f6d61705f6a6f686f725f757365727c613a333a7b733a323a226964223b733a313a2232223b733a353a22656d61696c223b733a32383a226b696b69406d6f62696c75732d696e7465726163746976652e636f6d223b733a373a22726f6c655f6964223b733a313a2230223b7d),
('1050a15e79e4c703c9fc9fa9e15ba299eda5e91d', '::1', 1464680095, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436343637393937363b696e6e6f6d61705f6a6f686f725f757365727c613a333a7b733a323a226964223b733a313a2232223b733a353a22656d61696c223b733a32383a226b696b69406d6f62696c75732d696e7465726163746976652e636f6d223b733a373a22726f6c655f6964223b733a313a2230223b7d696e6e6f6d61705f6a6f686f725f61646d696e7c613a333a7b733a323a226964223b733a313a2232223b733a353a22656d61696c223b733a32383a226b696b69406d6f62696c75732d696e7465726163746976652e636f6d223b733a373a22726f6c655f6964223b693a323b7d);

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE IF NOT EXISTS `district` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `district`
--

INSERT INTO `district` (`id`, `name`) VALUES
(1, 'Johor Bahru'),
(2, 'Kulai');

-- --------------------------------------------------------

--
-- Table structure for table `district_officer`
--

CREATE TABLE IF NOT EXISTS `district_officer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `district_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `district_id` (`district_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `district_officer`
--

INSERT INTO `district_officer` (`id`, `user_id`, `name`, `district_id`) VALUES
(2, 5, 'Ririn', 2);

-- --------------------------------------------------------

--
-- Table structure for table `evaluator`
--

CREATE TABLE IF NOT EXISTS `evaluator` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `innovator`
--

CREATE TABLE IF NOT EXISTS `innovator` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `mara_center_id` int(11) NOT NULL,
  `mara_address` mediumtext NOT NULL,
  `mara_telp_no` varchar(50) NOT NULL,
  `mara_fax_no` varchar(50) NOT NULL,
  `staff_id` varchar(50) NOT NULL,
  `telp_no` varchar(50) NOT NULL,
  `fax_no` varchar(50) NOT NULL,
  `kp_no` varchar(50) NOT NULL,
  `photo` varchar(256) NOT NULL,
  `district_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `mara_center_id` (`mara_center_id`),
  KEY `district_id` (`district_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `innovator`
--

INSERT INTO `innovator` (`id`, `user_id`, `name`, `mara_center_id`, `mara_address`, `mara_telp_no`, `mara_fax_no`, `staff_id`, `telp_no`, `fax_no`, `kp_no`, `photo`, `district_id`) VALUES
(1, 2, 'Kiki', 2, 'binaan', '982347', '8728347', '88973248234', '8789989', '898989', '09989', '2_1-Bunaken_jpg.jpg', 2);

-- --------------------------------------------------------

--
-- Table structure for table `mara_center`
--

CREATE TABLE IF NOT EXISTS `mara_center` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=409 ;

--
-- Dumping data for table `mara_center`
--

INSERT INTO `mara_center` (`id`, `name`) VALUES
(1, 'BAHAGIAN AUDIT DALAM  '),
(2, 'BAHAGIAN BINAAN DAN SELENGGARAAN  '),
(3, 'BAHAGIAN KEMAHIRAN DAN TEKNIKAL  '),
(4, 'BAHAGIAN PEMBANGUNAN USAHAWAN  '),
(5, 'BAHAGIAN PENDIDIKAN MENENGAH  '),
(6, 'BAHAGIAN PENDIDIKAN TINGGI  '),
(7, 'BAHAGIAN PENGURUSAN ASET DAN PEROLEHAN  '),
(8, 'BAHAGIAN PMBGNN INDUSTRI & INFRASTRUKTUR '),
(9, 'BAHAGIAN TEKNOLOGI MAKLUMAT  '),
(10, 'BAHAGIAN TEKNOLOGI PENDIDIKAN  '),
(11, 'BHG KAWALAN KREDIT  '),
(12, 'BHG KEWANGAN-CAW KEWANGAN & AKAUN  '),
(13, 'BHG KOMUNIKASI KORPORAT  '),
(14, 'BHG PELABURAN DAN PEMBANGUNAN SUBSIDIARI '),
(15, 'BHG PEMANTAUAN & INSPEKTORAT  '),
(16, 'BHG PEMB INFRASTRUKTUR PERNIAGAAN  '),
(17, 'BHG PEMBIAYAAN PERNIAGAAN  '),
(18, 'BHG PENGANJURAN PELAJARAN  '),
(19, 'BHG SUMBER MANUSIA  '),
(20, 'BHG UNDANG-UNDANG  '),
(21, 'BHGN DASAR & PERANCANGAN STRATEGIK  '),
(22, 'BHGN INDUSTRI PENGANGKUTAN  '),
(23, 'IKM ALOR SETAR, KEDAH  '),
(24, 'IKM BESERI, PERLIS  '),
(25, 'IKM BESUT, TERENGGANU  '),
(26, 'IKM BINTULU, SARAWAK  '),
(27, 'IKM JASIN, MELAKA  '),
(28, 'IKM JOHOR BAHRU, JOHOR  '),
(29, 'IKM KOTA KINABALU, SABAH  '),
(30, 'IKM KUALA LUMPUR, W.PERSEKUTUAN  '),
(31, 'IKM KUCHING, SARAWAK  '),
(32, 'IKM LUMUT, PERAK  '),
(33, 'IKM SIK, KEDAH  '),
(34, 'IKM SUNGAI PETANI, KEDAH  '),
(35, 'IKM TASYA PEKAN, PAHANG  '),
(36, 'INSTITUT KECERMELANGAN MARA (ILKM)  '),
(37, 'IPM W PERSEKUTUAN, KUALA LUMPUR  '),
(38, 'JAB.KETUA PENGARAH(TERMSK.U.PENYELARAS)  '),
(39, 'KKTM BALIK PULAU, PULAU PINANG  '),
(40, 'KKTM KEMAMAN, TERENGGANU  '),
(41, 'KKTM KUANTAN, PAHANG  '),
(42, 'KKTM LEDANG  '),
(43, 'KKTM LENGGONG, PERAK  '),
(44, 'KKTM MASJID TANAH,MELAKA  '),
(45, 'KKTM PASIR MAS, KELANTAN  '),
(46, 'KKTM PETALING JAYA, SELANGOR  '),
(47, 'KKTM REMBAU, N. SEMBILAN  '),
(48, 'KKTM SRI GADING, JOHOR  '),
(49, 'KOLEJ ANTARABANGSA MARA, KETENGAH  '),
(50, 'KOLEJ MARA BANTING, SELANGOR  '),
(51, 'KOLEJ MARA KUALA NERANG, KEDAH  '),
(52, 'KOLEJ MARA KULIM, KEDAH  '),
(53, 'KOLEJ MARA SEREMBAN, N. SEMBILAN  '),
(54, 'KOLEJ PROF.MARA BANDAR PENAWAR, JOHOR  '),
(55, 'KOLEJ PROF.MARA INDERA MAHKOTA, PAHANG  '),
(56, 'KOLEJ PROFESIONAL MARA AYER MOLEK,MELAKA '),
(57, 'KOLEJ PROFESIONAL MARA BANDAR MELAKA  '),
(58, 'KOLEJ PROFESIONAL MARA BERANANG,SELANGOR '),
(59, 'KOLEJ PROFESIONAL MARA SRI ISKANDAR  '),
(60, 'MARA JAPAN INDUSTRIAL INSTITUTE (MJII)  '),
(61, 'MRSM ALOR GAJAH, MELAKA  '),
(62, 'MRSM ARAU, PERLIS  '),
(63, 'MRSM BALIK PULAU, PULAU PINANG  '),
(64, 'MRSM BALING, KEDAH  '),
(65, 'MRSM BATU PAHAT, JOHOR  '),
(66, 'MRSM BENTONG, PAHANG  '),
(67, 'MRSM BESERI, PERLIS  '),
(68, 'MRSM BESUT, TERENGGANU  '),
(69, 'MRSM BETONG, SARAWAK  '),
(70, 'MRSM FELDA, TROLAK, PERAK  '),
(71, 'MRSM GEMENCHEH, N SEMBILAN  '),
(72, 'MRSM GRIK, PERAK  '),
(73, 'MRSM JELI, KELANTAN  '),
(74, 'MRSM JOHOR BAHRU, JOHOR  '),
(75, 'MRSM KEPALA BATAS, PULAU PINANG  '),
(76, 'MRSM KOTA KINABALU, SABAH  '),
(77, 'MRSM KOTA PUTRA, BESUT, TERENGGANU  '),
(78, 'MRSM KUALA BERANG, TERENGGANU  '),
(79, 'MRSM KUALA KANGSAR, PERAK  '),
(80, 'MRSM KUALA KLAWANG, N.SEMBILAN  '),
(81, 'MRSM KUALA KRAI, KELANTAN  '),
(82, 'MRSM KUALA KUBU, SELANGOR  '),
(83, 'MRSM KUALA TERENGGANU, TERENGGANU  '),
(84, 'MRSM KUANTAN, PAHANG  '),
(85, 'MRSM KUBANG PASU, KEDAH  '),
(86, 'MRSM KUCHING, SARAWAK  '),
(87, 'MRSM LANGKAWI, KEDAH  '),
(88, 'MRSM LENGGONG, PERAK  '),
(89, 'MRSM MERBOK, KEDAH  '),
(90, 'MRSM MERSING, JOHOR  '),
(91, 'MRSM MUADZAM SHAH, PAHANG  '),
(92, 'MRSM MUAR, JOHOR  '),
(93, 'MRSM MUKAH, SARAWAK  '),
(94, 'MRSM PARIT, PERAK  '),
(95, 'MRSM PASIR SALAK, PERAK  '),
(96, 'MRSM PASIR TUMBUH, KELANTAN  '),
(97, 'MRSM PDRM-KULIM, KEDAH  '),
(98, 'MRSM PENDANG, KEDAH  '),
(99, 'MRSM PENGKALAN CHEPA, KELANTAN  '),
(100, 'MRSM PENGKALAN HULU, PERAK  '),
(101, 'MRSM PONTIAN, JOHOR  '),
(102, 'MRSM SANDAKAN, SABAH  '),
(103, 'MRSM SERTING, N. SEMBILAN  '),
(104, 'MRSM SUNGAI BESAR,SELANGOR  '),
(105, 'MRSM TAIPING, PERAK  '),
(106, 'MRSM TAWAU, SABAH  '),
(107, 'MRSM TERENDAK, MELAKA  '),
(108, 'MRSM TRANSKERIAN, PULAU PINANG  '),
(109, 'MRSM TUMPAT, KELANTAN  '),
(110, 'MRSM TUN ABDUL RAZAK, PEKAN,PAHANG  '),
(111, 'MRSM TUN GHAFAR BABA  '),
(112, 'MRSM TUN GHAZALI SHAFIE  '),
(113, 'PEJABAT MARA AUSTRALIA  '),
(114, 'PEJABAT MARA INDONESIA  '),
(115, 'PEJABAT MARA IRELAND  '),
(116, 'PEJABAT MARA JERMAN  '),
(117, 'PEJABAT MARA LONDON  '),
(118, 'PEJABAT MARA MESIR  '),
(119, 'PEJABAT MARA NEGERI JOHOR  '),
(120, 'PEJABAT MARA NEGERI KEDAH  '),
(121, '0016 PEJABAT MARA NEGERI KELANTAN  '),
(122, 'PEJABAT MARA NEGERI MELAKA  '),
(123, 'PEJABAT MARA NEGERI N. SEMBILAN  '),
(124, 'PEJABAT MARA NEGERI PAHANG  '),
(125, 'PEJABAT MARA NEGERI PERAK  '),
(126, 'PEJABAT MARA NEGERI PERLIS  '),
(127, 'PEJABAT MARA NEGERI PULAU PINANG  '),
(128, 'PEJABAT MARA NEGERI SABAH  '),
(129, 'PEJABAT MARA NEGERI SARAWAK  '),
(130, 'PEJABAT MARA NEGERI SELANGOR  '),
(131, 'PEJABAT MARA NEGERI TERENGGANU  '),
(132, 'PEJABAT MARA NEGERI W PERSEKUTUAN - KL  '),
(133, 'PEJABAT MARA RUSIA  '),
(134, 'PEJABAT MARA WASHINGTON  '),
(135, 'PUSAT KEPIMPINAN PELAJAR(PUSKEP)LENGGONG '),
(136, 'UNIT INTEGRITI MARA  '),
(137, 'UNIT PENYELIDIKAN DAN INOVASI  '),
(138, 'UNIT URUSETIA MAJLIS(TERMSK P.PENGERUSI) '),
(139, 'WILAYAH TENGAH KENDERAAN (K.L)  '),
(140, 'WILAYAH UTARA KENDERAAN (KEDAH) '),
(141, 'KOLEJ UNIVERSITI POLY-TECH MARA KUALA LUMPUR'),
(142, 'KOLEJ POLY-TECH MARA BANGI'),
(143, 'KOLEJ POLY-TECH MARA KUANTAN'),
(144, 'KOLEJ POLY-TECH MARA BATU PAHAT'),
(145, 'KOLEJ POLY-TECH MARA IPOH'),
(146, 'KOLEJ POLY-TECH MARA ALOR SETAR'),
(147, 'KOLEJ POLY-TECH MARA KOTA BAHRU'),
(148, 'KOLEJ POLY-TECH MARA SEMPORNA'),
(149, 'KOLEJ POLY-TECH MARA ? KESEDAR'),
(150, 'GERMAN MALAYSIA INSTITUTE'),
(151, 'Universiti Kuala Lumpur Malaysia France Institute (UniKL MFI)'),
(152, 'Universiti Kuala Lumpur British Malaysian Institute (UniKL BMI)'),
(153, 'Universiti Kuala Lumpur Malaysian Institute of Aviation Technology (UniKL MIAT)'),
(154, 'Universiti Kuala Lumpur Malaysian Spanish Institute (UniKL MSI)'),
(155, 'Universiti Kuala Lumpur Malaysian Institute of Information Technology (UniKL MIIT)'),
(156, 'Universiti Kuala Lumpur Malaysian Institute of Chemical and Bio-Engineering Technology (UniKL MICET)'),
(157, 'ÿUniversiti Kuala Lumpur Malaysian Institute of Marine Engineering Technology (UniKL MIMET)'),
(158, 'Universiti Kuala Lumpur Royal College of Medicine Perak (UniKL RCMP)'),
(159, 'Universiti Kuala Lumpur Institute of Product Designÿ and Manufacturing (UniKL IPROM)'),
(160, 'Universiti Kuala Lumpurÿ Malaysian Institute of Industrial Technology (UniKL MITEC)'),
(161, 'Universiti Kuala Lumpur Institute of Medical Science Technology (UniKL MESTECH)'),
(162, 'Universiti Kuala Lumpur Business School (UniKL Business School)'),
(163, 'Institute of Postgraduate Studiesÿ Universiti Kuala Lumpur City Campus (IPS, UniKL City Campus)'),
(164, 'Universiti Kuala Lumpur International College (UniKL ICOLE)'),
(165, 'GiatMARA Arau'),
(166, 'GiatMARA Kangar'),
(167, 'GiatMARA Padang Besar'),
(168, 'GiatMARA Alor Setar'),
(169, 'GiatMARA Baling'),
(170, 'GiatMARA Jerai'),
(171, 'GiatMARA Jerlun'),
(172, 'GiatMARA Kuala Kedah'),
(173, 'GiatMARA Kubang Pasu'),
(174, 'GiatMARA Kulim/Bandar Baharu'),
(175, 'GiatMARA Langkawi'),
(176, 'GiatMARA Merbok'),
(177, 'GiatMARA Padang Serai'),
(178, 'GiatMARA Padang Terap'),
(179, 'GiatMARA Pendang'),
(180, 'GiatMARA Pokok Sena'),
(181, 'GiatMARA Sik'),
(182, 'GiatMARA Sungai Petani'),
(183, 'GiatMARA Bagan'),
(184, 'GiatMARABalik Pulau'),
(185, 'GiatMARA Batu Kawan'),
(186, 'GiatMARA Bayan Lepas'),
(187, 'GiatMARA Bukit Bendera'),
(188, 'GiatMARA Bukit Gelugor'),
(189, 'GiatMARA Bukit Mertajam'),
(190, 'GiatMARA Jelutong'),
(191, 'GiatMARA Kepala Batas'),
(192, 'GiatMARA Nibong Tebal'),
(193, 'GiatMARA Permatang Pauh'),
(194, 'GiatMARA PRIMA Tasek Gelugor'),
(195, 'GiatMARA Sungai Bakap - Polis'),
(196, 'GiatMARA Tanjung'),
(197, 'GiatMARA Bagan Datoh'),
(198, 'GiatMARA Bagan Serai'),
(199, 'GiatMARA Batu Gajah'),
(200, 'GiatMARA Beruas'),
(201, 'GiatMARA Bukit Gantang'),
(202, 'GiatMARA Gerik'),
(203, 'GiatMARA Gopeng'),
(204, 'GiatMARA Ipoh'),
(205, 'GiatMARA Ipoh Timur'),
(206, 'GiatMARA Kampar'),
(207, 'GiatMARA Kuala Kangsar'),
(208, 'GiatMARA Larut'),
(209, 'GiatMARA Lenggong'),
(210, 'GiatMARA Lumut'),
(211, 'GiatMARA Padang Rengas'),
(212, 'GiatMARA Parit'),
(213, 'GiatMARA Parit Buntar'),
(214, 'GiatMARA Pasir Salak'),
(215, 'GiatMARA Sungai Siput'),
(216, 'GiatMARA Taiping'),
(217, 'GiatMARA Tambun'),
(218, 'GiatMARA Tanjung Malim'),
(219, 'GiatMARA Tapah'),
(220, 'GiatMARA Teluk Intan'),
(221, 'GiatMARA Ampang Jaya'),
(222, 'GiatMARA Bangi'),
(223, 'GiatMARA Gombak'),
(224, 'GiatMARA Hulu Langat'),
(225, 'GiatMARA Hulu Selangor'),
(226, 'GiatMARA Kapar'),
(227, 'GiatMARA Kelana Jaya'),
(228, 'GiatMARA Klang'),
(229, 'GiatMARA Kota Raja'),
(230, 'GiatMARA Kuala Langat'),
(231, 'GiatMARA Kuala Selangor'),
(232, 'GiatMARA Pandan'),
(233, 'GiatMARA Petaling Jaya'),
(234, 'Petaling Jaya Utara'),
(235, 'GiatMARA Puchong'),
(236, 'GiatMARA Sabak Bernam'),
(237, 'GiatMARA Selayang'),
(238, 'GiatMARA Sepang'),
(239, 'GiatMARA Serdang'),
(240, 'GiatMARA Shah Alam'),
(241, 'GiatMARA Subang'),
(242, 'GiatMARA Sungai Besar'),
(243, 'GiatMARA Tanjung Karang'),
(244, 'GiatMARA Bandar Tun Razak'),
(245, 'GiatMARA Batu'),
(246, 'GiatMARA Cheras'),
(247, 'GiatMARA Kepong'),
(248, 'GiatMARA Kuala Lumpur'),
(249, 'GiatMARA Lembah Pantai'),
(250, 'GiatMARA Putrajaya'),
(251, 'GiatMARA Segambut'),
(252, 'GiatMARA Seputeh'),
(253, 'GiatMARA Setiawangsa'),
(254, 'GiatMARA Titiwangsa'),
(255, 'GiatMARA Wangsa Maju'),
(256, 'GiatMARA Jelebu'),
(257, 'GiatMARA Jempol'),
(258, 'GiatMARA Kuala Pilah'),
(259, 'GiatMARA Rasah'),
(260, 'GiatMARA Rembau'),
(261, 'GiatMARA Seremban'),
(262, 'GiatMARA Tampin'),
(263, 'GiatMARA Telok Kemang'),
(264, 'GiatMARA Alor Gajah'),
(265, 'GiatMARA Bukit Katil'),
(266, 'GiatMARA Jasin'),
(267, 'GiatMARA Kota Melaka'),
(268, 'GiatMARA Masjid Tanah'),
(269, 'GiatMARA Simpang Ampat'),
(270, 'GiatMARA Tangga Batu'),
(271, 'GiatMARA Ayer Hitam'),
(272, 'GiatMARA Bakri'),
(273, 'GiatMARA Batu Pahat'),
(274, 'GiatMARA Gelang Patah'),
(275, 'GiatMARA Johor Bahru'),
(276, 'GiatMARA Kluang'),
(277, 'GiatMARA Komuniti Ledang'),
(278, 'GiatMARA Kota Tinggi'),
(279, 'GiatMARA Kulai'),
(280, 'GiatMARA Labis'),
(281, 'GiatMARA Mersing'),
(282, 'GiatMARA Muar'),
(283, 'GiatMARA Pagoh'),
(284, 'GiatMARA Parit Sulong'),
(285, 'GiatMARA Pasir Gudang'),
(286, 'GiatMARA Pengerang'),
(287, 'GiatMARA Pontian'),
(288, 'GiatMARA Segamat'),
(289, 'GiatMARA Sekijang'),
(290, 'GiatMARA Sembrong'),
(291, 'GiatMARA Simpang Renggam'),
(292, 'GiatMARA Sri Gading'),
(293, 'GiatMARA Tanjong Piai'),
(294, 'GiatMARA Tebrau'),
(295, 'GiatMARA Tenggara'),
(296, 'GiatMARA Bentong'),
(297, 'GiatMARA Bera'),
(298, 'GiatMARA Cameron Highlands'),
(299, 'GiatMARA Indera Mahkota'),
(300, 'GiatMARA Jengka'),
(301, 'GiatMARA Jerantut'),
(302, 'GiatMARA Kuala Krau'),
(303, 'GiatMARA Kuala Lipis'),
(304, 'GiatMARA Kuantan'),
(305, 'GiatMARA Maran'),
(306, 'GiatMARA Muadzam Shah'),
(307, 'GiatMARA Paya Besar'),
(308, 'GiatMARA Pekan'),
(309, 'GiatMARA Raub'),
(310, 'GiatMARA Rompin'),
(311, 'GiatMARA Temerloh'),
(312, 'GiatMARA Batu Rakit'),
(313, 'GiatMARA Besut'),
(314, 'GiatMARA Dungun'),
(315, 'GiatMARA Hulu Terengganu'),
(316, 'GiatMARA Kemaman'),
(317, 'GiatMARA Ketengah'),
(318, 'GiatMARA Kuala Nerus'),
(319, 'GiatMARA Kuala Terengganu'),
(320, 'GiatMARA Marang'),
(321, 'GiatMARA Setiu'),
(322, 'GiatMARA Bachok'),
(323, 'GiatMARA Gua Musang'),
(324, 'GiatMARA Jeli'),
(325, 'GiatMARA Ketereh'),
(326, 'GiatMARA Kota Bharu'),
(327, 'GiatMARA Kuala Krai'),
(328, 'GiatMARA Kubang Kerian'),
(329, 'GiatMARA Machang'),
(330, 'GiatMARA Nilam Puri'),
(331, 'GiatMARA Pasir Mas'),
(332, 'GiatMARA Pasir Puteh'),
(333, 'GiatMARA Pengkalan Chepa'),
(334, 'GiatMARA Rantau Panjang'),
(335, 'GiatMARA Tanah Merah'),
(336, 'GiatMARA Tumpat'),
(337, 'GiatMARA Batu Sapi'),
(338, 'GiatMARA Beaufort'),
(339, 'GiatMARA Beluran'),
(340, 'GiatMARA Gaya'),
(341, 'GiatMARA Keningau'),
(342, 'GiatMARA Kimanis'),
(343, 'GiatMARA Kinabatangan'),
(344, 'GiatMARA Kota Belud'),
(345, 'GiatMARA Kota Kinabalu'),
(346, 'GiatMARA Kota Marudu'),
(347, 'GiatMARA Kudat'),
(348, 'GiatMARA Labuan'),
(349, 'GiatMARA Libaran'),
(350, 'GiatMARA Limbawang'),
(351, 'GiatMARA Papar'),
(352, 'GiatMARA Penampang'),
(353, 'GiatMARA Pensiangan'),
(354, 'GiatMARA Putatan'),
(355, 'GiatMARA Ranau'),
(356, 'GiatMARA Sandakan'),
(357, 'GiatMARA Semporna'),
(358, 'GiatMARA Sepanggar'),
(359, 'GiatMARA Silam'),
(360, 'GiatMARA Sipitang'),
(361, 'GiatMARA Tawau'),
(362, 'GiatMARA Tenom'),
(363, 'GiatMARA Tuaran'),
(364, 'GiatMARA Bandar Kuching'),
(365, 'GiatMARA Baram'),
(366, 'GiatMARA Batang Lupar'),
(367, 'GiatMARA Batang Sadong'),
(368, 'GiatMARA Betong'),
(369, 'GiatMARA Bintulu'),
(370, 'GiatMARA Hulu Rejang'),
(371, 'GiatMARA Igan'),
(372, 'GiatMARA Julau'),
(373, 'GiatMARA Kanowit'),
(374, 'GiatMARA Kapit'),
(375, 'GiatMARA Kota Samarahan'),
(376, 'GiatMARA Lanang'),
(377, 'GiatMARA Lawas'),
(378, 'GiatMARA Limbang'),
(379, 'GiatMARA Lubok Antu'),
(380, 'GiatMARA Mambong'),
(381, 'GiatMARA Mas Gading'),
(382, 'GiatMARA Miri'),
(383, 'GiatMARA Mukah'),
(384, 'GiatMARA Petra Jaya'),
(385, 'GiatMARA Santubong'),
(386, 'GiatMARA Saratok'),
(387, 'GiatMARA Sarikei'),
(388, 'GiatMARA Selangau'),
(389, 'GiatMARA Serian'),
(390, 'GiatMARA Sibu'),
(391, 'GiatMARA Sibuti'),
(392, 'GiatMARA Sri Aman'),
(393, 'GiatMARA Stampin'),
(394, 'GiatMARA Tanjung Manis'),
(395, 'Pelaburan MARA Berhad'),
(396, 'MARA Incorporated Sdn Bhd'),
(397, 'Glocal Link (M) Sdn Bhd'),
(398, 'MARA Excellent Ventures Sdn Bhd'),
(399, 'F.I.T Centre Sdn Bhd'),
(400, 'Yayasan Pelajaran MARA'),
(401, 'MARA Liner Sdn Bhd'),
(402, 'Rural Capital Sdn Bhd'),
(403, 'YPM Realities Sdn Bhd'),
(404, 'Universiti Teknikal MARA Sdn Bhd'),
(405, 'Kolej Poly-Tech MARA Sdn Bhd'),
(406, 'German Malaysia Institute'),
(407, 'GiatMARA Sdn Bhd'),
(408, 'Pusat Pembangunan Rekabentuk (M) Sdn Bhd');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `email` varchar(256) NOT NULL,
  `password` mediumtext NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `role_id`, `email`, `password`, `status`, `created_at`, `updated_at`) VALUES
(2, 2, 'kiki@mobilus-interactive.com', '8cf28803f9c57fbb0f6f06f1f41ebe17', 1, '2016-05-31 06:53:43', '2016-05-31 07:34:50'),
(5, 1, 'ririn@mobilus-interactive.com', 'fbce2c4defa2516a3c289b74f3695c35', 1, '2016-05-31 07:55:04', '2016-05-31 05:55:04');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `district_officer`
--
ALTER TABLE `district_officer`
  ADD CONSTRAINT `district_officer_ibfk_2` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `district_officer_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `evaluator`
--
ALTER TABLE `evaluator`
  ADD CONSTRAINT `evaluator_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `innovator`
--
ALTER TABLE `innovator`
  ADD CONSTRAINT `innovator_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `innovator_ibfk_2` FOREIGN KEY (`mara_center_id`) REFERENCES `mara_center` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
