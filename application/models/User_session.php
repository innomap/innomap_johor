<?php

class User_session extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get_user() {
		return $this->session->userdata('innomap_johor_user');
	}

	function set_user($data) {
		$this->session->set_userdata('innomap_johor_user', $data);
	}

	function clear() {
		$this->session->sess_destroy();
	}

	function get_admin() {
		return $this->session->userdata('innomap_johor_admin');
	}

	function set_admin($data) {
		$this->session->set_userdata('innomap_johor_admin', $data);
	}

	function get_evaluator() {
		return $this->session->userdata('innomap_johor_evaluator');
	}

	function set_evaluator($data) {
		$this->session->set_userdata('innomap_johor_evaluator', $data);
	}

	function get_district_officer() {
		return $this->session->userdata('innomap_johor_district_officer');
	}

	function set_district_officer($data) {
		$this->session->set_userdata('innomap_johor_district_officer', $data);
	}

	function get_yim_admin() {
		return $this->session->userdata('innomap_johor_yim_admin');
	}

	function set_yim_admin($data) {
		$this->session->set_userdata('innomap_johor_yim_admin', $data);
	}
}