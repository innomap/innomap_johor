<?php

require_once(APPPATH . 'models/General_model.php');
class Application extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "application";
		$this->primary_field = "id";
	}

	function get_join($where = NULL){
		$this->db->select('application.*, district.name as district_name, innovator.name as innovator_name, innovator.latitude, innovator.longitude, innovator.application_type, innovator.ic_number, innovator.gender, innovator.address, innovator.postcode, innovator.telp_home_no, innovator.telp_no, innovator.email');
		$this->db->from('application');
		$this->db->join('innovator','innovator.user_id = application.user_id');
		$this->db->join('district', 'innovator.district_id = district.id');
		if($where != NULL){
			$this->db->where($where);
		}
		$q = $this->db->get();

		return $q;
	}
        
        function get_number($where = NULL, $group_by = NULL){
        	$this->db->select("*");
                $this->db->from($this->table_name);
                if($where != NULL){
                	$this->db->where($where);
                }

                if($group_by != NULL){
                	$this->db->group_by($group_by);
                }
                $q = $this->db->get();

                return $q->num_rows();
	}
        
        function count_submitted_by_district($district_id){
                $this->db->select('application.*');
		$this->db->from('application');
		$this->db->join('innovator','innovator.user_id = application.user_id');
                $this->db->where('innovator.district_id', $district_id);
                $this->db->where('application.status >= ', APPLICATION_STATUS_SENT_APPROVAL);
                $q = $this->db->get();
                
                return $q->num_rows();
        }
}

?>