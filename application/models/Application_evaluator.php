<?php

require_once(APPPATH . 'models/General_model.php');
class Application_evaluator extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "application_evaluator";
		$this->primary_field = "id";
	}

	function get_list_evaluation($application_id, $where = NULL){
		$this->db->select('evaluator.name as evaluator, application_evaluator.*');
		$this->db->from($this->table_name);
		$this->db->join('evaluator', 'application_evaluator.evaluator_id = evaluator.user_id');
		$this->db->where("application_evaluator.application_id = ".$application_id);
		if($where != NULL){
			$this->db->where($where);
		}

		return $this->db->get()->result_array();
	}

	function get_list_applications($where = NULL){
		$this->db->select($this->table_name.'.*,application.*, innovator.name as innovator_name');
		$this->db->from($this->table_name);
		$this->db->join('application', 'application.id = application_evaluator.application_id');
		$this->db->join('innovator', 'innovator.user_id = application.user_id');
		if($where != NULL){
			$this->db->where($where);
		}

		return $this->db->get()->result_array();
	}

	function is_evaluation_done($application_id){
		$arg = "SELECT * FROM `application_evaluator` 
				LEFT JOIN evaluation ON evaluation.user_id = application_evaluator.evaluator_id AND evaluation.application_id = application_evaluator.application_id
				WHERE evaluation.id IS NULL AND application_evaluator.application_id = ".$application_id;

		$query = $this->db->query($arg);
		$row = $query->num_rows();

		if($row > 0){
			return false;
		}else{
			return true;
		}
	}
}

?>