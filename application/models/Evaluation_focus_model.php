<?php

require_once(APPPATH . 'models/General_model.php');
class Evaluation_focus_model extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "evaluation_focus";
		$this->primary_field = "id";
	}
}

?>