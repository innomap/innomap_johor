<?php

require_once(APPPATH . 'models/General_model.php');
class Innovator_expert_team extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "innovator_expert_team";
		$this->primary_field = "id";
	}
}

?>