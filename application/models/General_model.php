<?php

class General_model extends CI_Model {
    protected $table_name = "";
    protected $primary_field = "";
            
    function __construct() {
	  parent::__construct();
    }
        
    //return id
    public function insert($data){
        $this->db->insert($this->table_name, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data){
        $this->db->where($this->primary_field, $id);
        return $this->db->update($this->table_name, $data);
    }
        
    public function find_by_id($id){
        if(!$id){
            return NULL;    
        }
        
        $this->db->select("*");
        $this->db->from($this->table_name);
        $this->db->where($this->primary_field, $id);
        $q = $this->db->get();
        if($q->num_rows()){
                return $q->row_array();
        }
        return NULL;
    }
        
    public function delete($id){
        if(!$id){
            return 0;    
        }
        $this->db->where($this->primary_field, $id);
        $this->db->delete($this->table_name);
        
        return 1;
    }
        
    public function delete_all(){
        $this->db->empty_table($this->table_name); 
    }
        
    // $where is string. ex: `id` = 1, `name` = 'fikran'
    // $order_by is string. ex: `name` ASC, `id` DESC
    public function find($where = NULL, $limit = 0, $offset = 0, $order_by = NULL, $group_by = NULL){
            $arg = "SELECT * FROM `{$this->table_name}` ";
            if($where){
                    $arg .= " WHERE {$where} ";
            }
            if($order_by){
                    $arg .= " ORDER BY {$order_by} ";
            }
            if($group_by){
                    $arg .= " GROUP BY {$group_by} ";
            }
            if($limit){
                    $arg .= " LIMIT {$offset},{$limit} ";
            }
            
            $query = $this->db->query($arg);
            
            return $query->result_array();
    }
        
    public function find_one($where = NULL){
        $items = $this->find($where, 1);
        
        if($items){
            return $items[0];
        }
        
        return NULL;
    }

    public function find_all(){
        return $this->find();
    }
    
    public function find_with_query($query_arg){
        if(!$query_arg){
            return array();    
        }
        
        $query = $this->db->query($query_arg);
        
        return $query->result_array();
    }
}
