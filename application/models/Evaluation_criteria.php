<?php

require_once(APPPATH . 'models/General_model.php');
class Evaluation_criteria extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "evaluation_criteria";
		$this->primary_field = "id";
	}

	function delete_by_evaluation_focus($focus_id){
		$this->db->where('evaluation_focus_id', $focus_id);
		return $this->db->delete($this->table_name);
	}
}

?>