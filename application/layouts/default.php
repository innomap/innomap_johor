<?php
	$ol_user = $this->user_session->get_user();
?>
<html>
	<head>
		<base href="<?= base_url() ?>" />
        <meta charset="UTF-8">
        <title>{{title}} | Innomap Johor</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link rel="shortcut icon" href="<?= ASSETS_IMG ?>favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?= ASSETS_IMG ?>favicon.ico" type="image/x-icon">
        <link href="<?= ASSETS_CSS ?>bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="<?= ASSETS_CSS ?>bootstrapValidator.min.css" />
        <link rel="stylesheet" type="text/css" href="<?= ASSETS_CSS ?>font-awesome.min.css" />
        <link href="<?= ASSETS_CSS ?>datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="<?= ASSETS_CSS ?>nanoscroller.css" rel="stylesheet" type="text/css" />
        <link href="<?= ASSETS_CSS ?>site.css" rel="stylesheet" type="text/css" />
        {{styles}}
	</head>
	<body class="container-fluid <?= $ol_user ? 'bg-grey' : 'bg-default-front' ?>">
		<?php if($ol_user){ ?>
            <div class="header">
                <nav class="menu navbar navbar-turquoise">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-header" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar text-white"></span>
                            <span class="icon-bar text-white"></span>
                            <span class="icon-bar text-white"></span>
                        </button>
                    </div>
                    <div id="navbar-header" class="collapse navbar-collapse">
                        <?php if($this->menu != "site" && $this->menu != "account"){ ?>
                            <ul class="nav navbar-nav">
                                <li class="<?= $this->menu == "application" ? "active" : "" ?>"><a href="<?= base_url().'applications' ?>">Penyertaan</a></li>
                                <li class="<?= $this->menu == "profile" ? "active" : "" ?>"><a href="<?= base_url().'profile' ?>">Kemaskini Profil</a></li>
                            </ul>
                        
                            <ul class="nav navbar-nav navbar-right logout-btn">
                                <li>
                                    <a href="<?= base_url().'accounts/logout' ?>"><span class="fa fa-sign-out"></span>Log Keluar</a>
                                </li>
                            </ul> 
                        <?php } ?>
                    </div>
                </nav>
            </div>
        <?php } ?>
		<div class="col-xs-12 col-sm-12 col-md-12 content">
	        {{content}}
	    </div>
        <?php if(!$ol_user){ ?>
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding footer">
            <div class="foot-menu">
                <nav class="navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-footer" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div id="navbar-footer" class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right padding-right-110">
                            <?php foreach ($this->menu as $key => $value) { ?>
                                <li class="<?= $this->uri->segment(3) == $value['url'] ? 'active' : '' ?>"><a href="<?= base_url().'site/article/'.$value['url'] ?>"><?= $value['menu'] ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <?php } ?>
        
        <?php if($ol_user){ ?>
        <div class="col-md-12 no-padding footer-default text-center">
        <?php }else{ ?>
        <?php date_default_timezone_set("Asia/Kuala_Lumpur");
            if(strtotime("2016-07-15 16:59:59") >= strtotime(date("Y-m-d H:i:s"))){ ?>
                <div class="col-md-3 col-md-offset-2 footer-btn text-center">
                    <a href="<?= base_url().'register' ?>" class="btn btn-red">Pendaftaran</a>
                </div>
            <?php } ?>
        <?php } ?>
        <div class="col-md-5 col-md-offset-6 footer-default text-center">
            Copyright &copy 2016 YIM Technology Resources Sdn Bhd. All Rights Reserved.
        </div>
        

	    
	     <!-- jQuery -->
	    <script src="<?= ASSETS_JS ?>jquery-2.1.1.min.js"></script>
	    <!-- Bootstrap -->
	    <script src="<?= ASSETS_JS ?>bootstrap.min.js" type="text/javascript"></script>
	    <script src="<?= ASSETS_JS ?>bootstrapValidator.min.js" type="text/javascript"></script>
	    <script src="<?= ASSETS_JS ?>bootstrap-filestyle.min.js" type="text/javascript"></script>
	    <!-- DATA TABES SCRIPT -->
	    <script src="<?= ASSETS_JS ?>plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
	    <script src="<?= ASSETS_JS ?>plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
	     <script src="<?= ASSETS_JS ?>bootbox.min.js" type="text/javascript"></script>
         <script src="<?= ASSETS_JS ?>plugins/nanoscroller/jquery.nanoscroller.min.js" type="text/javascript"></script>
         <script src="<?= ASSETS_JS ?>common.js" type="text/javascript"></script>
         <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
         <script type="text/javascript">
            var baseUrl = "<?=base_url()?>";
            $(".nano").nanoScroller();
        </script>
	    {{scripts}}
	</body>
</html>