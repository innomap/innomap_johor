<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/*PATH DIR*/
define('ASSETS',    'assets/');
define('ASSETS_CSS',    'assets/css/');
define('ASSETS_JS',    'assets/js/');
define('ASSETS_IMG', 'assets/img/');

define('PATH_TO_ADMIN', 'administrator/');
define('PATH_TO_EVALUATOR', 'evaluator/');

define('PATH_TO_INNOVATOR_PHOTO', 'assets/attachment/innovator_photo/');
define('PATH_TO_INNOVATOR_PHOTO_THUMB', 'assets/attachment/innovator_photo/thumbnail/');
define('PATH_TO_APPLICATION_PICTURE', 'assets/attachment/application_picture/');
define('PATH_TO_APPLICATION_PICTURE_THUMB', 'assets/attachment/application_picture/thumbnail/');
define('PATH_TO_TEAM_MEMBER_PHOTO', 'assets/attachment/team_member/');
define('PATH_TO_TEAM_MEMBER_PHOTO_THUMB', 'assets/attachment/team_member/thumbnail/');
define('PATH_TO_ARTICLE_IMAGES', 'assets/attachment/article/');
define('PATH_TO_APPLICATION_PDF', 'assets/attachment/application_pdf/');
define('PATH_TO_APPLICATION_FILE', 'assets/attachment/application_file/');

define('DEFAULT_LAYOUT', 'default');
define('ADMIN_LAYOUT', 'admin');
define('EVALUATOR_LAYOUT', 'evaluator');

define('LANGUAGE_MELAYU','melayu');
define('LANGUAGE_ENGLISH','english');

/*USER STATUS*/
define('USER_STATUS_NON_ACTIVE', 0);
define('USER_STATUS_ACTIVE', 1);

/*USER ROLE*/
define('USER_ROLE_INNOVATOR',0);
define('USER_ROLE_DISTRICT_OFFICER',1);
define('USER_ROLE_SUPER_ADMIN',2);
define('USER_ROLE_YIM_SUPER_ADMIN',3);
define('USER_ROLE_EVALUATOR',4);

define('USER_ROLE_ADMIN',serialize(array(
	1 => 'Admin',
	2 => 'Super Admin',
	3 => 'YIM Super Admin'
)));

/*APPLICATION STATUS*/
define('APPLICATION_STATUS_DRAFT', 0);
define('APPLICATION_STATUS_SENT_APPROVAL', 1);
define('APPLICATION_STATUS_APPROVED', 2);
define('APPLICATION_STATUS_REJECTED', 3);
define('APPLICATION_STATUS_ASSIGNED_TO_EVALUATOR', 4);
define('APPLICATION_STATUS_EVALUATED', 5);
define('APPLICATION_STATUS_PASSED', 6);
define('APPLICATION_STATUS_FAILED', 7);

define('APPLICATION_STATUS',serialize(array(
	0 => array('name' => "Draf",'en_lang' => "Draft", 'class' => "text-blue"),
	1 => array('name' => "Dihantar",'en_lang' => "Submitted", 'class' => "text-yellow"),
	2 => array('name' => "Terima",'en_lang' => "Approved", 'class' => "text-green"),
	3 => array('name' => "Tolak",'en_lang' => "Rejected", 'class' => "text-red"),
	4 => array('name' => "Ditugaskan kepada penilai",'en_lang' => "Assigned to Evaluator", 'class' => "text-green"),
	5 => array('name' => "Telah dinilai",'en_lang' => "Evaluated", 'class' => "text-green"),
	6 => array('name' => "Berjaya",'en_lang' => "Passed", 'class' => "text-green"),
	7 => array('name' => "Tidak Berjaya",'en_lang' => "Failed", 'class' => "text-red"),
)));

define('APPLICATION_STATUS_ADMIN',serialize(array(
	1 => array('name' => "Dihantar",'en_lang' => "Submitted", 'class' => "text-yellow"),
	2 => array('name' => "Terima",'en_lang' => "Approved", 'class' => "text-green"),
	3 => array('name' => "Tolak",'en_lang' => "Rejected", 'class' => "text-red"),
	4 => array('name' => "Ditugaskan kepada penilai",'en_lang' => "Assigned to Evaluator", 'class' => "text-green"),
	5 => array('name' => "Telah dinilai",'en_lang' => "Evaluated", 'class' => "text-green"),
	6 => array('name' => "Berjaya",'en_lang' => "Passed", 'class' => "text-green"),
	7 => array('name' => "Tidak Berjaya",'en_lang' => "Failed", 'class' => "text-red"),
)));

define('APPLICATION_TYPE_INDIVIDU', 0);
define('APPLICATION_TYPE_BERKUMPULAN', 1);

define('APPLICATION_TYPE', serialize(array(
	0 => "Individu",
	1 => "Berkumpulan",
)));

define('GENDER', serialize(array(
	0 => "Lelaki",
	1 => "Perempuan",
)));

define('INNOVATION_CATEGORY', serialize(array(
	0 => "Utiliti",
	1 => "Kesihatan, Cara Hidup Sihat (Wellness)",
	2 => "Pendidikan",
	3 => "Produktiviti",
	4 => "Makanan & Minuman",
)));

define('INNOVATION_TARGET', serialize(array(
	1=>'Individu',
	2=>'Organisasi/Institusi',
	3=>'Peniaga'
)));

define('TROUBLESHOOTING_OPTION', serialize(array(
	0=>'Penjimatan masa',
	1=>'Kualiti lebih baik',
	2=>'Mampu milik',
	3=>'Cekap',
	4=>'Meningkatkan produktiviti',
	5=>'Mengurangkan penggunaan bahan mentah',
	6=>'Penjimatan Kos',
	7=>'Penyediaan secara Halal',
	8=>'Mesra alam',
	9=>'Guna-semula tenaga',
	10=>'Rekabentuk mesra pengguna',
	11=>'Lebih selamat',
)));

define('ESTIMATED_MARKET_SIZE', serialize(array(
	0=>'Daerah',
	1=>'Negeri',
	2=>'Kebangsaan',
	3=>'Antarabangsa',
)));

define('INCOME_OPTION', serialize(array(
	0=>'Kurang RM10,000',
	1=>'RM10,001 hingga RM30,000',
	2=>'RM30,001 hingga RM50,000',
	3=>'Melebihi RM50,000',
)));

define('APPLICATION_STATUS_EVALUATOR',serialize(array(
	2 => array('name' => "Diterima",'en_lang' => "Approved", 'class' => "text-green"),
	3 => array('name' => "Ditolak",'en_lang' => "Rejected", 'class' => "text-green"),
	4 => array('name' => "Ditugaskan kepada penilai",'en_lang' => "Assigned to Evaluator", 'class' => "text-green"),
	5 => array('name' => "Telah dinilai",'en_lang' => "Evaluated", 'class' => "text-green"),
	6 => array('name' => "Berjaya",'en_lang' => "Passed", 'class' => "text-green"),
	7 => array('name' => "Tidak Berjaya",'en_lang' => "Failed", 'class' => "text-red"),
)));



