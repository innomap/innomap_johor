<script type="text/javascript">
	var alert = "<?= $alert ?>",
		view_mode = "<?= $view_mode; ?>";
</script>
<div class="col-md-10 col-md-offset-1 content">
	<?php $this->load->view('partial/logo') ?>
	<div class="col-md-12 text-center">
		<h2><?= lang('evaluation_form') ?></h2>
	</div>

	<div class="col-md-12">
		<div class="alert alert-info alert-dismissable hide">
		    <i class="fa fa-info"></i>
		    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		    <?= $alert ?>
		</div>

		<form action="<?= base_url().PATH_TO_EVALUATOR.'applications/evaluation_handler' ?>" id="form-evaluation" method="post">
			<input type="hidden" name="evaluation_id" value="<?= (isset($evaluation) ? $evaluation['id'] : 0) ?>">
			
			<table border="1" cellspacing="0" cellpadding="0" class="table-evaluation-form" id="table-evaluation-form">
				<tr>
					<td><?= lang('evaluator') ?></td>
					<td colspan="2">
						<?= $evaluator['name'] ?>
						<input type="hidden" name="evaluator" value="<?= $evaluator['user_id'] ?>">
					</td>
				</tr>
				<tr>
					<td><?= lang('project_title') ?></td>
					<td colspan="2">
						<?= $application['title'] ?>
						<input type="hidden" name="application_id" value="<?= $application['id'] ?>">
					</td>
				</tr>
				<tr>
					<td><?= lang('innovator') ?></td>
					<td colspan="2">
						<?= $innovator['name'] ?>
					</td>
				</tr>
				<tr class="head">
					<th style="width:20%"><b><?= lang('evaluation_focus') ?></b></th>
					<th style="width:55%"><b><?= lang('criteria') ?></b></th>
					<th><b><?= lang('marks') ?></b></th>
				</tr>
				<?php $j = 0; foreach ($evaluation_focus as $key => $value) {
						$no = 1;
						foreach ($value['criteria'] as $i => $criteria) { ?>
						 	<tr class="<?= ($i == 0 ? 'focus-item' : '') ?>" data-id="<?= $value['id'] ?>" data-percentage="<?= $value['percentage'] ?>">
								<td><b><?= ($i == 0 ? $value['label']." (".$value['percentage']."%)" : '') ?></b></td>
								<td colspan="2">
									<table>
										<tr>
											<td>
												<b><?= $no.". ".$criteria['question'] ?></b>
											</td>
										</tr>
										<tr>
											<td width="80%">
												<?= $criteria['description'] ?>
												<?php if($criteria['guideline'] != ""){ ?>
													&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= $criteria['guideline'] ?>"><i class="fa fa-question-circle"></i></span>
												<?php } ?>
											</td>
											<td width="20%">
												<div class="form-group">
													<?php for($i = 0; $i <= $criteria['max_point'];$i++){ ?>
														<div class="radio"> 
															<label> <input type="radio" class="point-input input-<?= $value['id'] ?>" data-maxpoint="<?= $criteria['max_point'] ?>" name="input_<?= $value['id']."_".$criteria['id'] ?>" value="<?= $i ?>" 
															<?= (isset($evaluation) ? (isset($evaluation_detail[$j]['point']) ? ($evaluation_detail[$j]['point'] == $i ? 'checked' : '') : ($i == 0 ? 'checked' : '')) : ($i == 0 ? 'checked' : '')) ?>><?= $i ?></label> 
														</div>
													<?php } ?>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
				<?php	$no++;$j++;}
				} ?>
			</table>

			<table border="1" cellspacing="0" cellpadding="0" class="table-evaluation-form" id="table-evaluation-total">
				<tr class="head">
					<?php foreach ($evaluation_focus as $key => $value) { ?>
							<th><?= $value['label'] ?></th>
					<?php } ?>
						<th>Total</th>
				</tr>
				<tr>
					<?php foreach ($evaluation_focus as $key => $value) { ?>
							<td>
								<div class="form-group">
									<input type="text" id="total-<?= $value['id'] ?>" data-id="<?= $value['id'] ?>" class="form-control input-total" readonly>
								</div>
							</td>
					<?php } ?>
						<td>
							<div class="form-group">
								<input type="text" id="total" class="form-control" readonly>
								<input type="hidden" name="sum_total">
							</div>
						</td>
				</tr>
			</table>

			<div class="form-group">
				<label><?= lang('comment') ?></label>
				<textarea class="form-control" placeholder="<?= lang('comment') ?>" rows="5" name="comment"><?=(isset($evaluation) ? $evaluation['comment'] : '')?></textarea>
			</div>

			<div class="col-md-12 text-center">
				<button type="button" class="btn btn-grey flat btn-cancel"><?= lang('cancel') ?></button>
				<input type="submit" class="btn btn-dark-turquoise flat" value="<?= lang('save') ?>">
			</div>
		</form>
	</div>
</div>