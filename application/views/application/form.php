<script type="text/javascript">
        var alert = "<?= $alert ?>",
                alert_dialog = "",
                lang_send_for_approval_confirm_message = "<?= lang('send_for_approval_confirm_message') ?>";
        var lang_send_innovation_for_approval = "<?= lang('send_innovation_for_approval') ?>",
                lang_cancel = "<?= lang('cancel') ?>",
                lang_send_for_approval = "<?= lang('send_for_approval') ?>",
                lang_yes = "<?= lang('yes') ?>",
                lang_application_submitted_msg = "<?= lang('application_submitted_msg') ?>";

        var view_mode = "<?= ($view_mode == 1 ? true : false) ?>";
</script>
<?php $this->load->view('partial/logo') ?>
<div class="col-md-8 col-md-offset-2 bg-light-grey">
        <div class="col-md-12 text-center margin-btm-20">
                <h2><?= lang('application_form') ?></h2>
        </div>

        <div class="col-md-12">
                <div class="alert alert-info alert-dismissable hide">
                        <i class="fa fa-info"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?= $alert ?>
                </div>
        </div>

        <form id="form-application" class="form-horizontal" action="<?= base_url() . $action_url ?>" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?= (isset($application) ? $application['id'] : 0) ?>">

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('application_type') ?></label>
                        <div class="col-md-9">
                                <?php foreach ($application_types as $key => $value) { ?>
                                        <div class="radio"> 
                                                <label> <input type="radio" name="application_type" value="<?= $key ?>" <?= (isset($innovator) ? $key == $innovator['application_type'] ? 'checked' : '' : $key == 0 ? 'checked' : '') ?> disabled> <?= $value ?> </label> 
                                        </div>
                                <?php } ?>
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('name') ?></label>
                        <div class="col-md-9">
                                <input type="text" class="form-control" name="name" placeholder="<?= lang('name') ?>" value="<?= (isset($innovator) ? $innovator['name'] : '') ?>" readonly>
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('ic_number') ?></label>
                        <div class="col-md-9">
                                <div class="input-group">
                                        <input type="text" maxlength="6"  class="form-control number-only" name="ic_num_1" placeholder="<?= lang('ic_number') ?>" value="<?= (isset($innovator) ? $innovator['ic_num_1'] : '') ?>" readonly>
                                        <span class="input-group-addon">-</span>
                                        <input type="text" maxlength="2"  class="form-control number-only" name="ic_num_2" value="<?= (isset($innovator) ? $innovator['ic_num_2'] : '') ?>" readonly>
                                        <span class="input-group-addon">-</span>
                                        <input type="text" maxlength="4"  class="form-control number-only" name="ic_num_3" value="<?= (isset($innovator) ? $innovator['ic_num_3'] : '') ?>" readonly>
                                </div>
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('gender') ?></label>
                        <div class="col-md-9">
                                <?php foreach ($gender as $key => $value) { ?>
                                        <div class="radio"> 
                                                <label> <input type="radio" name="gender" value="<?= $key ?>" <?= (isset($innovator) ? $key == $innovator['gender'] ? 'checked' : '' : $key == 0 ? 'checked' : '') ?> disabled> <?= $value ?> </label> 
                                        </div>
                                <?php } ?>
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('address') ?></label>
                        <div class="col-md-9">
                                <textarea class="form-control" name="address" rows="5" placeholder="<?= lang('address') ?>" disabled><?= (isset($innovator) ? $innovator['address'] : '') ?></textarea>
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('postcode') ?></label>
                        <div class="col-md-9">
                                <input type="text" class="form-control" name="postcode" placeholder="<?= lang('postcode') ?>" value="<?= (isset($innovator) ? $innovator['postcode'] : '') ?>" readonly>
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('district') ?></label>
                        <div class="col-md-9">
                                <select class="form-control" name="district_id" disabled>
                                        <option value="">-- <?= lang('select_district') ?> --</option>
                                        <?php foreach ($districts as $key => $value) { ?>
                                                <option value="<?= $value['id'] ?>" <?= (isset($innovator) ? $value['id'] == $innovator['district_id'] ? 'selected' : '' : '') ?>><?= $value['name'] ?></option>
                                        <?php } ?>
                                </select>
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('state') ?></label>
                        <div class="col-md-9">
                                <input type="text" class="form-control" name="state" placeholder="<?= lang('state') ?>" value="Johor" readonly>
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('telp_no') ?></label>
                        <div class="col-md-9">
                                <input type="text" class="form-control" name="telp_no" placeholder="<?= lang('telp_no') ?>" value="<?= (isset($innovator) ? $innovator['telp_no'] : '') ?>" readonly>
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('telp_home_no') ?></label>
                        <div class="col-md-9">
                                <input type="text" class="form-control" name="telp_home_no" placeholder="<?= lang('telp_home_no') ?>" value="<?= (isset($innovator) ? $innovator['telp_home_no'] : '') ?>" readonly>
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('email') ?></label>
                        <div class="col-md-9">
                                <input type="text" class="form-control" name="email" placeholder="<?= lang('email') ?>" value="<?= (isset($innovator) ? $innovator['email'] : '') ?>" readonly>
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('innovator_photo') ?></label>
                        <div class="col-md-9">
                                <?php if (file_exists(realpath(APPPATH . '../' . PATH_TO_INNOVATOR_PHOTO) . DIRECTORY_SEPARATOR . $innovator['photo']) && $innovator['photo'] != "") { ?>
                                        <a href="<?= base_url() . PATH_TO_INNOVATOR_PHOTO . $innovator['photo'] ?>" class="fancyboxs"><img src="<?= base_url() . PATH_TO_INNOVATOR_PHOTO_THUMB . $innovator['photo'] ?>" width="30%"/></a>
                                <?php } ?>
                        </div>
                </div>			

                <?php if ($innovator['application_type'] != APPLICATION_TYPE_INDIVIDU) { ?>
                        <div class="form-group">
                                <label class="col-md-3 control-label"><?= lang('team_member') ?></label>
                                <div class="col-md-9">
                                        <?php foreach ($innovator['team_experts'] as $key => $value) { ?>
                                                <div class="col-md-12 team-member-item no-padding">
                                                        <div class="col-md-12 no-padding">
                                                                <div class="col-md-4">
                                                                        <?php if (file_exists(realpath(APPPATH . '../' . PATH_TO_TEAM_MEMBER_PHOTO) . DIRECTORY_SEPARATOR . $value['photo']) && $value['photo'] != "") { ?>
                                                                                <a href="<?= base_url() . PATH_TO_TEAM_MEMBER_PHOTO . $value['photo'] ?>" class="fancyboxs"><img src="<?= base_url() . PATH_TO_TEAM_MEMBER_PHOTO_THUMB . $value['photo'] ?>" width="100%"/></a>
                                                                        <?php } ?>
                                                                </div>
                                                                <div class="col-md-4 no-padding-left">
                                                                        <input type="text"class="form-control" name="team_member_0" placeholder="<?= lang('name') ?>" value="<?= $value['name'] ?>" readonly>
                                                                </div>
                                                                <div class="col-md-4 no-padding-left">
                                                                        <input type="text"class="form-control" name="team_member_ic_num_0" placeholder="<?= lang('ic_number') ?>" value="<?= $value['ic_number'] ?>" readonly>
                                                                </div>
                                                        </div>
                                                </div>
                                        <?php } ?>
                                </div>
                        </div>
                <?php } ?>

                <div class="form-group category-wrapper">
                        <label class="col-md-3 control-label"><?= lang('category') ?></label>

                        <div class="col-md-9">
                                <select name="innovation_category" class="form-control category-opt">
                                        <option value="" disabled selected>Sila Pilih...</option>
                                        <?php foreach ($innovation_category as $key => $value) { ?>
                                                <option value="<?= $key ?>" <?= isset($application) ? ($application['innovation_category'] == $key ? "selected" : "") : "" ?>><?= $value ?></option>
                                        <?php } ?>
                                </select>
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('troubleshooting') ?></label>
                        <div class="col-md-9">
                                <?php foreach ($troubleshootings as $key => $value) { ?>
                                        <div class="checkbox"> 
                                                <label> <input type="checkbox" name="troubleshooting[]" value="<?= $key ?>" 
                                                        <?php
                                                        if (isset($application)) {
                                                                foreach ($i_troubleshooting as $val) {
                                                                        echo ($val == $key ? 'checked' : '');
                                                                }
                                                        }
                                                        ?>> <?= $value ?> </label> 
                                        </div>
<?php } ?>
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('innovation_product') ?></label>
                        <div class="col-md-9">
                                <input type="text" class="form-control" name="title" placeholder="<?= lang('innovation_product') ?>" value="<?= (isset($application) ? $application['title'] : "") ?>">
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('inspiration') ?>&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= lang('tips_idea') ?>"><i class="fa fa-question-circle"></i></span></label>
                        <div class="col-md-9">
                                <textarea class="form-control" name="idea" rows="5" placeholder="<?= lang('inspiration') ?>"><?= (isset($application) ? $application['idea'] : "") ?></textarea>
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('description') ?>&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= lang('tips_description') ?>"><i class="fa fa-question-circle"></i></span></label>
                        <div class="col-md-9">
                                <textarea class="form-control" name="description" rows="5" placeholder="<?= lang('description_placeholder') ?>"><?= (isset($application) ? $application['description'] : "") ?></textarea>
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('estimated_market_size') ?></label>

                        <div class="col-md-9">
                                <select name="estimated_market_size" class="form-control category-opt">
                                        <option value="" disabled selected>Sila Pilih...</option>
                                        <?php foreach ($estimated_market_size as $key => $value) { ?>
                                                <option value="<?= $key ?>" <?= isset($application) ? ($application['estimated_market_size'] == $key ? "selected" : "") : "" ?>><?= $value ?></option>
<?php } ?>
                                </select>
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('material') ?>&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= lang('tips_material') ?>"><i class="fa fa-question-circle"></i></span></label>
                        <div class="col-md-9">
                                <textarea class="form-control" name="material" rows="5" placeholder="<?= lang('material') ?>"><?= (isset($application) ? $application['material'] : "") ?></textarea>
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('how_to_use') ?>&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= lang('tips_how_to_use') ?>"><i class="fa fa-question-circle"></i></span></label>
                        <div class="col-md-9">
                                <textarea class="form-control" name="how_to_use" rows="5" placeholder="<?= lang('how_to_use') ?>"><?= (isset($application) ? $application['how_to_use'] : "") ?></textarea>
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('special_achievement') ?></label>
                        <div class="col-md-9">
                                <textarea class="form-control" name="special_achievement" rows="5" placeholder="<?= lang('special_achievement') ?>"><?= (isset($application) ? $application['special_achievement'] : "") ?></textarea>
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('created_date') ?></label>
                        <div class="col-md-9">
                                <input type="text" class="form-control datepicker" name="created_date" value="<?= (isset($application) ? ($application['created_date'] != "0000-00-00" ? $application['created_date'] : "") : "") ?>">
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('discovered_date') ?></label>
                        <div class="col-md-9">
                                <input type="text" class="form-control datepicker" name="discovered_date" value="<?= (isset($application) ? ($application['discovered_date'] != "0000-00-00" ? $application['discovered_date'] : "") : "") ?>">
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('manufacturing_cost') ?></label>
                        <div class="col-md-9">
                                <div class="input-group">
                                        <div class="input-group-addon">RM</div>
                                        <input type="text" class="form-control" name="manufacturing_cost" value="<?= (isset($application) ? $application['manufacturing_cost'] : "") ?>">
                                </div>
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('selling_price') ?></label>
                        <div class="col-md-9">
                                <div class="input-group">
                                        <div class="input-group-addon">RM</div>
                                        <input type="text" class="form-control" name="selling_price" value="<?= (isset($application) ? $application['selling_price'] : "") ?>">
                                </div>
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('income') ?></label>

                        <div class="col-md-9">
                                <select name="income" class="form-control">
                                        <option value="" disabled selected>Sila Pilih...</option>
                                        <?php foreach ($income as $key => $value) { ?>
                                                <option value="<?= $key ?>" <?= isset($application) ? ($application['income'] == $key ? "selected" : "") : "" ?>><?= $value ?></option>
<?php } ?>
                                </select>
                        </div>
                </div>		

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('target') ?></label>
                        <div class="col-md-9">
                                                <?php foreach ($targets as $key => $value) { ?>
                                        <div class="checkbox"> 
                                                <label> <input type="checkbox" name="target[]" value="<?= $key ?>" 
                                                        <?php
                                                        if (isset($application)) {
                                                                foreach ($i_target as $val) {
                                                                        echo ($val == $key ? 'checked' : '');
                                                                }
                                                        }
                                                        ?>> <?= $value ?> </label> 
                                        </div>
<?php } ?>
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('myipo_protection') ?></label>
                        <div class="col-md-9">
                                <div class="radio"> 
                                        <label> <input type="radio" name="myipo_protection" value="1" <?= (isset($application) ? ($application['myipo_protection'] == 1 ? "checked" : "") : "checked") ?>> <?= lang('yes') ?> </label> 
                                </div>
                                <div class="radio"> 
                                        <label> <input type="radio" name="myipo_protection" value="2" <?= (isset($application) ? ($application['myipo_protection'] == 2 ? "checked" : "") : "") ?>> <?= lang('no') ?> </label> 
                                </div>
                        </div>
                </div>

                <div class="form-group picture-wrapper">
                        <label class="col-md-3 control-label"><?= lang('picture') ?>&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= lang('tips_images') ?>"><i class="fa fa-question-circle"></i></span></label>

                        <div class="col-md-9 item">
                                <?php
                                if (isset($application)) {
                                        foreach ($i_picture as $key => $value) {
                                                if (file_exists(realpath(APPPATH . '../' . PATH_TO_APPLICATION_PICTURE_THUMB) . DIRECTORY_SEPARATOR . $value['name'])) {
                                                        $url = site_url() . 'assets/attachment/application_picture/thumbnail/' . $value['name'];
                                                } else {
                                                        $url = site_url() . 'assets/attachment/application_picture/thumbnail/default.jpg';
                                                }
                                                ?>
                                                <div class="col-md-12">
                                                        <a href="<?= $url ?>" class="fancyboxs">
                                                                <img src="<?= $url ?>" width="300px" class="img-app">
                                                        </a>
                                                        <button type="button" class="btn btn-danger delete-picture" data-id="<?= $value['id'] ?>" data-name="<?= $value['name'] ?>"><span class="fa fa-times"></span></button>
                                                </div>
        <?php }
}
?>

                                <div class="col-md-11">
                                        <input
                                                name="picture_0"
                                                type="file" 
                                                accept="image/x-png, image/x-PNG, image/jpeg, image/jpg, image/JPEG, image/JPG" 
                                                class="form-control filestyle picture-item" 
                                                data-iconName="glyphicon glyphicon-inbox"
                                                data-buttonText="Pilih file"
                                                data-placeholder="Tiada file dipilih">
                                </div>
                                <div class="col-md-1">
                                        <button type="button" class="btn btn-grey add-picture"><span class="fa fa-plus"></span></button>
                                </div>
                        </div>
                </div>

                <div class="col-md-12 text-center">
                        <button type="button" class="btn btn-grey btn-back"><?= ($this->userdata['role_id'] == USER_ROLE_INNOVATOR ? lang('cancel') : lang('back')) ?></button>
                        <input type="submit" name="btn_save" id="btn-save" data-id="btn_save" class="btn btn-dark-turquoise" value="<?= lang('save') ?>">
                        <?php 
                        date_default_timezone_set("Asia/Kuala_Lumpur");
                        if(strtotime($round['deadline']) >= strtotime(date("Y-m-d H:i:s"))){ ?>
                                <input type="submit" name="btn_send_for_approval" data-id="btn_send_for_approval" class="btn btn-dark-turquoise btn-send-for-approval" data-id="<?= isset($innovation) ? $innovation['innovation_id'] : 0 ?>" value="<?= lang('send_for_approval') ?>">
                        <?php } ?>        
                </div>
        </form>
</div>

<div class="modal" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">
                <div class="modal-content">
                        <div class="modal-header" style="padding:0 15px;">
                                <h4>Processing...</h4>
                        </div>
                        <div class="modal-body text-center">
                                <img src="<?= base_url() . ASSETS_IMG . 'loading.gif' ?>">
                        </div>
                </div>
        </div>
</div>