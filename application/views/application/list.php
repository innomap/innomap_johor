<script type="text/javascript">
	var alert = "<?=$alert?>",
		alert_dialog = "<?= $alert_dialog ?>";
	var lang_delete = "<?= lang('delete') ?>",
		lang_delete_application = "<?= lang('delete_application') ?>",
		lang_cancel = "<?= lang('cancel') ?>",
		lang_delete_confirm_message = "<?= lang('delete_confirm_message') ?>",
		lang_application_submitted_msg = "<?= lang('application_submitted_msg') ?>";
</script>
<div class="col-md-12">
	<?php $this->load->view('partial/logo') ?>

	<?php date_default_timezone_set("Asia/Kuala_Lumpur");
            if(strtotime("2016-07-15 16:59:59") >= strtotime(date("Y-m-d H:i:s"))){ ?>
		    <div class="col-md-12">
				<a href="<?= base_url().'applications/add' ?>">
					<button type="button" class="btn btn-dark-turquoise">
						<span class="fa fa-plus"></span> <?= lang('new_application') ?>
					</button>
				</a>
		    </div>
	<?php } ?>

    <div class="col-md-12 table-responsive content-box">
		<div class="alert alert-info alert-dismissable hide">
		    <i class="fa fa-info"></i>
		    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		    <?= $alert ?>
		</div>
		<table id="table-application" class="table table-bordered table-dark table-striped table-dark">
		    <thead>
		    <tr>
				<th>No</th>
				<th><?= lang('project_title') ?></th>
				<th><?= lang('description') ?></th>
				<th><?= lang('status') ?></th>
				<th><?= lang('action') ?></th>
		    </tr>
		    </thead>
		    <tbody>
		    <?php $no = 1; foreach ($applications as $value) { ?>
			<tr>
			    <td><?= $no ?></td>
			    <td><?= $value['title'] ?></td>
			    <td><?= ellipsis($value['description'], 100) ?></td>
			    <td class="<?= $status[$value['status']]['class'] ?>"><?= $status[$value['status']]['name'] ?></td>
			    <td>
			    <?php if($value['status'] == APPLICATION_STATUS_DRAFT){ ?>
					<button type="button" class="btn btn-default btn-action btn-edit-application" title="<?= lang('edit') ?>" data-id="<?= $value['id'] ?>">
					    <span class="fa fa-pencil fa-lg"></span>
					</button>

					<button type="button" class="btn btn-default btn-action btn-delete-application text-red" data-id="<?= $value['id'] ?>"
					data-name="<?= $value['title'] ?>" title="<?= lang('delete') ?>">
					    <span class="fa fa-trash-o fa-lg"></span>
					</button>
					<?php 
				 }else{ ?>
					<button type="button" class="btn btn-default btn-action btn-view-application" title="<?= lang('view_detail') ?>" data-id="<?= $value['id'] ?>">
					    <span class="fa fa-search fa-lg"></span> 
					</button>
				<?php } ?>
				<?php
				 if($value['status'] == APPLICATION_STATUS_APPROVED || $value['status'] == APPLICATION_STATUS_REJECTED){ ?>
					<button type="button" class="btn btn-default btn-action btn-comment" title="Lihat ulasan" data-id="<?= $value['id'] ?>" data-comment="<?= $value['comment'] ?>" >
					    <span class="fa fa-comment-o fa-lg"></span> 
					</button>
				<?php } 
                                if($value['status'] >= APPLICATION_STATUS_SENT_APPROVAL){
                                ?>
                                        <button type="button" class="btn btn-default btn-action btn-download-pdf" title="Download pdf" data-id="<?= $value['id'] ?>" >
                                                <span class="fa fa-download fa-lg"></span> 
                                        </button>
                                <?php } ?>
                                    
			    </td>
			</tr>
		    <?php $no++;} ?>
		    </tbody>
		</table>
    </div>
</div>