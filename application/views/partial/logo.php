<div class="col-md-12 col-sm-12 col-xs-12 text-center part-logo-wrap">
	<div class="col-md-3 col-sm-6 col-xs-6 text-left">
		<a href="<?= base_url() ?>">
			<img src="<?= base_url().ASSETS_IMG.'logo-cabaran.png' ?>" width="80%"/>
		</a>
	</div>
	<div class="col-md-5 col-sm-6 col-md-offset-4 col-xs-6 text-right">
		<div class="col-md-3 col-sm-3 col-xs-3 no-padding margin-top-30 col-md-offset-2">
			<img src="<?= base_url().ASSETS_IMG.'logo-ict.png' ?>" width="80%"/>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-4 no-padding">
			<img src="<?= base_url().ASSETS_IMG.'logo-johor.png' ?>" width="90%"/>
		</div>
		<div class="col-md-3 col-sm-3 col-xs-3 no-padding margin-top-30">
			<img src="<?= base_url().ASSETS_IMG.'logo-ytr.png' ?>" width="80%"/>
		</div>
	</div>
</div>