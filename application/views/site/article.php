<div class="col-md-12 col-sm-12 col-xs-12 container">
	<div class="col-md-6 col-sm-12 col-xs-12">
		<?php $this->load->view('partial/logo_front') ?>
	</div>
	<div class="col-md-5 col-sm-8 col-xs-12 margin-top-100 nano" style="height:500px">
		<div class="col-md-12 bg-white-trans nano-content">
			<div class="col-md-12 text-left margin-btm-20">
				<h2><?= $article['title'] ?></h2>
			</div>
			<div class="col-md-12 padding-btm-20">
				<?= $article['content'] ?>
			</div>
		</div>
	</div>
</div>


