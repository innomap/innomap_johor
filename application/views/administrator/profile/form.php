<script type="text/javascript">
	var alert = "<?= $alert ?>",
		userId = "<?= $this->userdata['id'] ?>";
</script>
<?php $this->load->view('partial/logo') ?>
<div class="col-md-8 col-md-offset-2 bg-light-grey">
	<div class="col-md-12 text-center margin-btm-20">
		<h2><?= lang('edit_profile') ?></h2>
	</div>

	<div class="col-md-12 alert alert-info hide alert-dismissable">
	    <i class="fa fa-info"></i>
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <?= $alert ?>
	</div>

	<form id="form-profile" class="form-horizontal" action="<?= base_url().PATH_TO_ADMIN.'profile/save' ?>" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="id" value="<?= isset($user) ? $user['user_id'] : '' ?>" />
			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('username') ?></label>
				<div class="col-md-9">
					<input type="text" name="username" class="form-control" placeholder="<?= lang('username') ?>" value="<?= isset($user) ? $user['username'] : '' ?>" readonly>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button type="button" class="btn btn-default btn-change-pass"><?= lang('change_password') ?></button>
				</div>
			</div>

			<div class="collapse-group collapse">
				<div class="form-group pass-group">
					<label class="col-md-3 control-label"><?= lang('new_password') ?></label>
					<div class="col-md-9">
						<input type="password" class="form-control" name="password" placeholder="<?= lang('new_password') ?>">
					</div>
				</div>

				<div class="form-group pass-group">
					<label class="col-md-3 control-label"><?= lang('retype_new_password') ?></label>
					<div class="col-md-9">
						<input type="password" class="form-control" name="retype_new_password" placeholder="<?= lang('retype_password') ?>">	
					</div>
				</div>
			</div>	

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('role') ?></label>
				<div class="col-md-9">
					<select class="form-control" name="role_id" disabled>
						<?php foreach ($role as $key => $value) { ?>
							<option value="<?= $key ?>" <?= (isset($user) ? ($user['role_id'] == $key ? 'selected' : '') : '') ?>><?= $value ?></option>
						<?php } ?>
					</select>
				</div>
			</div>				

		<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('name') ?></label>
				<div class="col-md-9">
					<input type="text" class="form-control" name="name" placeholder="<?= lang('name') ?>" value="<?= isset($user) ? $user['name'] : '' ?>">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('ic_number') ?></label>
				<div class="col-md-9">
					<div class="input-group">
                                                <input type="text" class="form-control number-only" maxlength="6" name="ic_num_1" placeholder="<?= lang('ic_number') ?>" value="<?= isset($user) ? $user['ic_num_1'] : '' ?>">
						<span class="input-group-addon">-</span>
						<input type="text" class="form-control number-only" maxlength="2" name="ic_num_2" value="<?= isset($user) ? $user['ic_num_2'] : '' ?>">
						<span class="input-group-addon">-</span>
						<input type="text" class="form-control number-only" maxlength="4" name="ic_num_3" value="<?= isset($user) ? $user['ic_num_3'] : '' ?>">
					</div>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('gender') ?></label>
				<div class="col-md-9">
					<?php foreach ($gender as $key => $value) { ?>
						<div class="radio"> 
							<label> <input type="radio" name="gender" value="<?= $key ?>" <?= (isset($user) ? ($key == $user['gender'] ? 'checked' : '') : $key == 0 ? 'checked' : '') ?>> <?= $value ?> </label> 
						</div>
					<?php } ?>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('position') ?></label>
				<div class="col-md-9">
					<input type="text" class="form-control" name="position" placeholder="<?= lang('position') ?>" value="<?= isset($user) ? $user['position'] : '' ?>">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('department') ?></label>
				<div class="col-md-9">
					<input type="text" class="form-control" name="department" placeholder="<?= lang('department') ?>" value="<?= isset($user) ? $user['department'] : '' ?>">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('address') ?></label>
				<div class="col-md-9">
					<textarea class="form-control" name="address" placeholder="<?= lang('address') ?>"><?= isset($user) ? $user['address'] : '' ?></textarea>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('postcode') ?></label>
				<div class="col-md-9">
                                        <input type="number" min="79000" max="89999" class="form-control" name="postcode" placeholder="<?= lang('postcode') ?>" value="<?= isset($user) ? $user['postcode'] : '' ?>">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('district') ?></label>
				<div class="col-md-9">
					<select class="form-control" name="district_id" disabled>
						<option value="">-- <?= lang('select_district') ?> --</option>
						<?php foreach ($districts as $key => $value) { ?>
							<option value="<?= $value['id'] ?>" <?= (isset($user) ? ($user['district_id'] == $value['id'] ? 'selected' : '') : '') ?>><?= $value['name'] ?></option>
						<?php } ?>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('state') ?></label>
				<div class="col-md-9">
					<input type="text" class="form-control" name="state" placeholder="<?= lang('state') ?>" value="Johor" readonly>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('telp_no') ?></label>
				<div class="col-md-9">
					<input type="text" class="form-control number-only" name="telp_no" placeholder="<?= lang('telp_no') ?>" value="<?= isset($user) ? $user['telp_no'] : '' ?>">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('telp_home_no') ?></label>
				<div class="col-md-9">
					<input type="text" class="form-control number-only" name="telp_home_no" placeholder="<?= lang('telp_home_no') ?>" value="<?= isset($user) ? $user['telp_home_no'] : '' ?>">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('email') ?></label>
				<div class="col-md-9">
					<input type="text" class="form-control" name="email" placeholder="<?= lang('email') ?>" value="<?= isset($user) ? $user['email'] : '' ?>">
				</div>
			</div>

		<div class="col-md-12 text-center">
			<input type="submit" name="btn_save" id="btn-save" data-id="btn_save" class="btn btn-dark-turquoise" value="<?= lang('save') ?>">
		</div>
	</form>
</div>