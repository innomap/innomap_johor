<script type="text/javascript">
	var lang_delete = "<?= lang('delete') ?>",
		lang_delete_user = "<?= lang('delete_user') ?>",
		lang_cancel = "<?= lang('cancel') ?>",
		lang_delete_confirm_message = "<?= lang('delete_confirm_message') ?>",
		edit_mode = "<?= (isset($user) ? 1 : 0) ?>";
</script>
<?php $this->load->view('partial/logo') ?>
<div class="col-md-8 col-md-offset-2 content-box bg-light-grey">
	<div class="col-md-12 text-center">
		<h2><?= isset($user) ? lang('edit_user') : lang('add_user') ?></h2>
    </div>
	<form role="form" class="form-horizontal" id="form-user" action="<?= site_url(PATH_TO_ADMIN.'users/'.$form_action.'/') ?>" method="POST">
		<input type="hidden" name="id" value="<?= isset($user) ? $user['user_id'] : '' ?>" />
			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('username') ?></label>
				<div class="col-md-9">
					<input type="text" name="username" class="form-control" placeholder="<?= lang('username') ?>" value="<?= isset($user) ? $user['username'] : '' ?>">
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button type="button" class="btn btn-default hide btn-change-pass"><?= lang('change_password') ?></button>
				</div>
			</div>

			<div class="collapse-group">
				<div class="form-group pass-group">
					<label class="col-md-3 control-label"><?= lang('new_password') ?></label>
					<div class="col-md-9">
						<input type="password" class="form-control" name="password" placeholder="<?= lang('new_password') ?>">
					</div>
				</div>

				<div class="form-group pass-group">
					<label class="col-md-3 control-label"><?= lang('retype_new_password') ?></label>
					<div class="col-md-9">
						<input type="password" class="form-control" name="retype_password" placeholder="<?= lang('retype_new_password') ?>">	
					</div>
				</div>
			</div>	

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('role') ?></label>
				<div class="col-md-9">
					<select class="form-control" name="role_id">
						<?php foreach ($role as $key => $value) { ?>
							<option value="<?= $key ?>" <?= (isset($user) ? ($user['role_id'] == $key ? 'selected' : '') : '') ?>><?= $value ?></option>
						<?php } ?>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('district') ?></label>
				<div class="col-md-9">
					<select class="form-control" name="district_id">
						<option value="">-- <?= lang('select_district') ?> --</option>
						<?php foreach ($districts as $key => $value) { ?>
							<option value="<?= $value['id'] ?>" <?= (isset($user) ? ($user['district_id'] == $value['id'] ? 'selected' : '') : '') ?>><?= $value['name'] ?></option>
						<?php } ?>
					</select>
				</div>
			</div>

			<?php if(isset($user)){ ?>
				<div class="form-group">
					<label class="col-md-3 control-label"><?= lang('name') ?></label>
					<div class="col-md-9">
						<input type="text" class="form-control" name="name" placeholder="<?= lang('name') ?>" value="<?= isset($user) ? $user['name'] : '' ?>" readonly>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label"><?= lang('ic_number') ?></label>
					<div class="col-md-9">
						<div class="input-group">
							<input type="text" class="form-control" name="ic_num_1" placeholder="<?= lang('ic_number') ?>" value="<?= isset($user) ? $user['ic_num_1'] : '' ?>" readonly>
							<span class="input-group-addon">-</span>
							<input type="text" class="form-control" name="ic_num_2" value="<?= isset($user) ? $user['ic_num_2'] : '' ?>" readonly>
							<span class="input-group-addon">-</span>
							<input type="text" class="form-control" name="ic_num_3" value="<?= isset($user) ? $user['ic_num_3'] : '' ?>" readonly>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label"><?= lang('gender') ?></label>
					<div class="col-md-9">
						<?php foreach ($gender as $key => $value) { ?>
							<div class="radio"> 
								<label> <input type="radio" name="gender" value="<?= $key ?>" <?= (isset($user) ? ($key == $user['gender'] ? 'checked' : '') : $key == 0 ? 'checked' : '') ?> disabled> <?= $value ?> </label> 
							</div>
						<?php } ?>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label"><?= lang('position') ?></label>
					<div class="col-md-9">
						<input type="text" class="form-control" name="position" placeholder="<?= lang('position') ?>" value="<?= isset($user) ? $user['position'] : '' ?>" readonly>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label"><?= lang('department') ?></label>
					<div class="col-md-9">
						<input type="text" class="form-control" name="department" placeholder="<?= lang('department') ?>" value="<?= isset($user) ? $user['department'] : '' ?>" readonly>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label"><?= lang('address') ?></label>
					<div class="col-md-9">
						<textarea class="form-control" name="address" placeholder="<?= lang('address') ?>" disabled><?= isset($user) ? $user['address'] : '' ?></textarea>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label"><?= lang('postcode') ?></label>
					<div class="col-md-9">
						<input type="text" class="form-control" name="postcode" placeholder="<?= lang('postcode') ?>" value="<?= isset($user) ? $user['postcode'] : '' ?>" readonly>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label"><?= lang('telp_no') ?></label>
					<div class="col-md-9">
						<input type="text" class="form-control" name="telp_no" placeholder="<?= lang('telp_no') ?>" value="<?= isset($user) ? $user['telp_no'] : '' ?>" readonly>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label"><?= lang('telp_home_no') ?></label>
					<div class="col-md-9">
						<input type="text" class="form-control" name="telp_home_no" placeholder="<?= lang('telp_home_no') ?>" value="<?= isset($user) ? $user['telp_home_no'] : '' ?>" readonly>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label"><?= lang('email') ?></label>
					<div class="col-md-9">
						<input type="text" class="form-control" name="email" placeholder="<?= lang('email') ?>" value="<?= isset($user) ? $user['email'] : '' ?>" readonly>
					</div>
				</div>
			<?php } ?>
			
			<div class="form-group text-center">
				<button type="button" class="btn btn-grey btn-cancel"><?= lang('cancel') ?></button>
				<button type="submit" class="btn btn-dark-turquoise flat"><?= lang('save') ?></button>
			</div>
		</form>
</div>