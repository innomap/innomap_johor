<script type="text/javascript">
	var alert = "<?=$alert?>";
	var lang_delete = "<?= lang('delete') ?>",
		lang_delete_user = "<?= lang('delete_user') ?>",
		lang_cancel = "<?= lang('cancel') ?>",
		lang_delete_confirm_message = "<?= lang('delete_confirm_message') ?>";
</script>

<div class="col-md-12">
	<?php $this->load->view('partial/logo') ?>
	<div class="col-md-12">
		<a href="<?= base_url().PATH_TO_ADMIN.'innovators/export_innovator' ?>">
			<button type="button" class="btn btn-dark-turquoise flat" id="export-application-num">Export to Excel</button>
		</a>
    </div>
    <div class="col-md-12 table-responsive content-box">
		<div class="alert alert-info alert-dismissable hide">
		    <i class="fa fa-info"></i>
		    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		    <?= $alert ?>
		</div>
		<table id="table-innovator" class="table table-bordered table-striped table-dark">
		    <thead>
		    <tr>
				<th>No</th>
				<th><?= lang('name') ?></th>
				<th><?= lang('application_type') ?></th>
				<th><?= lang('ic_number') ?></th>
				<th><?= lang('district') ?></th>
				<th><?= lang('action') ?></th>
		    </tr>
		    </thead>
		    <tbody>
		    <?php $no = 1; foreach ($innovators as $key=>$value) { ?>
			<tr>
			    <td><?= $no ?></td>
			    <td><?= $value['name'] ?></td>
			    <td><?= $application_type[$value['application_type']] ?></td>
			    <td><?= $value['ic_number'] ?></td>
			    <td><?= $value['district_name'] ?></td>
			    <td>
					<button type="button" class="btn btn-default btn-action btn-view-innovator" title="<?= lang('view') ?>" data-id="<?= $value['user_id'] ?>">
					    <span class="fa fa-search fa-lg"></span>
					</button>
					<button type="button" class="btn btn-default btn-action btn-reset-password" title="<?= lang('reset_password') ?>" data-id="<?= $value['user_id'] ?>">
                                                <span class="fa fa-lock fa-lg"></span><span>    Ubah Kata Laluan</span>
					</button>
					<?php if($this->userdata['role_id'] == USER_ROLE_YIM_SUPER_ADMIN){
						if($value['requested_to_delete'] == 0){ ?>
							<button type="button" class="btn btn-default btn-action btn-delete-innovator text-red" title="<?= lang('delete') ?>" data-id="<?= $value['user_id'] ?>"
							data-name="<?= $value['name'] ?>">
							    <span class="fa fa-trash-o fa-lg"></span>
							</button>
					<?php }else if($value['requested_to_delete'] == 1 && $value['requested_to_delete_by'] != $this->userdata['id']){ ?>
							<button type="button" class="btn btn-default btn-action btn-delete-innovator text-yellow" title="<?= lang('confirm_delete') ?>" data-id="<?= $value['user_id'] ?>"
							data-name="<?= $value['name'] ?>">
							    <span class="fa fa-trash-o fa-lg"></span>
							</button>
					<?php } ?>
					<?php } ?>
			    </td>
			</tr>
		    <?php $no++;} ?>
		    </tbody>
		</table>
    </div>
</div>