<script type="text/javascript">
	var alert = "<?=$alert?>";
</script>
<div class="col-md-12">
	<?php $this->load->view('partial/logo') ?>
    <div class="col-md-6 table-responsive content-box">
		<div class="alert alert-info alert-dismissable hide">
		    <i class="fa fa-info"></i>
		    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		    <?= $alert ?>
		</div>
		<form id="form-application-num-by-district" action="<?= base_url().PATH_TO_ADMIN.'reports/export_application_number_by_district' ?>" method="post">
			<div class="form-group col-md-12 no-padding">
				<div class="col-md-3 no-padding">
					<button type="submit" class="btn btn-success flat" id="export-application-num">Export to Excel</button>
				</div>
			</div>
		</form>
		<table id="table-list" class="table table-bordered table-striped table-dark">
		    <thead>
		    <tr>
				<th width="10%">No</th>
				<th><?= lang('district') ?></th>
				<th width="30%"><?= lang('count_submission') ?></th>
		    </tr>
		    </thead>
		    <tbody>
		    <?php $no = 1; foreach ($data as $key=>$value) { ?>
			<tr>
			    <td><?= $no ?></td>
			    <td><?= $value['district'] ?></td>
			    <td><?= $value['count'] ?></td>
			</tr>
		    <?php $no++;} ?>
		    </tbody>
		</table>
    </div>

    <div class="col-md-6">
    	<div class="col-md-6 col-md-offset-3 text-center">
			<div class="panel panel-default">
				<div class="panel-body">
			  		<h4><?= lang('total_application') ?></h4>
			    	<h1><?= $total_application ?></h1>
				</div>
			</div>		
		</div>
		<div class="col-md-12">
			<div class="col-md-6 text-center">
				<div class="panel panel-default">
					<div class="panel-body">
				  		<h4><?= lang('total_approved_application') ?></h4>
				    	<h2><?= $total_approved_application ?></h2>
					</div>
				</div>		
			</div>
			<div class="col-md-6 text-center">
				<div class="panel panel-default">
					<div class="panel-body">
				  		<h4><?= lang('total_rejected_application') ?></h4>
				    	<h2><?= $total_rejected_application ?></h2>
					</div>
				</div>		
			</div>
		</div>
    </div>
</div>