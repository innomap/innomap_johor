<div class="modal fade" id="modal-assign" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= lang('cancel') ?></span></button>
				<h4 class="modal-title"><?= lang('assign_to_evaluator') ?></h4>
			</div>
			<form role="form" id="form-assign" action="<?= site_url(PATH_TO_ADMIN.'applications/assign_evaluation/') ?>" method="POST">
			<input type="hidden" name="id" />
				<div class="modal-body">
					<!-- text input -->
					<input type="hidden" name="application_id">
					<div class="form-group">
						<label><?= lang('select_group').' :' ?></label>
						<select name="group" class="form-control">
						<?php foreach ($groups as $key => $value) { ?>
							<option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
						<?php } ?>
						</select>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default flat" data-dismiss="modal"><?= lang('cancel') ?></button>
					<button type="submit" class="btn btn-success flat"><?= lang('save') ?></button>
				</div>
			</form>
		</div>
	</div>
</div>