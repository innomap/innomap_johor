<script type="text/javascript">
	var alert = "<?=$alert?>";
	var lang_approve = "<?= lang('approve') ?>",
		lang_approve_confirm_message = "<?= lang('approve_confirm_message') ?>",
		lang_reject = "<?= lang('reject') ?>",
		lang_reject_confirm_message = "<?= lang('reject_confirm_message') ?>",
		lang_cancel = "<?= lang('cancel') ?>";
</script>
<div class="col-md-12">
	<?php $this->load->view('partial/logo') ?>
	 <div class="col-md-12">
		<button type="button" class="btn btn-grey print-page">
			<span class="fa fa-print"></span> <?= lang('print') ?>
		</button>
		<a href="<?= base_url().PATH_TO_ADMIN.'applications/export_application' ?>">
			<button type="button" class="btn btn-dark-turquoise flat" id="export-application-num">Export to Excel</button>
		</a>
    </div>
    <div class="col-md-12 table-responsive content-box">
		<div class="alert alert-info alert-dismissable hide">
		    <i class="fa fa-info"></i>
		    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		    <?= $alert ?>
		</div>
		<form id="form-application" action="<?= base_url().PATH_TO_ADMIN.'applications/proceed_to_next_round' ?>" method="post">
			<table id="table-application" class="table table-bordered table-striped table-dark">
			    <thead>
			    <tr>
			    	<th></th>
					<th>No</th>
					<th><?= lang('innovator') ?></th>
					<th><?= lang('project_title') ?></th>
					<th><?= lang('description') ?></th>
					<th><?= lang('category') ?></th>
					<th><?= lang('district') ?></th>
					<th><?= lang('status') ?></th>
					<th><?= lang('average_score') ?></th>
					<th class="print-action"><?= lang('action') ?></th>
			    </tr>
			    </thead>
			    <tbody>
			    <?php $no = 1; foreach ($applications as $key=>$value) { ?>
				<tr>
					<td><input type="checkbox" name="ids[]" <?= ($value['status'] == APPLICATION_STATUS_EVALUATED ? '' : 'disabled') ?> value="<?= $value['id'] ?>"></td>
				    <td><?= $no ?></td>
				    <td><?= $value['innovator_name'] ?></td>
				    <td><?= $value['title'] ?></td>
				    <td><?= ellipsis($value['description'], 100) ?></td>
				    <td><?= $innovation_category[$value['innovation_category']] ?></td>
				    <td><?= $value['district_name'] ?></td>
				    <td class="<?= $status[$value['status']]['class'] ?>"><?= $status[$value['status']]['name'] ?></td>
				    <td><?= $value['average_score'] ?></td>
				    <td class="print-action">
					<button type="button" class="btn btn-default btn-action btn-view-application" title="<?= lang('view') ?>" data-id="<?= $value['id'] ?>">
					    <span class="fa fa-search fa-lg"></span>
					</button>

					<?php if($this->userdata['role_id'] != USER_ROLE_DISTRICT_OFFICER){ ?>
						<?php if($value['status'] > APPLICATION_STATUS_REJECTED){ ?>
							<button type="button" class="btn btn-default btn-action btn-view-evaluation-list" title="<?= lang('view_evaluation_result') ?>" data-id="<?= $value['id'] ?>">
							    <span class="fa fa-files-o fa-lg"></span>
							</button>
						<?php } ?>

						<?php if($value['status'] == APPLICATION_STATUS_APPROVED){ ?>
							<button type="button" class="btn btn-default btn-action btn-assign-application" title="<?= lang('assign_to_evaluator') ?>" data-id="<?= $value['id'] ?>">
								<span class="fa fa-user fa-lg"></span>
							</button>
						<?php } ?>
					<?php }else{ ?>
							<?php if($value['status'] == APPLICATION_STATUS_SENT_APPROVAL){ ?>
								<button type="button" class="btn btn-success btn-action btn-approve" title="<?= lang('approve') ?>" data-id="<?= $value['id'] ?>" data-name="<?= $value['title'] ?>">
								    <span class="fa fa-check fa-lg"></span>
								</button>

								<button type="button" class="btn btn-danger btn-action btn-reject" title="<?= lang('reject') ?>" data-id="<?= $value['id'] ?>" data-name="<?= $value['title'] ?>">
								    <span class="fa fa-times fa-lg"></span>
								</button>
					<?php 	}
						} ?>
	                    <?php if($value['status'] == APPLICATION_STATUS_APPROVED || $value['status'] == APPLICATION_STATUS_REJECTED ){ ?>
							<button type="button" class="btn btn-default btn-action btn-comment" title="Lihat ulasan" data-id="<?= $value['id'] ?>" data-comment="<?= $value['comment'] ?>">
								<span class="fa fa-comment-o fa-lg"></span>
							</button>
	                    <?php } ?>
	                    <?php if($this->userdata['role_id'] == USER_ROLE_YIM_SUPER_ADMIN || $this->userdata['role_id'] == USER_ROLE_SUPER_ADMIN){ ?>
			                <button type="button" class="btn btn-default btn-action btn-download-pdf" title="Download pdf" data-id="<?= $value['id'] ?>" >
	                            <span class="fa fa-download fa-lg"></span> 
	                    	</button>

	                    	<button type="button" class="btn btn-default btn-action btn-print-pdf" title="Print pdf" data-id="<?= $value['id'] ?>" >
	                            <span class="fa fa-print fa-lg"></span> 
	                    	</button>
	                    <?php } ?>
				    </td>
				</tr>
			    <?php $no++;} ?>
			    </tbody>
			</table>
			<?php if(count($applications) > 0){ ?>
				<div class="form-group">
					<label><?= lang('with_selected')." : " ?></label>
					<input type="submit" value="Kemaskini status : Berjaya" data-id="btn-proceed" class="btn btn-success">
				</div>
			<?php } ?>
		</form>
    </div>
</div>
<?= $form_assign ?>

<iframe id="iframeprint" style="display:none"></iframe>