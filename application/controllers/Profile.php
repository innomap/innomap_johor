<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/Common.php');
class Profile extends Common {

    function __construct() {
        parent::__construct();

        $this->load->model('innovator');
        $this->load->model('user');
        $this->load->model('district');
        $this->load->model('innovator_expert_team');

        $this->title = "Edit Profile";
        $this->menu = "profile";

        $this->lang->load('account',$this->language);

        $this->scripts[] = 'plugins/fancybox/jquery.fancybox.pack';
        $this->scripts[] = 'site/profile';

        $this->styles[] = 'fancybox/jquery.fancybox';
    }

    public function index(){
        $data['alert'] = $this->session->flashdata('alert');
        $data['innovator'] = $this->innovator->get_one_join($this->userdata['id']);
        if($data['innovator']){
            $split_ic_num = explode("-", $data['innovator']['ic_number']);
            $data['innovator']['ic_num_1'] = $split_ic_num[0];
            $data['innovator']['ic_num_2'] = $split_ic_num[1];
            $data['innovator']['ic_num_3'] = $split_ic_num[2];
            $data['innovator']['team_experts'] = $this->innovator_expert_team->find("user_id = ".$data['innovator']['user_id']);
        }
        $data['application_types'] = unserialize(APPLICATION_TYPE);
        $data['gender'] = unserialize(GENDER);
        $data['districts'] = $this->district->find_all();
        $this->load->view('profile/form',$data);
    }

    function save(){
        $this->layout = FALSE;
        $status = 0;

        $postdata = $this->postdata();

        if($postdata['name'] != "" && $postdata['district_id'] != ""){
            if($postdata['password'] != ""){
                $user = $this->user->find_one("id = ".$this->userdata['id']);
                $data = array('username' => $user['username']);
                $data['password'] = $postdata['password'];

                if($this->user->update_user($this->userdata['id'], $data)){
                    $this->session->set_flashdata('alert','Data has been saved.');
                }else{
                    $this->session->set_flashdata('alert','Sorry, an error occurred, please try again later.');
                }
            }
            
            $innovator = array( "name" => $postdata['name'],
                                "application_type" => $postdata['application_type'],
                                "ic_number" => $postdata['ic_num_1']."-".$postdata['ic_num_2']."-".$postdata['ic_num_3'],
                                "gender" => $postdata['gender'],
                                "address" => $postdata['address'],
                                "postcode" => $postdata['postcode'],
                                "district_id" => $postdata['district_id'],
                                "telp_home_no" => $postdata['telp_home_no'],
                                "telp_no" => $postdata['telp_no'],
                                "email" => $postdata['email'],
                                "latitude" => $postdata['latitude'],
                                "longitude" => $postdata['longitude']);
            //save photo
            if(isset($_FILES['innovator_photo'])){
                if($_FILES['innovator_photo']['name'] != NULL){
                    $photo_filename = $this->generate_filename($this->userdata['id'],$_FILES['innovator_photo']['name']);
                    $uploaded_photo = $this->_upload($photo_filename,'innovator_photo',PATH_TO_INNOVATOR_PHOTO, PATH_TO_INNOVATOR_PHOTO_THUMB);
                
                    $innovator['photo'] = $uploaded_photo;
                }
            }

            if($this->innovator->update($this->userdata['id'], $innovator)){
                $this->save_team_member($this->userdata['id'], $postdata);
                $status = 1;
                $this->session->set_flashdata('alert','Data has been saved.');
            }else{
                $this->session->set_flashdata('alert','Sorry, an error occurred, please try again later.');    
            }
            
        }

        redirect(base_url().'profile');
    }

    private function _upload($name,$attachment,$upload_path, $thumb_path = NULL) {
        $this->load->library('upload');
        $config['file_name']        = $name;
        $config['upload_path']      = $upload_path;
        $config['allowed_types']    = 'png|jpg|gif|bmp|jpeg';
        $config['remove_spaces']    = TRUE;
        
        $this->upload->initialize($config);
        if(!$this->upload->do_upload($attachment,true)) {
            echo $this->upload->display_errors();
            return false;
        }else{
            $upload_data = $this->upload->data();
            if($thumb_path != NULL){
                $this->create_thumbnail($upload_data['image_width'],$upload_data['image_height'],$upload_path,$upload_data['file_name'], $thumb_path);
            }
            return $upload_data['file_name'];
        }
    }

    private function create_thumbnail($image_width,$image_height,$upload_path,$file_name,$new_path){
        $this->load->library('image_lib');
        
        if ($image_width > 1024 || $image_height > 1024) {
            $config['image_library'] = 'gd2';
            $config['source_image'] = $upload_path.$file_name;
            $config['new_image'] = $new_path.$file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 1024;
            $config['height'] = 1024;

            $this->image_lib->clear();
            $this->image_lib->initialize($config); 

            if ( ! $this->image_lib->resize()){
                echo $this->image_lib->display_errors();
                return false;
            }
        }else{
            $config['image_library'] = 'gd2';
            $config['source_image'] = $upload_path.$file_name;
            $config['new_image'] = $new_path.$file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = $image_width;
            $config['height'] = $image_height;

            $this->image_lib->clear();
            $this->image_lib->initialize($config); 

            if ( ! $this->image_lib->resize()){
                echo $this->image_lib->display_errors();
                return false;
            }
        }
    }

    private function generate_filename($id, $filename){
        $attachment_name = preg_replace('/\s+/', '_', $filename);
        $attachment_name = str_replace('.', '_', $attachment_name);
        $retval = $id."_".$attachment_name;

        return $retval;
    }

    private function _remove($file_name,$path){
        if (file_exists(realpath(APPPATH . '../'.$path) . DIRECTORY_SEPARATOR . $file_name)) {
            unlink("./".$path."/".$file_name);
        }
    }

    private function postdata(){
        if($post = $this->input->post()){
            return $post;
        }
        redirect('accounts');
    }

    private function save_team_member($id, $postdata){
        $this->load->model('innovator_expert_team');

        foreach ($postdata['team_member_id'] as $key => $value) {
            if(isset($postdata['i_deleted_team_member_'.$key])){
                $data_member = array('user_id' => $id,
                            'name' => $postdata['team_member_'.$key],
                            'ic_number' => $postdata['team_member_ic_num_'.$key]);

                if(isset($_FILES['team_member_pic_'.$key])){
                    if($_FILES['team_member_pic_'.$key]['name'] != NULL){
                        if($postdata['h_team_member_pic_'.$key] != ""){
                            $this->_remove($postdata['h_team_member_pic_'.$key],PATH_TO_TEAM_MEMBER_PHOTO);
                            $this->_remove($postdata['h_team_member_pic_'.$key],PATH_TO_TEAM_MEMBER_PHOTO);
                        }
                        $photo_filename = $this->generate_filename($id,$_FILES['team_member_pic_'.$key]['name']);
                        $uploaded_photo = $this->_upload($photo_filename,'team_member_pic_'.$key,PATH_TO_TEAM_MEMBER_PHOTO, PATH_TO_TEAM_MEMBER_PHOTO_THUMB);
                        
                        $data_member['photo'] = $uploaded_photo;
                    }
                }
                if($postdata['team_member_id'][$key] > 0){
                    $this->innovator_expert_team->update($postdata['team_member_id'][$key], $data_member);
                }else{
                    $this->innovator_expert_team->insert($data_member);
                }
            }else{
                if($postdata['team_member_id'][$key] > 0){
                    $team_member = $this->innovator_expert_team->find_by_id($postdata['team_member_id'][$key]);
                    $this->_remove($team_member['photo'],PATH_TO_TEAM_MEMBER_PHOTO);
                    $this->innovator_expert_team->delete($postdata['team_member_id'][$key]);
                }
            }
        }
    }
}
