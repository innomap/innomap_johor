<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/Common.php');

class Applications extends Common {

        function __construct() {
                parent::__construct();

                $this->load->model('application');
                $this->load->model('innovator');
                $this->load->model('application_picture');
                $this->load->model('round');
                $this->load->model('district');
                $this->load->model('innovator_expert_team');

                $this->load->library('upload');

                $this->title = "Application";
                $this->menu = "application";

                $this->lang->load('application', $this->language);

                $this->scripts[] = 'plugins/fancybox/jquery.fancybox.pack';
                $this->scripts[] = 'jquery.form';
                $this->scripts[] = 'plugins/bootstrap-datepicker/bootstrap-datepicker.min';
                $this->scripts[] = 'site/application';

                $this->styles[] = 'fancybox/jquery.fancybox';
                $this->styles[] = 'bootstrap-datepicker/bootstrap-datepicker.min';
        }

        public function index() {
                $this->load->helper("mystring_helper");

                $data['alert'] = $this->session->flashdata('alert');
                $data['alert_dialog'] = $this->session->flashdata('alert_dialog');
                $data['applications'] = $this->application->find("user_id = " . $this->userdata['id']);
                foreach ($data['applications'] as $key => $value) {
                        $data['round'] = $this->round->find_by_id($value['round_id']);
                }
                $data['status'] = unserialize(APPLICATION_STATUS);
                $this->load->view('application/list', $data);
        }

        function add() {
                $data['action_url'] = 'applications/store';
                $data['alert'] = $this->session->flashdata('alert');
                $data['innovator'] = $this->innovator->get_one_join($this->userdata['id']);
                if ($data['innovator']) {
                        $split_ic_num = explode("-", $data['innovator']['ic_number']);
                        $data['innovator']['ic_num_1'] = $split_ic_num[0];
                        $data['innovator']['ic_num_2'] = $split_ic_num[1];
                        $data['innovator']['ic_num_3'] = $split_ic_num[2];
                        $data['innovator']['team_experts'] = $this->innovator_expert_team->find("user_id = " . $data['innovator']['user_id']);
                }
                $data['view_mode'] = 0;
                $data['round'] = $this->round->find_by_id(1);
                $data['application_types'] = unserialize(APPLICATION_TYPE);
                $data['gender'] = unserialize(GENDER);
                $data['districts'] = $this->district->find_all();
                $data['innovation_category'] = unserialize(INNOVATION_CATEGORY);
                $data['targets'] = unserialize(INNOVATION_TARGET);
                $data['troubleshootings'] = unserialize(TROUBLESHOOTING_OPTION);
                $data['estimated_market_size'] = unserialize(ESTIMATED_MARKET_SIZE);
                $data['income'] = unserialize(INCOME_OPTION);

                $this->load->view('application/form', $data);
        }

        function store() {
                $this->load->model('application_status_history');
                $this->layout = FALSE;

                $postdata = $this->postdata();

                $data_application = $this->generate_data_application($postdata);
                $data_application['created_at'] = date('Y-m-d H:i:s');
                if ($id = $this->application->insert($data_application)) {
                        //save picture
                        $pict_num = count($_FILES);
                        for ($i = 0; $i <= $pict_num; $i++) {
                                if (isset($_FILES['picture_' . $i])) {
                                        if ($_FILES['picture_' . $i]['name'] != NULL) {
                                                $filename = $this->generate_filename($id, $_FILES['picture_' . $i]['name']);
                                                $uploaded_picture = $this->_upload($filename, 'picture_' . $i, PATH_TO_APPLICATION_PICTURE, PATH_TO_APPLICATION_PICTURE_THUMB);
                                                $this->application_picture->insert(array('application_id' => $id, 'name' => $uploaded_picture));
                                        }
                                }
                        }

                        //save history
                        $this->application_status_history->insert(array('application_id' => $id, 'round_id' => 1, 'status' => APPLICATION_STATUS_DRAFT, 'user_id' => $this->userdata['id']));
                        if (isset($_POST['btn_send_for_approval'])) {
                                $this->application->update($id, array('status' => APPLICATION_STATUS_SENT_APPROVAL, 'submission_date' => date('Y-m-d H:i:s')));
                                $this->application_status_history->insert(array('application_id' => $id, 'round_id' => 1, 'status' => APPLICATION_STATUS_SENT_APPROVAL, 'user_id' => $this->userdata['id']));
                        }

                        if (isset($_POST['btn_send_for_approval'])) {
                                $this->session->set_flashdata('alert_dialog', $id);
                                $redirect_url = base_url() . 'applications';
                        } else {
                                $redirect_url = base_url() . 'applications/edit/' . $id;
                        }

                        echo json_encode(array('redirect_url' => $redirect_url));
                } else {
                        echo json_encode(array('redirect_url' => base_url() . 'applications'));
                }
        }

        private function generate_data_application($postdata) {
                $postdata['innovation_category'] = (isset($postdata['innovation_category']) ? $postdata['innovation_category'] : 1);
                if($postdata['created_date'] != ""){
                    $created_date = DateTime::createFromFormat('d-m-Y', $postdata['created_date']);
                    $created_date = $created_date->format('Y-m-d');
                }else{
                    $created_date = "";
                }

                if($postdata['discovered_date'] != ""){
                    $discovered_date = DateTime::createFromFormat('d-m-Y', $postdata['discovered_date']);
                    $discovered_date = $discovered_date->format('Y-m-d');
                }else{
                    $discovered_date = "";
                }

                $data_application = array("user_id" => $this->userdata['id'],
                    "title" => $postdata['title'],
                    "idea" => $postdata['idea'],
                    "description" => $postdata['description'],
                    "material" => $postdata['material'],
                    "how_to_use" => $postdata['how_to_use'],
                    "special_achievement" => $postdata['special_achievement'],
                    "created_date" => $created_date,
                    "discovered_date" => $discovered_date,
                    "manufacturing_cost" => $postdata['manufacturing_cost'],
                    "selling_price" => $postdata['selling_price'],
                    "target" => (isset($postdata['target']) ? json_encode($postdata['target']) : ""),
                    "myipo_protection" => $postdata['myipo_protection'],
                    "innovation_category" => $postdata['innovation_category'],
                    "troubleshooting" => (isset($postdata['troubleshooting']) ? json_encode($postdata['troubleshooting']) : ""),
                    "estimated_market_size" => $postdata['estimated_market_size'],
                    "income" => $postdata['income'],
                    "round_id" => 1,
                    "status" => APPLICATION_STATUS_DRAFT,
                    "created_at" => date('Y-m-d H:i:s')
                );
                return $data_application;
        }

        private function postdata() {
                if ($post = $this->input->post()) {
                        return $post;
                }
                redirect('applications');
        }

        function edit($id, $view_mode = 0) {
                $data['action_url'] = 'applications/update';
                $data['alert'] = $this->session->flashdata('alert');

                if ($data['application'] = $this->application->find_by_id($id)) {
                        if ($data['application']['created_date'] != "0000-00-00") {
                                $tmp_date = DateTime::createFromFormat('Y-m-d', $data['application']['created_date']);
                                $data['application']['created_date'] = $tmp_date->format('d-m-Y');
                        }
                        if ($data['application']['discovered_date'] != "0000-00-00") {
                                $tmp_date = DateTime::createFromFormat('Y-m-d', $data['application']['discovered_date']);
                                $data['application']['discovered_date'] = $tmp_date->format('d-m-Y');
                        }

                        $data['i_picture'] = $this->application_picture->find("application_id = " . $id);
                        $data['i_target'] = ($data['application']['target'] != NULL ? json_decode($data['application']['target']) : array());
                        $data['i_troubleshooting'] = ($data['application']['troubleshooting'] != NULL ? json_decode($data['application']['troubleshooting']) : array());
                }

                $data['innovator'] = $this->innovator->get_one_join($this->userdata['id']);
                if ($data['innovator']) {
                        $split_ic_num = explode("-", $data['innovator']['ic_number']);
                        $data['innovator']['ic_num_1'] = $split_ic_num[0];
                        $data['innovator']['ic_num_2'] = $split_ic_num[1];
                        $data['innovator']['ic_num_3'] = $split_ic_num[2];
                        $data['innovator']['team_experts'] = $this->innovator_expert_team->find("user_id = " . $data['innovator']['user_id']);
                }
                $data['view_mode'] = $view_mode;
                $data['round'] = $this->round->find_by_id(1);
                $data['application_types'] = unserialize(APPLICATION_TYPE);
                $data['gender'] = unserialize(GENDER);
                $data['districts'] = $this->district->find_all();
                $data['innovation_category'] = unserialize(INNOVATION_CATEGORY);
                $data['targets'] = unserialize(INNOVATION_TARGET);
                $data['troubleshootings'] = unserialize(TROUBLESHOOTING_OPTION);
                $data['estimated_market_size'] = unserialize(ESTIMATED_MARKET_SIZE);
                $data['income'] = unserialize(INCOME_OPTION);

                $this->load->view('application/form', $data);
        }

        function update() {
                $this->load->model('application_status_history');
                $this->layout = FALSE;

                $postdata = $this->postdata();
                $id = $postdata['id'];

                $data_application = $this->generate_data_application($postdata);
                $get_app = $this->application->find_one("id = " . $this->db->escape($id));

                if ($get_app['user_id'] == $this->userdata['id']) {
                        if (isset($_POST['btn_send_for_approval']) || $get_app['status'] == APPLICATION_STATUS_SENT_APPROVAL) {
                                $data_application['submission_date'] = date('Y-m-d H:i:s');
                                $status = APPLICATION_STATUS_SENT_APPROVAL;
                        } else {
                                $status = APPLICATION_STATUS_DRAFT;
                        }

                        if ($this->application->update($postdata["id"], $data_application)) {
                                //save picture
                                if (isset($postdata['deleted_picture'])) {
                                        $deleted_pict = $postdata['deleted_picture'];
                                        $deleted_pict_name = $postdata['deleted_picture_name'];
                                        foreach ($deleted_pict as $key => $value) {
                                                if ($this->application_picture->delete($value)) {
                                                        $this->_remove($deleted_pict_name[$key], PATH_TO_APPLICATION_PICTURE);
                                                        $this->_remove($deleted_pict_name[$key], PATH_TO_APPLICATION_PICTURE_THUMB);
                                                }
                                        }
                                }

                                $pict_num = count($_FILES);
                                for ($i = 0; $i <= $pict_num; $i++) {
                                        if (isset($_FILES['picture_' . $i])) {
                                                if ($_FILES['picture_' . $i]['name'] != NULL) {
                                                        $picture_name = preg_replace('/\s+/', '_', $_FILES['picture_' . $i]['name']);
                                                        $filename = $id . "_" . $picture_name;
                                                        $uploaded_picture = $this->_upload($filename, 'picture_' . $i, PATH_TO_APPLICATION_PICTURE, PATH_TO_APPLICATION_PICTURE_THUMB);
                                                        $this->application_picture->insert(array('application_id' => $id, 'name' => $uploaded_picture));
                                                }
                                        }
                                }

                                if ($status == APPLICATION_STATUS_SENT_APPROVAL) {
                                        if ($this->application->update($id, array('status' => APPLICATION_STATUS_SENT_APPROVAL))) {
                                                $this->application_status_history->insert(array('application_id' => $id, 'round_id' => 1, 'status' => APPLICATION_STATUS_SENT_APPROVAL, 'user_id' => $this->userdata['id']));
                                        }
                                        $this->session->set_flashdata('alert_dialog', $id);
                                        $redirect_url = base_url() . 'applications';
                                } else {
                                        $this->session->set_flashdata('alert', 'Application has been updated.');
                                        $redirect_url = base_url() . 'applications/edit/' . $id;
                                }
                        } else {
                                $this->session->set_flashdata('alert', 'Appication cannot be updated.');
                                $redirect_url = base_url() . 'applications/edit/' . $id;
                        }
                } else {
                        $this->session->set_flashdata('alert', 'Appication cannot be updated.');
                        $redirect_url = base_url() . 'applications';
                }

                echo json_encode(array('redirect_url' => $redirect_url));
        }

        function delete($id) {
                $this->layout = FALSE;
                if ($this->application->delete($id)) {
                        $this->session->set_flashdata('alert', 'Application has been deleted.');
                } else {
                        $this->session->set_flashdata('alert', 'Application can not be deleted.');
                }

                redirect(base_url() . 'applications');
        }

        function view($id) {
                $this->edit($id, 1);
        }

        private function _upload($name, $attachment, $upload_path, $thumb_path = NULL, $is_file = 0) {
                $config['file_name'] = $name;
                $config['upload_path'] = $upload_path;
                if ($is_file == 0) {
                        $config['allowed_types'] = 'png|jpg|gif|bmp|jpeg';
                } else {
                        $config['allowed_types'] = 'png|jpg|gif|bmp|jpeg|pdf|doc';
                }

                $config['remove_spaces'] = TRUE;

                $this->upload->initialize($config);
                if (!$this->upload->do_upload($attachment, true)) {
                        echo $this->upload->display_errors();
                        return false;
                } else {
                        $upload_data = $this->upload->data();
                        if ($thumb_path != NULL) {
                                $this->create_thumbnail($upload_data['image_width'], $upload_data['image_height'], $upload_path, $upload_data['file_name'], $thumb_path);
                        }
                        return $upload_data['file_name'];
                }
        }

        private function _remove($file_name, $path) {
                if (file_exists(realpath(APPPATH . '../' . $path) . DIRECTORY_SEPARATOR . $file_name)) {
                        unlink("./" . $path . "/" . $file_name);
                }
        }

        private function create_thumbnail($image_width, $image_height, $upload_path, $file_name, $new_path) {
                $this->load->library('image_lib');

                if ($image_width > 1024 || $image_height > 1024) {
                        $config['image_library'] = 'gd2';
                        $config['source_image'] = $upload_path . $file_name;
                        $config['new_image'] = $new_path . $file_name;
                        $config['maintain_ratio'] = TRUE;
                        $config['width'] = 1024;
                        $config['height'] = 1024;

                        $this->image_lib->clear();
                        $this->image_lib->initialize($config);

                        if (!$this->image_lib->resize()) {
                                echo $this->image_lib->display_errors();
                                return false;
                        }
                } else {
                        $config['image_library'] = 'gd2';
                        $config['source_image'] = $upload_path . $file_name;
                        $config['new_image'] = $new_path . $file_name;
                        $config['maintain_ratio'] = TRUE;
                        $config['width'] = $image_width;
                        $config['height'] = $image_height;

                        $this->image_lib->clear();
                        $this->image_lib->initialize($config);

                        if (!$this->image_lib->resize()) {
                                echo $this->image_lib->display_errors();
                                return false;
                        }
                }
        }

        private function generate_filename($id, $filename) {
                $attachment_name = preg_replace('/\s+/', '_', $filename);
                $attachment_name = str_replace('.', '_', $attachment_name);
                $retval = $id . "_" . rand() . "_" . $attachment_name;

                return $retval;
        }

        function download_pdf($id) {
                $application = $this->application->find_by_id($id);
                if(!$application || !$application['pdf']){
                        exit;
                }
                $file = PATH_TO_APPLICATION_FILE . $application['pdf'];
                
                header('Content-type: application/pdf');
                header('Content-Disposition: attachment; filename="' . basename($file) . '"');
                header('Content-Transfer-Encoding: binary');
                readfile($file);
                exit;
        }

}
