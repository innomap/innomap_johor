<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/' . PATH_TO_ADMIN . '/Common.php');

class Applications extends Common {

        function __construct() {
                parent::__construct();

                $this->title = "Manage Application";
                $this->menu = "application";

                $this->load->model('application');
                $this->load->model('admin');
                $this->load->model('evaluation');
                $this->load->model('evaluator_group');
                $this->load->model("application_evaluator");
                $this->load->model("evaluator");

                $this->lang->load('application', $this->language);

                $this->scripts[] = 'plugins/fancybox/jquery.fancybox.pack';
                $this->scripts[] = 'administrator/application';

                $this->styles[] = 'fancybox/jquery.fancybox';
        }

        public function index() {
                $this->load->helper("mystring_helper");

                $data['alert'] = $this->session->flashdata('alert');
                if ($this->userdata['role_id'] == USER_ROLE_DISTRICT_OFFICER) {
                        $admin = $this->admin->find_one("user_id = " . $this->userdata['id']);
                        $applications = $this->application->get_join("status > " . APPLICATION_STATUS_DRAFT . " AND district_id = " . $admin['district_id'])->result_array();
                } else {
                        $applications = $this->application->get_join("status > " . APPLICATION_STATUS_DRAFT)->result_array();
                }

                foreach ($applications as $key => $value) {
                        if ($value['status'] == APPLICATION_STATUS_SENT_APPROVAL) {
                                $applications[$key]['is_evaluation_done'] = false;
                        } else {
                                $applications[$key]['is_evaluation_done'] = $this->application_evaluator->is_evaluation_done($value['id']);
                                if (!$applications[$key]['is_evaluation_done']) {
                                        $applications[$key]['status'] = APPLICATION_STATUS_ASSIGNED_TO_EVALUATOR;
                                }
                        }
                        $applications[$key]['average_score'] = $this->get_average_score($value['id']);
                        $get_evaluation = $this->evaluation->find_one("application_id = " . $value['id']);
                        $applications[$key]['has_evaluation'] = ($get_evaluation ? 1 : 0);
                }

                $data['applications'] = $applications;

                $data['status'] = unserialize(APPLICATION_STATUS_ADMIN);
                $data['innovation_category'] = unserialize(INNOVATION_CATEGORY);
                $data_assign['groups'] = $this->evaluator_group->find_all();
                $data['form_assign'] = $this->load->view(PATH_TO_ADMIN . 'application/form_assign', $data_assign, TRUE);

                $this->load->view(PATH_TO_ADMIN . 'application/list', $data);
        }

        function view($id, $view_mode = 1) {
                $this->load->model('application_picture');
                $this->load->model('innovator');
                $this->load->model('innovator_expert_team');
                $this->load->model('round');
                $this->load->model('district');

                $data['action_url'] = 'applications/update';
                $data['alert'] = $this->session->flashdata('alert');

                if ($data['application'] = $this->application->find_by_id($id)) {
                        $data['i_picture'] = $this->application_picture->find("application_id = " . $id);
                        $data['i_target'] = ($data['application']['target'] != NULL ? json_decode($data['application']['target']) : array());
                        $data['i_troubleshooting'] = ($data['application']['troubleshooting'] != NULL ? json_decode($data['application']['troubleshooting']) : array());
                        $data['innovator'] = $this->innovator->get_one_join($data['application']['user_id']);
                        if ($data['innovator']) {
                                $split_ic_num = explode("-", $data['innovator']['ic_number']);
                                $data['innovator']['ic_num_1'] = $split_ic_num[0];
                                $data['innovator']['ic_num_2'] = $split_ic_num[1];
                                $data['innovator']['ic_num_3'] = $split_ic_num[2];
                                $data['innovator']['team_experts'] = $this->innovator_expert_team->find("user_id = " . $data['innovator']['user_id']);
                        }
                }

                $data['view_mode'] = $view_mode;
                $data['round'] = $this->round->find_by_id(1);
                $data['application_types'] = unserialize(APPLICATION_TYPE);
                $data['gender'] = unserialize(GENDER);
                $data['districts'] = $this->district->find_all();
                $data['innovation_category'] = unserialize(INNOVATION_CATEGORY);
                $data['targets'] = unserialize(INNOVATION_TARGET);
                $data['troubleshootings'] = unserialize(TROUBLESHOOTING_OPTION);
                $data['estimated_market_size'] = unserialize(ESTIMATED_MARKET_SIZE);
                $data['income'] = unserialize(INCOME_OPTION);

                $this->load->view('application/form', $data);
        }

        private function get_average_score($application_id) {
                $data_score = $this->evaluation->get_average_score("application_id = " . $application_id)->row_array();

                return $data_score['score'];
        }

        function assign_evaluation() {
                $this->layout = FALSE;
                $this->load->model("application_status_history");

                $postdata = $this->postdata();
                $evaluators = $this->evaluator->find("evaluator_group_id = " . $this->db->escape($postdata['group']));
                if ($evaluators) {
                        foreach ($evaluators as $evaluator) {
                                $row = $this->application_evaluator->find_one("application_id = " . $this->db->escape($postdata['application_id']) . " AND evaluator_id = " . $evaluator['user_id']);
                                if (!$row) {
                                        $this->application_evaluator->insert(array('application_id' => $postdata['application_id'], 'evaluator_id' => $evaluator['user_id']));
                                }
                        }

                        //save approval status
                        if (count($evaluators) > 0) {
                                $this->application->update($postdata['application_id'], array('status' => APPLICATION_STATUS_ASSIGNED_TO_EVALUATOR));
                                $this->application_status_history->insert(array('application_id' => $postdata['application_id'], 'round_id' => 1, 'status' => APPLICATION_STATUS_ASSIGNED_TO_EVALUATOR, 'user_id' => $this->userdata['id']));
                        }

                        $this->session->set_flashdata('alert', "Your data has been updated.");
                } else {
                        $this->session->set_flashdata('alert', "No Evaluator selected.");
                }

                redirect(base_url() . PATH_TO_ADMIN . 'applications');
        }

        private function postdata() {
                if ($post = $this->input->post()) {
                        return $post;
                }
                redirect(base_url() . PATH_TO_ADMIN . 'applications');
        }

        function view_evaluation_list($application_id) {
                $data['application'] = $this->application->find_by_id($application_id);
                $data['evaluations'] = $this->application_evaluator->get_list_evaluation($application_id);

                if ($data['evaluations'] > 0) {
                        foreach ($data['evaluations'] as $key => $value) {
                                $evaluation = $this->evaluation->find_one("application_id = " . $value['application_id'] . " AND user_id = " . $value['evaluator_id']);
                                if ($evaluation) {
                                        $data['evaluations'][$key]['evaluation_id'] = $evaluation['id'];
                                        $data['evaluations'][$key]['score'] = $evaluation['total'];
                                } else {
                                        $data['evaluations'][$key]['evaluation_id'] = 0;
                                        $data['evaluations'][$key]['score'] = 0;
                                }
                        }
                }

                $this->load->view(PATH_TO_ADMIN . 'application/evaluation_list', $data);
        }

        function view_evaluation($evaluation_id) {
                $this->load->model('innovator');
                $this->load->model('evaluation');
                $this->load->model('evaluation_detail');
                $data['alert'] = $this->session->flashdata('alert');

                $evaluation = $this->evaluation->find_by_id($evaluation_id);
                $data['evaluation_focus'] = $this->get_evaluation_focus($evaluation['application_id']);
                $data['application'] = $this->application->find_by_id($evaluation['application_id']);
                $data['innovator'] = $this->innovator->find_by_id($data['application']['user_id']);
                $data['view_mode'] = 1;
                $evaluator_id = $evaluation['user_id'];
                $data['evaluator'] = $this->evaluator->find_one('user_id = ' . $evaluator_id);

                if ($evaluation) {
                        $data['evaluation'] = $evaluation;
                        $data['evaluation_detail'] = $this->evaluation_detail->find('evaluation_id = ' . $evaluation['id']);
                        $evaluator_id = $evaluation['user_id'];
                }

                $this->load->view(PATH_TO_EVALUATOR . 'application/evaluation_form', $data);
        }

        private function get_evaluation_focus($innovation_id) {
                $this->load->model('evaluation_form_model');
                $this->load->model('evaluation_focus_model');
                $this->load->model('evaluation_criteria');
                $this->load->model('evaluator');

                $form = $this->evaluation_form_model->find_one();
                $evaluation_focus = $this->evaluation_focus_model->find('evaluation_form_id = ' . $form['id']);
                foreach ($evaluation_focus as $key => $value) {
                        $evaluation_focus[$key]['criteria'] = $this->evaluation_criteria->find("evaluation_focus_id = " . $value['id']);
                }

                return $evaluation_focus;
        }

        function approve() {
                $this->layout = FALSE;
                $this->load->model('application_status_history');

                $id = $this->input->post("id");
                $is_approved = $this->input->post("is_approved");
                $comment = $this->input->post("comment");

                $update_data = array(
                    'status' => $is_approved ? APPLICATION_STATUS_APPROVED : APPLICATION_STATUS_REJECTED,
                    'comment' => $comment,
                );

                $this->application->update($id, $update_data);

                $this->application_status_history->insert(array(
                    'application_id' => $id,
                    'round_id' => 1,
                    'status' => $update_data['status'],
                    'user_id' => $this->userdata['id'],
                ));

                $response = json_encode(array(
                    "status" => 1,
                    "msg" => $is_approved ? "Data has been approved." : "Data has been rejected.",
                ));

                exit($response);
        }
        
        function approved() {
                $this->session->set_flashdata('alert', "Data has been approved.");
                redirect(base_url() . PATH_TO_ADMIN . 'applications');
        }

        function rejected() {
                $this->session->set_flashdata('alert', "Data has been approved.");
                redirect(base_url() . PATH_TO_ADMIN . 'applications');
        }

        function reject() {
                $this->layout = FALSE;
                $this->load->model('application_status_history');

                $id = $this->input->post("id");
                $is_approved = $this->input->post("is_approved");
                $comment = $this->input->post("comment");
                $status = $is_approved ? APPLICATION_STATUS_APPROVED : APPLICATION_STATUS_REJECTED;

                if ($this->application->update($id, array('status' => APPLICATION_STATUS_REJECTED))) {
                        $this->application_status_history->insert(array('application_id' => $id, 'round_id' => 1, 'status' => APPLICATION_STATUS_REJECTED, 'user_id' => $this->userdata['id']));
                        $this->session->set_flashdata('alert', "Data has been rejected.");
                } else {
                        $this->session->set_flashdata('alert', "Data cannot be rejected.");
                }

                redirect(base_url() . PATH_TO_ADMIN . 'applications');
        }

          private function export_to_excel($file_name, $title, $columns, $fields, $contents) {
                $this->load->library('PHPExcel');

                if (!$fields) {
                        echo 'No result. <a href="' . base_url() . PATH_TO_ADMIN . '">Back</a>';
                        die();
                }

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="' . $file_name . '.xls"');
                header('Cache-Control: max-age=0');

                // Create new PHPExcel object
                $objPHPExcel = new PHPExcel();

                // Set properties
                $objPHPExcel->getProperties()->setCreator("Innomap Johor");
                $objPHPExcel->getProperties()->setLastModifiedBy("Innomap Johor");
                $objPHPExcel->getProperties()->setTitle($title);
                $objPHPExcel->getProperties()->setSubject("Innomap Johor");

                #Column Width
                $objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(25);

                #Column Title Style
                $objPHPExcel->getActiveSheet()->getStyle("A1:AE1")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle("A1:AE1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                #Wrap Text
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setWrapText(true);

                // Add some data
                $objPHPExcel->setActiveSheetIndex(0);
                $col = 0;
                $row = 1;

                foreach ($columns as $column) {
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $column);
                        $col++;
                }
                $col = 0;
                $row++;
                foreach ($contents as $content) {
                        foreach ($fields as $field) {
                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $content[$field]);
                                $col++;
                        }
                        $col = 0;
                        $row++;
                }
                // Rename sheet
                $objPHPExcel->getActiveSheet()->setTitle('Sheet 1');

                // Save it as an excel 2003 file
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                //$objWriter->save("nameoffile.xls");
                $objWriter->save('php://output');
                exit;
        }

        private function generate_innovation_data($data){
                $this->load->model('innovator_expert_team');
                $this->lang->load('application',$this->language);

                $innovation_category = unserialize(INNOVATION_CATEGORY);
                $troubleshooting_opt = unserialize(TROUBLESHOOTING_OPTION);
                $targets = unserialize(INNOVATION_TARGET);
                $estimated_market_size = unserialize(ESTIMATED_MARKET_SIZE);
                $income = unserialize(INCOME_OPTION);

                $contents = array();
                $no = 1; 
                foreach ($data as $key => $value) {
                    $contents[$key]['no'] = $no;
                    $contents[$key]['innovator'] = $value['innovator_name'];
                    $contents[$key]['innovation'] = $value['title'];
                    $contents[$key]['innovation_category'] = $innovation_category[$value['innovation_category']];
                    $troubleshooting = json_decode($value['troubleshooting']);
                    $troubleshooting_str = "";
                    foreach ($troubleshooting as $val) {
                        $troubleshooting_str .= "- ".$troubleshooting_opt[$val]."\n";
                    }

                    $contents[$key]['troubleshooting'] = $troubleshooting_str;
                    $contents[$key]['idea'] = $value['idea'];
                    $contents[$key]['description'] = $value['description'];
                    $contents[$key]['estimated_market_size'] = $estimated_market_size[$value['estimated_market_size']];
                    $contents[$key]['material'] = $value['material'];
                    $contents[$key]['how_to_use'] = $value['how_to_use'];
                    $contents[$key]['special_achievement'] = $value['special_achievement'];
                    $contents[$key]['created_date'] = $value['created_date'];
                    $contents[$key]['discovered_date'] = $value['discovered_date'];
                    $contents[$key]['manufacturing_cost'] = $value['manufacturing_cost'];
                    $contents[$key]['selling_price'] = $value['selling_price'];
                    $contents[$key]['income'] = $income[$value['income']];
                    $target = json_decode($value['target']);
                    $target_str = "";
                    foreach ($target as $val) {
                        $target_str .= "- ".$targets[$val]."\n";
                    }

                    $contents[$key]['target'] = $target_str;
                    $contents[$key]['myipo_protection'] = ($value['myipo_protection'] == 1 ? lang('yes') : lang('no'));
                    $contents[$key]['submission_date'] = $value['submission_date'];
                    $contents[$key]['average_score'] = $this->get_average_score($value['id']);
                $no++;}

                return $contents;
        }

          function export_application(){
                $this->layout = FALSE;

                $where = "application.status >= ".APPLICATION_STATUS_SENT_APPROVAL;

                $data = $this->application->get_join($where)->result_array();
                $contents = $this->generate_innovation_data($data);

                $file_name = "Submission_List";
                $title = "Submission List";
                $columns = array("No",lang('innovator'),lang('project_title'), lang('category'),lang('troubleshooting'),lang('inspiration'),lang('description'),lang('estimated_market_size'),lang('material'),lang('how_to_use'),lang('special_achievement'),lang('created_date'),lang('discovered_date'),lang('manufacturing_cost'),lang('selling_price'),lang('income'),lang('target'),lang('myipo_protection'),lang('submission_date'),lang('average_score'));
                if(count($contents) > 0){
                    $fields = array_keys($contents[0]);

                    $this->export_to_excel($file_name,$title,$columns,$fields,$contents);
                }else{
                    redirect(base_url().PATH_TO_ADMIN.'applications');
                }
            }

    function proceed_to_next_round(){
        $this->layout = FALSE;
        $this->load->model('application_status_history');

        $postdata = $this->postdata();
        foreach ($postdata['ids'] as $key => $value) {
            $this->application->update($value, array('status' => APPLICATION_STATUS_PASSED));
            $this->application_status_history->insert(array('application_id' => $value, 'round_id' => 1, 'status' => APPLICATION_STATUS_PASSED, 'user_id' => $this->userdata['id']));
        }

        $this->session->set_flashdata('alert', "Your data has been updated.");
        redirect(base_url().PATH_TO_ADMIN.'applications');
    }

    function get_print_pdf_url(){
        $id = $this->input->post('id');

        $this->generate_pdf($id, FALSE);

        $application = $this->application->find_by_id($id);
        $url = "";

        if($application){
            $url = base_url().'assets/attachment/application_file/'.$application['pdf'];
        }
        echo json_encode(array('url' => $url));
    }
}
