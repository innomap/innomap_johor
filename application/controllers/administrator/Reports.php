<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/' . PATH_TO_ADMIN . '/Common.php');

class Reports extends Common {

        function __construct() {
                parent::__construct();

                $this->title = "Report";
                $this->menu = "report";

                $this->lang->load(PATH_TO_ADMIN . 'report', $this->language);

                $this->load->model('innovator');
                $this->load->model('application');
                $this->load->model('evaluation');
                $this->load->model('innovator_expert_team');

                $this->scripts[] = 'plugins/highcharts/highcharts';
                $this->scripts[] = 'plugins/bootstrap-datepicker/bootstrap-datepicker.min';
                $this->scripts[] = 'administrator/report';

                $this->styles[] = 'bootstrap-datepicker/bootstrap-datepicker.min';
        }

        function index() {
                
        }

        function registration_number_by_date() {
                $user = $this->innovator->get_registration_number_by_date(NULL, 1, "user_id DESC")->row_array();
                
                $data['from_date'] = date('d-m-Y', strtotime('-29 days', strtotime($user['created_at'])));
                $data['to_date'] = date('d-m-Y', strtotime($user['created_at']));
                $data['modal_detail'] = $this->load->view(PATH_TO_ADMIN . 'report/modal_detail', array('action_url' => 'export_registration_number_by_date'), TRUE);
                
                $this->load->view(PATH_TO_ADMIN . 'report/registration_number_by_date', $data);
        }

        
        function registration_number_by_date_handler() {
                $this->layout = FALSE;

                $xAxis = array();
                $data = array();
                $date_from = $this->input->post('date_from');
                $date_to = $this->input->post('date_to');
                
                $days = $this->count_interval($date_from, $date_to);
                
                for ($i = 0; $i <= $days; $i++) {
                        $raw_date = strtotime('+' . $i . ' days', strtotime($date_from));
                        $xAxis[] = date('d-m-Y', $raw_date);
                        $date = date('Y-m-d', $raw_date);
                        $data[] = $this->innovator->get_registration_number_by_date("DATE( created_at ) =  '" . $date . "'")->num_rows();
                }

                $result = array('xAxis' => $xAxis, 'series' => array(array('name' => 'Registration Number', 'data' => $data)));
                echo json_encode($result);
        }
        
        function submission_number_by_date() {
                $application = $this->application->find("status >= " . APPLICATION_STATUS_SENT_APPROVAL, 1, 0, "id DESC");
                $start_date = "2016-06-15";
                $data['from_date'] = date("d-m-Y", strtotime($start_date));
                $data['to_date'] = date("d-m-Y");
                $data['modal_detail'] = $this->load->view(PATH_TO_ADMIN . 'report/modal_detail', array('action_url' => 'export_submission_number_by_date'), TRUE);
                $this->load->view(PATH_TO_ADMIN . 'report/submission_number_by_date', $data);
        }

        function submission_number_by_date_handler() {
                $this->layout = FALSE;

                $xAxis = array();
                $data = array();
                $date_from = $this->input->post('date_from');
                $date_to = $this->input->post('date_to');

                $days = $this->count_interval($date_from, $date_to);

                for ($i = 0; $i <= $days; $i++) {
                        $raw_date = strtotime('+' . $i . ' days', strtotime($date_from));
                        $xAxis[] = date('d-m-Y', $raw_date);
                        $date = date('Y-m-d', $raw_date);
                        $data[] = $this->application->get_number("status >= " . APPLICATION_STATUS_SENT_APPROVAL . " AND DATE( created_at ) =  '" . $date . "'");
                }

                $result = array('xAxis' => $xAxis, 'series' => array(array('name' => 'Submission Number', 'data' => $data)));
                echo json_encode($result);
        }
        
        private function count_interval($date_from, $date_to) {
                $startTimeStamp = strtotime($date_from);
                $endTimeStamp = strtotime($date_to);

                $timeDiff = abs($endTimeStamp - $startTimeStamp);

                $numberDays = $timeDiff/86400;  // 86400 seconds in one day

                // and you might want to convert to integer
                $days = intval($numberDays);
                return $days;
        }
        
        function export_registration_number_by_date() {
                $this->layout = FALSE;
                $this->lang->load('account', $this->language);

                $date = $this->input->post('date');
                $from = $this->input->post('date_from');
                $to = $this->input->post('date_to');

                if ($date != "") {
                        $where = "DATE( created_at ) = '" . date('Y-m-d', strtotime($date)) . "'";
                } else {
                        $where = "DATE( created_at ) BETWEEN '" . date('Y-m-d', strtotime($from)) . "' AND '" . date('Y-m-d', strtotime($to)) . "'";
                }

                $data = $this->innovator->get_registration_number_by_date($where)->result_array();
                $contents = $this->generate_innovator_data($data);

                $file_name = "Registration_Number_by_Date";
                $title = "Registration Number by Date";
                $columns = array("No", lang('email'), lang('name'), lang('telp_no'), lang('fax_no'), lang('no_kp'), lang('address'));
                if (count($contents) > 0) {
                        $fields = array_keys($contents[0]);

                        $this->export_to_excel($file_name, $title, $columns, $fields, $contents);
                } else {
                        redirect(base_url() . PATH_TO_ADMIN . 'reports/registration_number_by_date');
                }
        }
        
        private function generate_innovator_data($data) {
                $contents = array();
                $no = 1;
                foreach ($data as $key => $value) {
                        $contents[$key]['no'] = $no++;
                        $contents[$key]['email'] = $value['email'];
                        $contents[$key]['name'] = $value['name'];
                        $contents[$key]['telp_no'] = $value['telp_no'];
                        $contents[$key]['fax_no'] = $value['fax_no'];
                        $contents[$key]['kp_no'] = $value['kp_no'];
                        $contents[$key]['address'] = $value['address'];
                }

                return $contents;
        }
        
        private function export_to_excel($file_name, $title, $columns, $fields, $contents) {
                $this->load->library('PHPExcel');

                if (!$fields) {
                        echo 'No result. <a href="' . base_url() . PATH_TO_ADMIN . '">Back</a>';
                        die();
                }

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="' . $file_name . '.xls"');
                header('Cache-Control: max-age=0');

                // Create new PHPExcel object
                $objPHPExcel = new PHPExcel();

                // Set properties
                $objPHPExcel->getProperties()->setCreator("Innomap Johor");
                $objPHPExcel->getProperties()->setLastModifiedBy("Innomap Johor");
                $objPHPExcel->getProperties()->setTitle($title);
                $objPHPExcel->getProperties()->setSubject("Innomap Johor");

                #Column Width
                $objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(25);

                #Column Title Style
                $objPHPExcel->getActiveSheet()->getStyle("A1:AE1")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle("A1:AE1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                #Wrap Text
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setWrapText(true);

                // Add some data
                $objPHPExcel->setActiveSheetIndex(0);
                $col = 0;
                $row = 1;

                foreach ($columns as $column) {
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $column);
                        $col++;
                }
                $col = 0;
                $row++;
                foreach ($contents as $content) {
                        foreach ($fields as $field) {
                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $content[$field]);
                                $col++;
                        }
                        $col = 0;
                        $row++;
                }
                // Rename sheet
                $objPHPExcel->getActiveSheet()->setTitle('Sheet 1');

                // Save it as an excel 2003 file
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                //$objWriter->save("nameoffile.xls");
                $objWriter->save('php://output');
                exit;
        }
        
        function submission_number_by_category() {
                $data['modal_detail'] = $this->load->view(PATH_TO_ADMIN . 'report/modal_detail', array('action_url' => 'export_application_number_by_category'), TRUE);
                $this->load->view(PATH_TO_ADMIN . 'report/application_number_by_category', $data);
        }

        function submission_number_by_category_handler() {
                $this->layout = FALSE;

                $data = array();
                $categories = unserialize(INNOVATION_CATEGORY);
                foreach ($categories as $key => $value) {
                        $data[$key]['name'] = $value;
                        $data[$key]['y'] = $this->application->get_join("innovation_category = " . $key . " AND application.status >= " . APPLICATION_STATUS_SENT_APPROVAL)->num_rows();
                        $data[$key]['val_id'] = $key;
                }

                $result = array("data" => $data);
                echo json_encode($result);
        }
        
        function submission_by_district() {
                $this->load->model('district');
                
                $districts = $this->district->find_all();
                foreach($districts AS $key => $district){
                        $count = $this->application->count_submitted_by_district($district['id']);
                        $districts[$key]['count'] = $count;
                        $districts[$key]['district'] = $district['name'];
                        
                        unset($districts[$key]['name']);
                }
                
                $total_application = $this->application->get_number("status >= " . APPLICATION_STATUS_SENT_APPROVAL);
                $total_approved_application = $this->application->get_number("status = " . APPLICATION_STATUS_APPROVED);
                $total_rejected_application = $this->application->get_number("status = " . APPLICATION_STATUS_REJECTED);
                $result = array("data" => $districts, 
                                "total_application" => $total_application,
                                "total_approved_application" => $total_approved_application,
                                "total_rejected_application" => $total_rejected_application);
                $this->load->view(PATH_TO_ADMIN . 'report/submission_by_district', $result);
        }

        function get_detail(){
                $this->layout = FALSE;

                $date = $this->input->post('date');
                $type = $this->input->post('type');

                if($type == 3){ //submission by number
                    $columns = array("No","Innovator","Innovation","Tindakan");
                    $fields = array("no","innovator_name","title","detail_innovation");
                    $data = $this->application->get_join("application.status >= ".APPLICATION_STATUS_SENT_APPROVAL." AND DATE( application.created_at ) =  '".date('Y-m-d',strtotime($date))."'")->result_array();
                }else if($type == 5){
                    $category = $this->input->post('category');
                    $columns = array("No","Innovator","Innovation","Tindakan");
                    $fields = array("no","innovator_name","title","detail_innovation");
                    $data = $this->application->get_join("innovation_category = ".$category." AND application.status >= ".APPLICATION_STATUS_SENT_APPROVAL)->result_array();
                }

                $result = array('columns' => $columns,'data' => $data, 'fields' => $fields);
                echo json_encode($result);
        }

        function export_submission_number_by_date(){
                $this->layout = FALSE;
                $this->lang->load('application',$this->language);

                $date = $this->input->post('date');
                $from = $this->input->post('date_from');
                $to = $this->input->post('date_to');

                if($date != ""){
                    $where = "application.status >= ".APPLICATION_STATUS_SENT_APPROVAL." AND DATE( application.created_at ) =  '".date('Y-m-d',strtotime($date))."'";
                }else{
                    $where = "application.status >= ".APPLICATION_STATUS_SENT_APPROVAL." AND DATE( application.created_at ) BETWEEN '".date('Y-m-d',strtotime($from))."' AND '".date('Y-m-d',strtotime($to))."'";
                }

                $data = $this->application->get_join($where)->result_array();
                $contents = $this->generate_innovation_data($data);

                $file_name = "Submission_Number_by_Date";
                $title = "Submission Number by Date";
                $columns = array("No",lang('innovator'),lang('project_title'), lang('category'),lang('troubleshooting'),lang('inspiration'),lang('description'),lang('estimated_market_size'),lang('material'),lang('how_to_use'),lang('special_achievement'),lang('created_date'),lang('discovered_date'),lang('manufacturing_cost'),lang('selling_price'),lang('income'),lang('target'),lang('myipo_protection'),lang('submission_date'));
                if(count($contents) > 0){
                    $fields = array_keys($contents[0]);

                    $this->export_to_excel($file_name,$title,$columns,$fields,$contents);
                }else{
                    redirect(base_url().PATH_TO_ADMIN.'reports/submission_number_by_date');
                }
        }

        private function generate_innovation_data($data){
                $this->load->model('innovator_expert_team');
                $this->lang->load('application',$this->language);

                $innovation_category = unserialize(INNOVATION_CATEGORY);
                $troubleshooting_opt = unserialize(TROUBLESHOOTING_OPTION);
                $targets = unserialize(INNOVATION_TARGET);
                $estimated_market_size = unserialize(ESTIMATED_MARKET_SIZE);
                $income = unserialize(INCOME_OPTION);

                $contents = array();
                $no = 1; 
                foreach ($data as $key => $value) {
                    $contents[$key]['no'] = $no;
                    $contents[$key]['innovator'] = $value['innovator_name'];
                    $contents[$key]['innovation'] = $value['title'];
                    $contents[$key]['innovation_category'] = $innovation_category[$value['innovation_category']];
                    $troubleshooting = json_decode($value['troubleshooting']);
                    $troubleshooting_str = "";
                    foreach ($troubleshooting as $val) {
                        $troubleshooting_str .= "- ".$troubleshooting_opt[$val]."\n";
                    }

                    $contents[$key]['troubleshooting'] = $troubleshooting_str;
                    $contents[$key]['idea'] = $value['idea'];
                    $contents[$key]['description'] = $value['description'];
                    $contents[$key]['estimated_market_size'] = $estimated_market_size[$value['estimated_market_size']];
                    $contents[$key]['material'] = $value['material'];
                    $contents[$key]['how_to_use'] = $value['how_to_use'];
                    $contents[$key]['special_achievement'] = $value['special_achievement'];
                    $contents[$key]['created_date'] = $value['created_date'];
                    $contents[$key]['discovered_date'] = $value['discovered_date'];
                    $contents[$key]['manufacturing_cost'] = $value['manufacturing_cost'];
                    $contents[$key]['selling_price'] = $value['selling_price'];
                    $contents[$key]['income'] = $income[$value['income']];
                    $target = json_decode($value['target']);
                    $target_str = "";
                    foreach ($target as $val) {
                        $target_str .= "- ".$targets[$val]."\n";
                    }

                    $contents[$key]['target'] = $target_str;
                    $contents[$key]['myipo_protection'] = ($value['myipo_protection'] == 1 ? lang('yes') : lang('no'));
                    $contents[$key]['submission_date'] = $value['submission_date'];
                $no++;}

                return $contents;
        }

        function export_application_number_by_category(){
                $this->layout = FALSE;

                $category = $this->input->post('category');

                if($category != ""){
                    $where = "innovation_category = ".$category." AND application.status >= ".APPLICATION_STATUS_SENT_APPROVAL;
                }else{
                    $where = "application.status >= ".APPLICATION_STATUS_SENT_APPROVAL;
                }

                $data = $this->application->get_join($where)->result_array();
                $contents = $this->generate_innovation_data($data);

                $file_name = "Submission_Number_by_Category";
                $title = "Submission Number by Category";
                $columns = array("No",lang('innovator'),lang('project_title'), lang('category'),lang('troubleshooting'),lang('inspiration'),lang('description'),lang('estimated_market_size'),lang('material'),lang('how_to_use'),lang('special_achievement'),lang('created_date'),lang('discovered_date'),lang('manufacturing_cost'),lang('selling_price'),lang('income'),lang('target'),lang('myipo_protection'),lang('submission_date'));
                if(count($contents) > 0){
                    $fields = array_keys($contents[0]);

                    $this->export_to_excel($file_name,$title,$columns,$fields,$contents);
                }else{
                    redirect(base_url().PATH_TO_ADMIN.'reports/application_number_by_category');
                }
            }

        function export_application_number_by_district(){
                $this->layout = FALSE;

                $where = "application.status >= ".APPLICATION_STATUS_SENT_APPROVAL;

                $data = $this->application->get_join($where)->result_array();
                $contents = $this->generate_innovation_data($data);

                $file_name = "Submission_Number_by_District";
                $title = "Submission Number by District";
                $columns = array("No",lang('innovator'),lang('project_title'), lang('category'),lang('troubleshooting'),lang('inspiration'),lang('description'),lang('estimated_market_size'),lang('material'),lang('how_to_use'),lang('special_achievement'),lang('created_date'),lang('discovered_date'),lang('manufacturing_cost'),lang('selling_price'),lang('income'),lang('target'),lang('myipo_protection'),lang('submission_date'));
                if(count($contents) > 0){
                    $fields = array_keys($contents[0]);

                    $this->export_to_excel($file_name,$title,$columns,$fields,$contents);
                }else{
                    redirect(base_url().PATH_TO_ADMIN.'reports/application_number_by_district');
                }
            }

        function export_complete_application_data(){
            $this->layout = FALSE;

            $where = "application.status >= ".APPLICATION_STATUS_SENT_APPROVAL;

            $data = $this->application->get_join($where)->result_array();
            $contents = $this->generate_complete_innovation_data($data);

            $file_name = "Submission_List";
            $title = "Submission List";
            $columns = array("No",lang('innovator'), lang('application_type'), lang('ic_number'), lang('gender'), lang('address'),lang('postcode'),lang('telp_home_no'), lang('telp_no'),lang('email'), lang('district'),lang('team_member'),lang('project_title'), lang('category'),lang('troubleshooting'),lang('inspiration'),lang('description'),lang('estimated_market_size'),lang('material'),lang('how_to_use'),lang('special_achievement'),lang('created_date'),lang('discovered_date'),lang('manufacturing_cost'),lang('selling_price'),lang('income'),lang('target'),lang('myipo_protection'),lang('submission_date'), lang('average_score'), lang('status'));
            if(count($contents) > 0){
                $fields = array_keys($contents[0]);

                $this->export_to_excel($file_name,$title,$columns,$fields,$contents);
            }else{
                redirect(base_url().PATH_TO_ADMIN.'reports/application_number_by_district');
            }
        }

        private function generate_complete_innovation_data($data){
                $this->load->model('innovator_expert_team');
                $this->lang->load('application',$this->language);

                $status = unserialize(APPLICATION_STATUS);
                $application_type = unserialize(APPLICATION_TYPE);
                $gender = unserialize(GENDER);
                $innovation_category = unserialize(INNOVATION_CATEGORY);
                $troubleshooting_opt = unserialize(TROUBLESHOOTING_OPTION);
                $targets = unserialize(INNOVATION_TARGET);
                $estimated_market_size = unserialize(ESTIMATED_MARKET_SIZE);
                $income = unserialize(INCOME_OPTION);

                $contents = array();
                $no = 1; 
                foreach ($data as $key => $value) {
                    $contents[$key]['no'] = $no;
                    $contents[$key]['innovator'] = $value['innovator_name'];
                    $contents[$key]['application_type'] = $application_type[$value['application_type']];
                    $contents[$key]['ic_number'] = $value['ic_number'];
                    $contents[$key]['gender'] = $gender[$value['gender']];
                    $contents[$key]['address'] = $value['address'];
                    $contents[$key]['postcode'] = $value['postcode'];
                    $contents[$key]['telp_home_no'] = $value['telp_home_no'];
                    $contents[$key]['telp_no'] = $value['telp_no'];
                    $contents[$key]['email'] = $value['email'];
                    $contents[$key]['district_name'] = $value['district_name'];
                    $team_member = $this->innovator_expert_team->find("user_id = ".$value['user_id']);
                    $team_member_str = "";
                    foreach ($team_member as $val) {
                        if($val['name'] != ""){
                            $team_member_str = "- ".$val['name']."/".$val['ic_number'];
                        }
                    }
                    $contents[$key]['team_member'] = $team_member_str;
                    $contents[$key]['innovation'] = $value['title'];
                    $contents[$key]['innovation_category'] = $innovation_category[$value['innovation_category']];
                    $troubleshooting = json_decode($value['troubleshooting']);
                    $troubleshooting_str = "";
                    foreach ($troubleshooting as $val) {
                        $troubleshooting_str .= "- ".$troubleshooting_opt[$val]."\n";
                    }

                    $contents[$key]['troubleshooting'] = $troubleshooting_str;
                    $contents[$key]['idea'] = $value['idea'];
                    $contents[$key]['description'] = $value['description'];
                    $contents[$key]['estimated_market_size'] = $estimated_market_size[$value['estimated_market_size']];
                    $contents[$key]['material'] = $value['material'];
                    $contents[$key]['how_to_use'] = $value['how_to_use'];
                    $contents[$key]['special_achievement'] = $value['special_achievement'];
                    $contents[$key]['created_date'] = $value['created_date'];
                    $contents[$key]['discovered_date'] = $value['discovered_date'];
                    $contents[$key]['manufacturing_cost'] = $value['manufacturing_cost'];
                    $contents[$key]['selling_price'] = $value['selling_price'];
                    $contents[$key]['income'] = $income[$value['income']];
                    $target = json_decode($value['target']);
                    $target_str = "";
                    foreach ($target as $val) {
                        $target_str .= "- ".$targets[$val]."\n";
                    }

                    $contents[$key]['target'] = $target_str;
                    $contents[$key]['myipo_protection'] = ($value['myipo_protection'] == 1 ? lang('yes') : lang('no'));
                    $contents[$key]['submission_date'] = $value['submission_date'];
                    $contents[$key]['average_score'] = $this->get_average_score($value['id']);
                    $contents[$key]['status'] = $status[$value['status']]['name'];
                $no++;}

                return $contents;
        }

        private function get_average_score($application_id) {
            $data_score = $this->evaluation->get_average_score("application_id = " . $application_id)->row_array();

            return $data_score['score'];
        }
}
