<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/' . PATH_TO_ADMIN . '/Common.php');

class Locators extends Common {

        function __construct() {
                parent::__construct();

                $this->title = "Locator";
                $this->menu = "report";

                $this->load->model('innovator');
                $this->load->model('innovator_expert_team');
                $this->load->model('district');
                $this->load->model('application');

                $this->load->library('googlemaps');

                $this->lang->load('account', $this->language);
        }

        function index() {
        }

        function innovator(){
                $data = $this->get_locator();

                $this->load->view(PATH_TO_ADMIN . 'locator/index', $data);
        }

        function get_locator() {
                //set configuration for google map
                $config['map_height'] = '610px';

                //set map marker filter by posted data
                $config['center'] = '2.0507919,102.9491133';
                $config['zoom'] = '9';

                $pins = $this->innovator->find_all();

                $this->googlemaps->initialize($config);

                $count_innovation = 0;
                $count_university = 0;
                $count_expert = 0;
                $count_project = 0;
                $kosong = 0;

                if ($pins) {
                        foreach ($pins as $pin) {
                                if ($pin['latitude'] != "" && $pin['longitude'] != "") {
                                        $marker = array();
                                        $marker['position'] = $pin['latitude'] . "," . $pin['longitude'];
                                        $marker['infowindow_content'] = preg_replace("/\r\n|\r|\n/", '<br/>', $this->set_locator_info_window($pin));
                                        $marker['animation'] = 'DROP';
                                        $marker['title'] = $pin['name'];
                                        $this->googlemaps->add_marker($marker);
                                }
                        }
                }

                //pass form data into view
                $data['map'] = $this->googlemaps->create_map();

                return $data;
        }

        function set_locator_info_window($content) {
                $application_types = unserialize(APPLICATION_TYPE);
                $gender = unserialize(GENDER);
                $district = $this->district->find_one("id = " . $content['district_id']);
                $innovation_category = unserialize(INNOVATION_CATEGORY);

                $info_window = '';

                if (file_exists(realpath(APPPATH . '../' . PATH_TO_INNOVATOR_PHOTO) . DIRECTORY_SEPARATOR . $content['photo']) && $content['photo'] != "") {
                        $info_window .= "<img src='" . base_url() . PATH_TO_INNOVATOR_PHOTO_THUMB . $content['photo'] . "' width='100px'/><br/>";
                }

                $info_window .= '<strong class=mr>' . lang('application_type') . '</strong>: ' . $application_types[$content['application_type']] . '<br/>';
                $info_window .= '<strong class=mr>' . lang('name') . '</strong>: ' . $content['name'] . '<br/>';
                $info_window .= '<strong class=mr>' . lang('ic_number') . '</strong>: ' . $content['ic_number'] . '<br/>';
                $info_window .= '<strong class=mr>' . lang('gender') . '</strong>: ' . $gender[$content['gender']] . '<br/>';
                $info_window .= '<strong class=mr>' . lang('address') . '</strong>: ' . $content['address'] . '<br/>';
                $info_window .= '<strong class=mr>' . lang('postcode') . '</strong>: ' . $content['postcode'] . '<br/>';
                $info_window .= '<strong class=mr>' . lang('district') . '</strong>: ' . $district['name'] . '<br/>';
                $info_window .= '<strong class=mr>' . lang('telp_home_no') . '</strong>: ' . $content['telp_home_no'] . '<br/>';
                $info_window .= '<strong class=mr>' . lang('telp_no') . '</strong>: ' . $content['telp_no'] . '<br/>';

                if ($content['application_type'] != 0) {
                        $team = $this->innovator_expert_team->find('user_id = ' . $content['user_id']);
                        $info_window .= '<strong class=mr>' . lang('team_member') . '</strong>: <br/>';
                        $no = 1;
                        foreach ($team as $key => $value) {
                                $info_window .= '<strong class=mr>' . $no . " " . lang('name') . '</strong>: ' . $value['name'] . '<br/>';
                                $info_window .= '&nbsp;&nbsp;&nbsp;&nbsp;<strong class=mr>' . lang('ic_number') . '</strong>: ' . $value['ic_number'] . '<br/>';
                                $no++;
                        }
                }


                return $info_window;
        }

        function application(){
                $data = $this->get_innovation_locator();

                $this->load->view(PATH_TO_ADMIN . 'locator/application', $data);
        }

        function get_innovation_locator() {
                //set configuration for google map
                $config['map_height'] = '610px';

                //set map marker filter by posted data
                $config['center'] = '2.0507919,102.9491133';
                $config['zoom'] = '9';

                $pins = $this->application->get_join("status = ".APPLICATION_STATUS_SENT_APPROVAL." OR status = ".APPLICATION_STATUS_APPROVED)->result_array();

                $this->googlemaps->initialize($config);

                $count_innovation = 0;
                $count_university = 0;
                $count_expert = 0;
                $count_project = 0;
                $kosong = 0;

                if ($pins) {
                        foreach ($pins as $pin) {
                                if ($pin['latitude'] != "" && $pin['longitude'] != "") {
                                        $marker = array();
                                        $marker['position'] = $pin['latitude'] . "," . $pin['longitude'];
                                        $marker['infowindow_content'] = preg_replace("/\r\n|\r|\n/", '<br/>', $this->set_application_locator_info_window($pin));
                                        $marker['animation'] = 'DROP';
                                        $marker['title'] = $pin['title'];
                                        $this->googlemaps->add_marker($marker);
                                }
                        }
                }

                //pass form data into view
                $data['map'] = $this->googlemaps->create_map();

                return $data;
        }

        function set_application_locator_info_window($content) {
                $this->lang->load('application', $this->language);

                $innovation_category = unserialize(INNOVATION_CATEGORY);

                $info_window = '';

                $info_window .= '<strong class=mr>' . lang('innovator') . '</strong>: ' . $content['innovator_name'] . '<br/>';
                $info_window .= '<strong class=mr>' . lang('district') . '</strong>: ' . $content['district_name'] . '<br/>';
                $info_window .= '<strong class=mr>' . lang('project_title') . '</strong>: ' . $content['title'] . '<br/>';
                $info_window .= '<strong class=mr>' . lang('description') . '</strong>: ' . $content['description'] . '<br/>';
                $info_window .= '<strong class=mr>' . lang('category') . '</strong>: ' . $innovation_category[$content['innovation_category']] . '<br/>';
                $link = base_url().PATH_TO_ADMIN.'applications/view/'.$content['id'];
                $info_window .= '<a href='.$link.' target=_blank>' . lang('view_detail') . '</a><br/>';

                return $info_window;
        }

}
