<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Users extends Common {

	function __construct() {
		parent::__construct();

		$this->title = "Manage User Admin";
		$this->menu = "user";

		$this->load->model('user');
        $this->load->model('admin');
        $this->load->model('district');

		$this->lang->load(PATH_TO_ADMIN.'user',$this->language);

		$this->scripts[] = 'administrator/user';
    }

    public function index(){
    	$data['alert'] = $this->session->flashdata('alert');
        $data['users'] = $this->user->get_admin()->result_array();
        $data['role'] = unserialize(USER_ROLE_ADMIN);

		$this->load->view(PATH_TO_ADMIN.'user/list', $data);
    }

    function add(){
        $data['districts'] = $this->district->find_all();
        $data['form_action'] = 'save';
        $data['role'] = unserialize(USER_ROLE_ADMIN);
        $data['gender'] = unserialize(GENDER);

        $this->load->view(PATH_TO_ADMIN.'user/form',$data);
    }

    function save(){
    	$this->layout = FALSE;

        $postdata = $this->postdata();

        if($postdata['username'] != "" && $postdata['district_id'] != ""){
            if($this->user->is_username_exist($postdata['username'], $postdata['id'])){
                $this->session->set_flashdata('alert','Sorry, your username has been registered.');
            }else{
                $data = array(
                    "username" => $postdata['username'],
                    "status" => USER_STATUS_ACTIVE,
                    "role_id" => $postdata['role_id'],
                    "created_at" => date('Y-m-d H:i:s'));
                
                if($postdata['password'] != ""){
                    $data["password"] = $postdata['password'];
                }

                if($postdata['id'] > 0){
                    $id = $this->user->update_user($postdata['id'],$data);
                }else{
                    $id = $this->user->insert_user($data);
                }
            
                if($id > 0){
                    $admin = array( //"name" => $postdata['name'],
                                    //"ic_number" => $postdata['ic_num_1']."-".$postdata['ic_num_2']."-".$postdata['ic_num_3'],
                                    //"gender" => $postdata['gender'],
                                    //"position" => $postdata['position'],
                                    //"department" => $postdata['department'],
                                    //"address" => $postdata['address'],
                                    //"postcode" => $postdata['postcode'],
                                    "district_id" => $postdata['district_id'],
                                    //"telp_home_no" => $postdata['telp_home_no'],
                                    //"telp_no" => $postdata['telp_no']
                                    );

                    if($postdata['id'] > 0){
                        if($this->admin->update($postdata['id'],$admin)){
                            $this->session->set_flashdata('alert','User has been updated.');
                        }
                    }else{
                        $admin['user_id'] = $id;
                        if($this->admin->insert($admin)){
                            $this->session->set_flashdata('alert','User has been added.');
                        }
                    }
                }else{
                    $this->session->set_flashdata('alert','Sorry, an error occurred, please try again later.');
                }
            }
        }

        redirect(base_url().PATH_TO_ADMIN.'users');
    }

    public function edit($id = 0){
		$data['districts'] = $this->district->find_all();
        $data['form_action'] = 'save';
        $data['role'] = unserialize(USER_ROLE_ADMIN);
        $data['gender'] = unserialize(GENDER);
        $data['user'] = $this->user->get_admin("user_id = ".$id)->row_array();
        if($data['user']){
            if($data['user']['ic_number'] != ""){
                $split_ic_num = explode("-", $data['user']['ic_number']);
                $data['user']['ic_num_1'] = $split_ic_num[0];
                $data['user']['ic_num_2'] = $split_ic_num[1];
                $data['user']['ic_num_3'] = $split_ic_num[2];
            }else{
                $data['user']['ic_num_1'] = "";
                $data['user']['ic_num_2'] = "";
                $data['user']['ic_num_3'] = "";
            }
        }
		
		$this->load->view(PATH_TO_ADMIN.'user/form',$data);
	}

    private function postdata(){
        if($post = $this->input->post()){
            return $post;
        }
        redirect(base_url().PATH_TO_ADMIN.'users');
    }

    function delete($id){
        $this->layout = FALSE;
        if($this->user->delete($id)){
            $this->session->set_flashdata('alert','User has been deleted.');
        }else{
            $this->session->set_flashdata('alert','User can not be deleted.');
        }

        redirect(base_url().PATH_TO_ADMIN.'users');
    }
}
