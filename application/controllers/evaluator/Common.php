<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Controller {

	function __construct($module = NULL) {
		parent::__construct();

		$this->layout = EVALUATOR_LAYOUT;

		$this->language = LANGUAGE_MELAYU;

		if($module != "account"){
			$this->userdata = $this->user_session->get_evaluator();
			if (!$this->userdata) {
				redirect(base_url().'login');
			}	
		}
    }
}